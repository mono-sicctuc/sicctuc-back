-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-06-2021 a las 07:05:05
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sicctuc`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asiento`
--

CREATE TABLE `asiento` (
  `id` int(11) NOT NULL,
  `asiento_monto` double NOT NULL,
  `asiento_descripcion` varchar(100) DEFAULT NULL,
  `asiento_fecha` timestamp NULL DEFAULT current_timestamp(),
  `asiento_tipo` tinyint(4) NOT NULL COMMENT '1. Debe\n2. Haber',
  `asiento_estado` tinyint(1) DEFAULT 1 COMMENT '1. Activo \n0. Anulado',
  `persona_id` int(11) NOT NULL,
  `codigo_contable_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asiento_anulacion`
--

CREATE TABLE `asiento_anulacion` (
  `id` int(11) NOT NULL,
  `anulacion_fecha` timestamp NULL DEFAULT current_timestamp(),
  `asiento_id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL COMMENT 'anulado por'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bus`
--

CREATE TABLE `bus` (
  `id` int(11) NOT NULL,
  `bus_placa` varchar(10) NOT NULL,
  `bus_modelo` varchar(20) DEFAULT NULL,
  `bus_chasis` varchar(100) DEFAULT NULL,
  `bus_motor` varchar(100) DEFAULT NULL,
  `bus_circulacion` varchar(255) DEFAULT NULL,
  `bus_color` varchar(25) DEFAULT NULL,
  `bus_estado` tinyint(4) DEFAULT 1 COMMENT '1 = activo',
  `persona_id` int(11) NOT NULL,
  `bus_marca_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bus`
--

INSERT INTO `bus` (`id`, `bus_placa`, `bus_modelo`, `bus_chasis`, `bus_motor`, `bus_circulacion`, `bus_color`, `bus_estado`, `persona_id`, `bus_marca_id`) VALUES
(1, 'M-0643', '', '', '', '', 'Blanco', 1, 2, 1),
(2, 'M-0677', '', '', '', '', 'Multicolor', 1, 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bus_marca`
--

CREATE TABLE `bus_marca` (
  `id` int(11) NOT NULL,
  `bus_marca_nombre` varchar(100) NOT NULL,
  `bus_marca_estado` tinyint(4) DEFAULT NULL COMMENT '0 inactivo\n1 activo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bus_marca`
--

INSERT INTO `bus_marca` (`id`, `bus_marca_nombre`, `bus_marca_estado`) VALUES
(1, 'Dina', 1),
(2, 'Ruso', 1),
(3, 'testtttt', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codigo_contable`
--

CREATE TABLE `codigo_contable` (
  `id` int(11) NOT NULL,
  `codigo_valor` varchar(10) DEFAULT NULL,
  `codigo_tipo` tinyint(1) DEFAULT NULL COMMENT '1. Activo\n2. Pasivo\n3. Capital',
  `codigo_subtipo` tinyint(1) DEFAULT NULL COMMENT '1. Activo Circulante\n2. Activo Diferido\n3. Activo Fijo\n4. Pasivo Corto Plazo\n5. Pasivo Largo Plazo \n6. Capital',
  `codigo_nombre` varchar(100) NOT NULL,
  `codigo_descripcion` varchar(200) DEFAULT NULL,
  `estado_resultado` tinyint(1) DEFAULT 0 COMMENT '1. Tomar en cuenta para el estado de resultado\n0. No tomar en cuenta',
  `estado_resultado_tipo` tinyint(1) DEFAULT NULL COMMENT '1. Ingreso\n2. Costo\n3. Gastos de Operacion',
  `created` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `constante`
--

CREATE TABLE `constante` (
  `id` int(11) NOT NULL,
  `constante_nombre` varchar(45) NOT NULL,
  `constante_slug` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Valores que podrían cambiar en el tiempo pero no a largo plazo';

--
-- Volcado de datos para la tabla `constante`
--

INSERT INTO `constante` (`id`, `constante_nombre`, `constante_slug`) VALUES
(1, 'INSS Laboral', 'INSS-L'),
(2, 'INSS Patronal', 'INSS-P'),
(3, 'Inatec', 'Inatec'),
(4, 'Pasaje', 'Pasaje');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `constante_cambio`
--

CREATE TABLE `constante_cambio` (
  `id` int(11) NOT NULL,
  `constante_id` int(11) NOT NULL,
  `constante_valor` float NOT NULL,
  `constante_estado` tinyint(4) DEFAULT 1,
  `constante_creado` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `constante_cambio`
--

INSERT INTO `constante_cambio` (`id`, `constante_id`, `constante_valor`, `constante_estado`, `constante_creado`) VALUES
(1, 1, 0, 1, NULL),
(2, 2, 0, 1, NULL),
(3, 3, 0, 1, NULL),
(4, 4, 2.5, 1, '2021-06-06 04:32:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `control_denominacion`
--

CREATE TABLE `control_denominacion` (
  `id` int(11) NOT NULL,
  `nombre_moneda` varchar(45) NOT NULL,
  `valor_moneda` double NOT NULL,
  `cantidad` int(11) NOT NULL,
  `tipo` tinyint(4) NOT NULL DEFAULT 2 COMMENT '1. Billete\n2. Moneda\n3. Cheque',
  `control_diario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `control_diario`
--

CREATE TABLE `control_diario` (
  `id` int(11) NOT NULL,
  `control_fecha` date NOT NULL,
  `control_medida_inicial` float NOT NULL,
  `control_medida_final` float NOT NULL,
  `control_diario_estado` tinyint(1) DEFAULT 1 COMMENT '1. Abierto\n2. Cerrado',
  `control_creado` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cooperativa`
--

CREATE TABLE `cooperativa` (
  `id` int(11) NOT NULL,
  `cooperativa_nombre` varchar(45) NOT NULL,
  `cooperativa_direccion` varchar(255) DEFAULT NULL,
  `cooperativa_telefono` varchar(45) DEFAULT NULL,
  `cooperativa_email` varchar(255) DEFAULT NULL,
  `cooperativa_ruc` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diesel`
--

CREATE TABLE `diesel` (
  `id` int(11) NOT NULL,
  `diesel_precio` double NOT NULL,
  `diesel_created` timestamp NULL DEFAULT current_timestamp(),
  `diesel_estado` tinyint(1) DEFAULT 1 COMMENT '1. Actual / Activo\n2. Anterior / Inactivo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `id` int(11) NOT NULL,
  `factura_fecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `factura_creado` timestamp NULL DEFAULT current_timestamp(),
  `factura_total` double DEFAULT NULL,
  `factura_tipo` tinyint(1) DEFAULT NULL COMMENT '1. Contado\n2. Crédito',
  `factura_estado` tinyint(1) DEFAULT NULL COMMENT '1. Activo\n2. Anulado',
  `facturado_por` int(11) NOT NULL,
  `socio_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_diesel`
--

CREATE TABLE `factura_diesel` (
  `id` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `cantidad_sugerida` float DEFAULT NULL COMMENT 'Cantidad sugerida en base a Kilometraje',
  `factura_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `diesel_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_producto`
--

CREATE TABLE `factura_producto` (
  `id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` double NOT NULL,
  `producto_id` int(11) NOT NULL,
  `factura_rol_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_rol`
--

CREATE TABLE `factura_rol` (
  `id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `factura_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuesto_detalle`
--

CREATE TABLE `impuesto_detalle` (
  `id` int(11) NOT NULL,
  `monto_desde` double NOT NULL,
  `monto_hasta` double DEFAULT NULL,
  `impuesto_base` double DEFAULT NULL,
  `porcentaje` float DEFAULT NULL,
  `sobre_exceso` double DEFAULT NULL,
  `impuesto_renta_id` int(11) NOT NULL,
  `creado_el` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `impuesto_detalle`
--

INSERT INTO `impuesto_detalle` (`id`, `monto_desde`, `monto_hasta`, `impuesto_base`, `porcentaje`, `sobre_exceso`, `impuesto_renta_id`, `creado_el`) VALUES
(1, 0.01, 100000, 0, 0, 0, 1, NULL),
(2, 100000.01, 200000, 0, 15, 100000, 1, NULL),
(3, 200000.01, 350000, 15000, 20, 200000, 1, NULL),
(4, 350000.01, 500000, 35000, 25, 350000, 1, NULL),
(5, 500000.01, 0, 82500, 30, 500000, 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuesto_renta`
--

CREATE TABLE `impuesto_renta` (
  `id` int(11) NOT NULL,
  `creado_el` timestamp NULL DEFAULT current_timestamp(),
  `estado` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sólo para tener agrupados los rangos';

--
-- Volcado de datos para la tabla `impuesto_renta`
--

INSERT INTO `impuesto_renta` (`id`, `creado_el`, `estado`) VALUES
(1, '2017-11-23 05:15:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

CREATE TABLE `permiso` (
  `id` int(11) NOT NULL,
  `valor` varchar(10) NOT NULL,
  `seccion_id` int(11) NOT NULL,
  `rol_usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` int(11) NOT NULL,
  `persona_nombre` varchar(60) NOT NULL,
  `persona_apellido` varchar(120) NOT NULL,
  `persona_cedula` varchar(16) DEFAULT NULL,
  `persona_nacimiento` date DEFAULT NULL,
  `persona_telefono` varchar(12) DEFAULT NULL,
  `persona_email` varchar(100) DEFAULT NULL,
  `persona_direccion` text DEFAULT NULL,
  `persona_salario` double DEFAULT NULL,
  `persona_acceso` tinyint(4) DEFAULT 0 COMMENT '0 - acceso no permitido\n1 - acceso permitido',
  `persona_ingreso` date DEFAULT NULL,
  `persona_estado` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0 inactivo\n1 activo',
  `persona_tipo_id` int(11) DEFAULT NULL,
  `puesto_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `persona_nombre`, `persona_apellido`, `persona_cedula`, `persona_nacimiento`, `persona_telefono`, `persona_email`, `persona_direccion`, `persona_salario`, `persona_acceso`, `persona_ingreso`, `persona_estado`, `persona_tipo_id`, `puesto_id`) VALUES
(1, 'Jesus', 'Perez', '000-050500-0000P', '2000-04-25', '22803901', '', 'Managua', 15000, 0, '2017-10-18', 1, 2, NULL),
(2, 'Jorge', 'Manzanarez', NULL, NULL, '86864586', NULL, NULL, NULL, 1, '1990-10-10', 1, 1, NULL),
(3, 'Lavinia', 'Manzanarez', NULL, NULL, NULL, 'laviniamanzanares@gmail.com', NULL, NULL, 1, NULL, 1, 1, NULL),
(4, 'Lorem', 'Ipsum', '000-120583-0000X', '2021-01-25', '88884445', '', '', 12000, 0, NULL, 1, 2, 3),
(10, 'Commodi commodo vel ', 'Laborum et labore ea', 'Ipsam quis incid', '1994-04-26', '+1 (505) 651', 'wonalybu@mailinator.com', 'Aut ut irure labore ', 44, 0, NULL, 0, 2, NULL),
(12, 'Rerum', 'Voluptate', '000-150173-0000O', '2003-05-01', '+1 (772) 721', 'wibiwepi@mailinator.com', 'Harum ipsa odit sun', 8000, 0, '2021-05-24', 1, 2, 6),
(13, 'Voluptatem', 'Dolores', '111-111100-0000P', '1971-05-07', '+1 (565) 172', 'sasonyj@mailinator.com', 'Et non aut commodi i', 9525.25, 1, '2021-05-24', 1, 2, NULL),
(17, 'Luisa', 'Lopez', '000-220589-0016O', '0000-00-00', '+1 (942) 391', '', '', 10000, 0, '2021-06-02', 1, 2, 2),
(18, 'Carlos', 'Sanchez', '000-111262-0001P', '1994-02-09', '+1 (604) 308', '', '', 9525, 0, '2021-06-03', 1, 2, 3),
(19, 'ABC', 'DEF', '111-101055-0000L', '2021-06-03', '+1 (769) 593', '', '', 15000, 0, '2021-06-03', 1, 2, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_acceso`
--

CREATE TABLE `persona_acceso` (
  `id` int(11) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `contrasena` text NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NULL DEFAULT NULL,
  `persona_id` int(11) NOT NULL,
  `rol_usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona_acceso`
--

INSERT INTO `persona_acceso` (`id`, `usuario`, `contrasena`, `create_time`, `update_time`, `persona_id`, `rol_usuario_id`) VALUES
(1, 'lavinia', '$2y$10$U7OCMSGVJN3Gc7oMlLPrceWGE1HtNwziQZpACfQUOZK1MpnGXWtrm', '2021-02-15 03:24:36', NULL, 3, 1),
(4, 'voluptatem', '$2y$10$/iwFIQmMdqSnZxXiydzLVuhxQexkdEkt.JQNSZpT93AiAJuQR9egu', '2021-05-25 05:01:31', NULL, 13, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_salario`
--

CREATE TABLE `persona_salario` (
  `id` int(11) NOT NULL,
  `persona_salario_monto` double NOT NULL,
  `persona_salario_estado` tinyint(4) DEFAULT 0 COMMENT '1. El salario actual de la persona',
  `persona_salario_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `persona_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona_salario`
--

INSERT INTO `persona_salario` (`id`, `persona_salario_monto`, `persona_salario_estado`, `persona_salario_created`, `persona_id`) VALUES
(1, 12000, 0, '2017-07-08 19:05:00', 1),
(2, 14000, 0, '2021-02-20 00:29:52', 1),
(3, 10000, 0, '2021-02-20 00:30:34', 4),
(9, 44, 1, '2021-05-24 23:28:16', 10),
(11, 8000, 1, '2021-05-25 04:58:48', 12),
(12, 8542, 0, '2021-05-25 05:01:30', 13),
(16, 10000, 1, '2021-06-03 01:07:56', 17),
(17, 15000, 1, '2021-06-03 23:59:56', 1),
(18, 12000, 1, '2021-06-04 00:00:07', 4),
(19, 9000, 0, '2021-06-04 00:01:06', 13),
(20, 9525.25, 1, '2021-06-04 00:02:25', 13),
(21, 9525, 1, '2021-06-04 00:03:58', 18),
(22, 15000, 1, '2021-06-04 04:44:23', 19);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_tipo`
--

CREATE TABLE `persona_tipo` (
  `id` int(11) NOT NULL,
  `persona_tipo_valor` varchar(45) NOT NULL COMMENT 'Cargo o Puesto'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona_tipo`
--

INSERT INTO `persona_tipo` (`id`, `persona_tipo_valor`) VALUES
(1, 'Socio'),
(2, 'Empleado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planilla`
--

CREATE TABLE `planilla` (
  `id` int(11) NOT NULL,
  `planilla_descripcion` varchar(45) NOT NULL,
  `planilla_desde` date NOT NULL,
  `planilla_hasta` date NOT NULL,
  `planilla_estado` tinyint(4) DEFAULT 1,
  `impuesto_renta_id` int(11) NOT NULL,
  `impuesto_inss_laboral` int(11) NOT NULL COMMENT 'el id que correponda al inss',
  `impuesto_inss_patronal` int(11) NOT NULL,
  `impuesto_inatec` int(11) NOT NULL,
  `creado_el` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planilla_detalle`
--

CREATE TABLE `planilla_detalle` (
  `id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `salario_id` int(11) NOT NULL,
  `vacaciones_acumuladas` int(11) DEFAULT NULL,
  `planilla_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamo`
--

CREATE TABLE `prestamo` (
  `id` int(11) NOT NULL,
  `fecha_solicitud` date NOT NULL,
  `monto` double NOT NULL,
  `saldo` double DEFAULT NULL,
  `monto_abono` double NOT NULL,
  `forma_pago` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1. Deducir de Salario\n2. Deducir de Subsidio\n3. Abono personal',
  `persona_id` int(11) NOT NULL,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `estado` tinyint(4) DEFAULT 1 COMMENT '1. Activo\n2. Atrasado\n3. Cancelado '
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamo_abono`
--

CREATE TABLE `prestamo_abono` (
  `id` int(11) NOT NULL,
  `fecha_abono` date NOT NULL,
  `monto` double NOT NULL,
  `saldo` double NOT NULL,
  `forma_pago` tinyint(1) DEFAULT NULL,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `prestamo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `produccion`
--

CREATE TABLE `produccion` (
  `id` int(11) NOT NULL,
  `produccion_fecha` datetime NOT NULL,
  `produccion_monto_total` double NOT NULL,
  `ciclos_total` int(11) NOT NULL,
  `pasajeros_total` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `created` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reportar la produccion del conductor por ciclo o por dia';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `produccion_ciclo`
--

CREATE TABLE `produccion_ciclo` (
  `id` int(11) NOT NULL,
  `ciclo_monto` float NOT NULL,
  `ciclo_pasajeros` int(11) NOT NULL DEFAULT 0,
  `ciclo_tipo` smallint(6) NOT NULL DEFAULT 1 COMMENT '1. Ciclo \n2. Dia',
  `sentido` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1. Abajo - Arriba\n2. Arriba - Abajo\n3. Completo',
  `ciclo_km` float DEFAULT NULL COMMENT 'Kilómetros recorridos por ciclo',
  `cantidad` smallint(6) NOT NULL DEFAULT 1 COMMENT 'Cantidad de ciclos (en caso de ser dia debe ser el dia se debe especificar)',
  `ciclo_notas` varchar(255) DEFAULT NULL,
  `ruta_id` int(11) NOT NULL,
  `conductor_id` int(11) NOT NULL,
  `produccion_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `producto_nombre` varchar(45) NOT NULL,
  `producto_descripcion` varchar(150) DEFAULT NULL,
  `producto_precio` double DEFAULT NULL,
  `producto_cantidad` int(11) DEFAULT 0 COMMENT 'Cantidad Actual',
  `producto_minimo` int(11) DEFAULT 0,
  `producto_entrada` int(11) DEFAULT 0 COMMENT 'Cantidad al Iniciar o Registrar producto',
  `producto_tipo_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `producto_nombre`, `producto_descripcion`, `producto_precio`, `producto_cantidad`, `producto_minimo`, `producto_entrada`, `producto_tipo_id`) VALUES
(1, 'Est qui neque quibu', 'Laudantium necessit', NULL, 0, 11, 0, 1),
(2, 'Nihil numquam sed id', 'Reprehenderit nemo ', NULL, 0, 2, 0, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_lote`
--

CREATE TABLE `producto_lote` (
  `id` int(11) NOT NULL,
  `lote_fecha` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'Fecha de Entrada',
  `lote_cantidad` int(11) NOT NULL,
  `lote_cantidad_actual` int(11) DEFAULT 0,
  `lote_precio` double NOT NULL,
  `lote_precio_venta` double NOT NULL,
  `producto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto_lote`
--

INSERT INTO `producto_lote` (`id`, `lote_fecha`, `lote_cantidad`, `lote_cantidad_actual`, `lote_precio`, `lote_precio_venta`, `producto_id`) VALUES
(1, '2021-02-25 07:54:29', 20, 20, 55.5, 65, 1),
(2, '2021-02-25 07:59:20', 5, 5, 70, 90, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_tipo`
--

CREATE TABLE `producto_tipo` (
  `id` int(10) UNSIGNED NOT NULL,
  `producto_tipo_descripcion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto_tipo`
--

INSERT INTO `producto_tipo` (`id`, `producto_tipo_descripcion`) VALUES
(1, 'Llantas'),
(2, 'Baterias'),
(3, 'Repuestos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL,
  `nombre_proveedor` varchar(255) NOT NULL,
  `contacto_proveedor` varchar(30) DEFAULT NULL COMMENT 'numeros de contacto',
  `detalle_proveedor` text DEFAULT NULL COMMENT 'detalles como persona a quien contactar y direccion'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id`, `nombre_proveedor`, `contacto_proveedor`, `detalle_proveedor`) VALUES
(1, 'Llantasa', '2268-3943', ''),
(2, 'Remasa', '2222-3762', 'Aceites y lubricantes en el Oriental ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puesto`
--

CREATE TABLE `puesto` (
  `id` int(11) NOT NULL,
  `puesto_nombre` varchar(60) NOT NULL,
  `puesto_descripcion` varchar(255) DEFAULT NULL,
  `puesto_estado` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puesto`
--

INSERT INTO `puesto` (`id`, `puesto_nombre`, `puesto_descripcion`, `puesto_estado`) VALUES
(1, 'Contador', NULL, 1),
(2, 'Conductor', NULL, 1),
(3, 'Secretaria', '', 1),
(4, 'Analista', 'Analista de barras electronicas', 1),
(5, 'Despachador', '', 1),
(6, 'Cajero', 'Editadoss', 1),
(7, 'testing', '---', 0),
(8, 'Auxiliar Contable', '', 1),
(9, 'Otro', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `rol_precio` double NOT NULL,
  `rol_created` timestamp NULL DEFAULT current_timestamp(),
  `rol_estado` tinyint(1) DEFAULT 1 COMMENT '1. Actual / Activo\n2. Anterior / Inactivo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_usuario`
--

CREATE TABLE `rol_usuario` (
  `id` int(11) NOT NULL,
  `rol_nombre` varchar(45) NOT NULL,
  `rol_descripcion` text DEFAULT NULL COMMENT '0 inactivo\n1 activo',
  `rol_estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol_usuario`
--

INSERT INTO `rol_usuario` (`id`, `rol_nombre`, `rol_descripcion`, `rol_estado`) VALUES
(1, 'Super Administrador', 'Administrador de todo el sistema', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ruta`
--

CREATE TABLE `ruta` (
  `id` int(11) NOT NULL,
  `ruta_nombre` varchar(25) NOT NULL,
  `ruta_descripcion` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion`
--

CREATE TABLE `seccion` (
  `id` int(11) NOT NULL,
  `seccion_nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `seccion`
--

INSERT INTO `seccion` (`id`, `seccion_nombre`) VALUES
(1, 'Gestionar Personal'),
(2, 'Gestionar Puestos'),
(3, 'Gestionar Planilla'),
(4, 'Gestionar Socios'),
(5, 'Gestionar Cuentas'),
(6, 'Gestionar Unidades de Transporte'),
(7, 'Gestionar Registros Contables'),
(8, 'Gestionar Gastos'),
(9, 'Gestionar Códigos'),
(10, 'Reporte TUC'),
(11, 'Gestionar Informes'),
(12, 'Gestionar Productos'),
(13, 'Gestionar Compras'),
(14, 'Inventario'),
(15, 'Facturar'),
(16, 'Gestionar Ciclos'),
(17, 'Asignar Ruta'),
(18, 'Gestionar Reporte SIBE'),
(19, 'Gestionar Roles'),
(20, 'Gestionar Permisos');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asiento`
--
ALTER TABLE `asiento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_asiento_persona1_idx` (`persona_id`),
  ADD KEY `fk_asiento_codigo_contable1_idx` (`codigo_contable_id`);

--
-- Indices de la tabla `asiento_anulacion`
--
ALTER TABLE `asiento_anulacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_asiento_anulacion_asiento1_idx` (`asiento_id`),
  ADD KEY `fk_asiento_anulacion_persona1_idx` (`persona_id`);

--
-- Indices de la tabla `bus`
--
ALTER TABLE `bus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bus_placa_UNIQUE` (`bus_placa`),
  ADD KEY `fk_bus_persona1_idx` (`persona_id`),
  ADD KEY `fk_bus_bus_marca1_idx` (`bus_marca_id`);

--
-- Indices de la tabla `bus_marca`
--
ALTER TABLE `bus_marca`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `codigo_contable`
--
ALTER TABLE `codigo_contable`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `constante`
--
ALTER TABLE `constante`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `constante_cambio`
--
ALTER TABLE `constante_cambio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_constante_idx` (`constante_id`);

--
-- Indices de la tabla `control_denominacion`
--
ALTER TABLE `control_denominacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_control_denominacion_control_diario1_idx` (`control_diario_id`);

--
-- Indices de la tabla `control_diario`
--
ALTER TABLE `control_diario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cooperativa`
--
ALTER TABLE `cooperativa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `diesel`
--
ALTER TABLE `diesel`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_factura_persona1_idx` (`facturado_por`),
  ADD KEY `fk_factura_persona2_idx` (`socio_id`);

--
-- Indices de la tabla `factura_diesel`
--
ALTER TABLE `factura_diesel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_factura_diesel_factura1_idx` (`factura_id`),
  ADD KEY `fk_factura_diesel_bus1_idx` (`bus_id`),
  ADD KEY `fk_factura_diesel_diesel1_idx` (`diesel_id`);

--
-- Indices de la tabla `factura_producto`
--
ALTER TABLE `factura_producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_factura_producto_producto1_idx` (`producto_id`),
  ADD KEY `fk_factura_producto_factura_rol1_idx` (`factura_rol_id`);

--
-- Indices de la tabla `factura_rol`
--
ALTER TABLE `factura_rol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_factura_rol_factura1_idx` (`factura_id`),
  ADD KEY `fk_factura_rol_bus1_idx` (`bus_id`),
  ADD KEY `fk_factura_rol_rol1_idx` (`rol_id`);

--
-- Indices de la tabla `impuesto_detalle`
--
ALTER TABLE `impuesto_detalle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_impuesto_renta_idx` (`impuesto_renta_id`);

--
-- Indices de la tabla `impuesto_renta`
--
ALTER TABLE `impuesto_renta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_permiso_secion1_idx` (`seccion_id`),
  ADD KEY `fk_permiso_rol_usuario1_idx` (`rol_usuario_id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_persona_persona_tipo_idx` (`persona_tipo_id`),
  ADD KEY `fk_persona_puesto1_idx` (`puesto_id`) USING BTREE;

--
-- Indices de la tabla `persona_acceso`
--
ALTER TABLE `persona_acceso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_persona_acceso_persona1_idx` (`persona_id`),
  ADD KEY `fk_persona_acceso_rol_usuario1_idx` (`rol_usuario_id`);

--
-- Indices de la tabla `persona_salario`
--
ALTER TABLE `persona_salario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_persona_salario_persona1_idx` (`persona_id`);

--
-- Indices de la tabla `persona_tipo`
--
ALTER TABLE `persona_tipo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `planilla`
--
ALTER TABLE `planilla`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_planilla_impuesto_idx` (`impuesto_renta_id`),
  ADD KEY `fk_constante_inss_idx` (`impuesto_inss_laboral`),
  ADD KEY `fk_constante_inss_patronal_idx` (`impuesto_inss_patronal`),
  ADD KEY `fk_constante_inatec_idx` (`impuesto_inatec`);

--
-- Indices de la tabla `planilla_detalle`
--
ALTER TABLE `planilla_detalle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_planilla_idx` (`planilla_id`),
  ADD KEY `fk_planilla_persona_idx` (`persona_id`),
  ADD KEY `fk_planilla_salario_idx` (`salario_id`);

--
-- Indices de la tabla `prestamo`
--
ALTER TABLE `prestamo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_prestamo_persona1_idx` (`persona_id`);

--
-- Indices de la tabla `prestamo_abono`
--
ALTER TABLE `prestamo_abono`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_prestamo_abono_prestamo1_idx` (`prestamo_id`);

--
-- Indices de la tabla `produccion`
--
ALTER TABLE `produccion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_produccion_bus1_idx` (`bus_id`);

--
-- Indices de la tabla `produccion_ciclo`
--
ALTER TABLE `produccion_ciclo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ciclo_ruta1_idx` (`ruta_id`),
  ADD KEY `fk_ciclo_persona1_idx` (`conductor_id`),
  ADD KEY `fk_produccion_ciclo_produccion1_idx` (`produccion_id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_producto_producto_tipo1_idx` (`producto_tipo_id`);

--
-- Indices de la tabla `producto_lote`
--
ALTER TABLE `producto_lote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_producto_lote_producto1_idx` (`producto_id`);

--
-- Indices de la tabla `producto_tipo`
--
ALTER TABLE `producto_tipo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `puesto`
--
ALTER TABLE `puesto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rol_usuario`
--
ALTER TABLE `rol_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ruta`
--
ALTER TABLE `ruta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `seccion`
--
ALTER TABLE `seccion`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `asiento`
--
ALTER TABLE `asiento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `asiento_anulacion`
--
ALTER TABLE `asiento_anulacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `bus`
--
ALTER TABLE `bus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `bus_marca`
--
ALTER TABLE `bus_marca`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `codigo_contable`
--
ALTER TABLE `codigo_contable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `constante`
--
ALTER TABLE `constante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `constante_cambio`
--
ALTER TABLE `constante_cambio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `control_denominacion`
--
ALTER TABLE `control_denominacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `control_diario`
--
ALTER TABLE `control_diario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cooperativa`
--
ALTER TABLE `cooperativa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `diesel`
--
ALTER TABLE `diesel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `factura_diesel`
--
ALTER TABLE `factura_diesel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `factura_producto`
--
ALTER TABLE `factura_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `factura_rol`
--
ALTER TABLE `factura_rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `impuesto_detalle`
--
ALTER TABLE `impuesto_detalle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `impuesto_renta`
--
ALTER TABLE `impuesto_renta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `permiso`
--
ALTER TABLE `permiso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `persona_acceso`
--
ALTER TABLE `persona_acceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `persona_salario`
--
ALTER TABLE `persona_salario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `persona_tipo`
--
ALTER TABLE `persona_tipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `planilla`
--
ALTER TABLE `planilla`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `planilla_detalle`
--
ALTER TABLE `planilla_detalle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `prestamo`
--
ALTER TABLE `prestamo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `prestamo_abono`
--
ALTER TABLE `prestamo_abono`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `produccion`
--
ALTER TABLE `produccion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `produccion_ciclo`
--
ALTER TABLE `produccion_ciclo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `producto_lote`
--
ALTER TABLE `producto_lote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `producto_tipo`
--
ALTER TABLE `producto_tipo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `puesto`
--
ALTER TABLE `puesto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rol_usuario`
--
ALTER TABLE `rol_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `ruta`
--
ALTER TABLE `ruta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `seccion`
--
ALTER TABLE `seccion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `asiento`
--
ALTER TABLE `asiento`
  ADD CONSTRAINT `fk_asiento_codigo_contable1` FOREIGN KEY (`codigo_contable_id`) REFERENCES `codigo_contable` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_asiento_persona1` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `asiento_anulacion`
--
ALTER TABLE `asiento_anulacion`
  ADD CONSTRAINT `fk_asiento_anulacion_asiento1` FOREIGN KEY (`asiento_id`) REFERENCES `asiento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_asiento_anulacion_persona1` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `bus`
--
ALTER TABLE `bus`
  ADD CONSTRAINT `fk_bus_bus_marca1` FOREIGN KEY (`bus_marca_id`) REFERENCES `bus_marca` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_bus_persona1` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `constante_cambio`
--
ALTER TABLE `constante_cambio`
  ADD CONSTRAINT `fk_constante` FOREIGN KEY (`constante_id`) REFERENCES `constante` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `control_denominacion`
--
ALTER TABLE `control_denominacion`
  ADD CONSTRAINT `fk_control_denominacion_control_diario1` FOREIGN KEY (`control_diario_id`) REFERENCES `control_diario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `fk_factura_persona1` FOREIGN KEY (`facturado_por`) REFERENCES `persona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_factura_persona2` FOREIGN KEY (`socio_id`) REFERENCES `persona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `factura_diesel`
--
ALTER TABLE `factura_diesel`
  ADD CONSTRAINT `fk_factura_diesel_bus1` FOREIGN KEY (`bus_id`) REFERENCES `bus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_factura_diesel_diesel1` FOREIGN KEY (`diesel_id`) REFERENCES `diesel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_factura_diesel_factura1` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `factura_producto`
--
ALTER TABLE `factura_producto`
  ADD CONSTRAINT `fk_factura_producto_factura_rol1` FOREIGN KEY (`factura_rol_id`) REFERENCES `factura_rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_factura_producto_producto1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `factura_rol`
--
ALTER TABLE `factura_rol`
  ADD CONSTRAINT `fk_factura_rol_bus1` FOREIGN KEY (`bus_id`) REFERENCES `bus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_factura_rol_factura1` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_factura_rol_rol1` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `impuesto_detalle`
--
ALTER TABLE `impuesto_detalle`
  ADD CONSTRAINT `fk_impuesto_renta` FOREIGN KEY (`impuesto_renta_id`) REFERENCES `impuesto_renta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD CONSTRAINT `fk_permiso_rol_usuario1` FOREIGN KEY (`rol_usuario_id`) REFERENCES `rol_usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_permiso_secion1` FOREIGN KEY (`seccion_id`) REFERENCES `seccion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `fk_persona_persona_tipo` FOREIGN KEY (`persona_tipo_id`) REFERENCES `persona_tipo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_persona_puesto1` FOREIGN KEY (`puesto_id`) REFERENCES `puesto` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `persona_acceso`
--
ALTER TABLE `persona_acceso`
  ADD CONSTRAINT `fk_persona_acceso_persona1` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_persona_acceso_rol_usuario1` FOREIGN KEY (`rol_usuario_id`) REFERENCES `rol_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `persona_salario`
--
ALTER TABLE `persona_salario`
  ADD CONSTRAINT `fk_persona_salario_persona1` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `planilla`
--
ALTER TABLE `planilla`
  ADD CONSTRAINT `fk_constante_inatec` FOREIGN KEY (`impuesto_inatec`) REFERENCES `constante_cambio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_constante_inss_laboral` FOREIGN KEY (`impuesto_inss_laboral`) REFERENCES `constante_cambio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_constante_inss_patronal` FOREIGN KEY (`impuesto_inss_patronal`) REFERENCES `constante_cambio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_planilla_impuesto` FOREIGN KEY (`impuesto_renta_id`) REFERENCES `impuesto_renta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `planilla_detalle`
--
ALTER TABLE `planilla_detalle`
  ADD CONSTRAINT `fk_planilla` FOREIGN KEY (`planilla_id`) REFERENCES `planilla` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_planilla_persona` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_planilla_salario` FOREIGN KEY (`salario_id`) REFERENCES `persona_salario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `prestamo`
--
ALTER TABLE `prestamo`
  ADD CONSTRAINT `fk_prestamo_persona1` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `prestamo_abono`
--
ALTER TABLE `prestamo_abono`
  ADD CONSTRAINT `fk_prestamo_abono_prestamo1` FOREIGN KEY (`prestamo_id`) REFERENCES `prestamo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `produccion`
--
ALTER TABLE `produccion`
  ADD CONSTRAINT `fk_produccion_bus1` FOREIGN KEY (`bus_id`) REFERENCES `bus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `produccion_ciclo`
--
ALTER TABLE `produccion_ciclo`
  ADD CONSTRAINT `fk_ciclo_persona1` FOREIGN KEY (`conductor_id`) REFERENCES `persona` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ciclo_ruta1` FOREIGN KEY (`ruta_id`) REFERENCES `ruta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_produccion_ciclo_produccion1` FOREIGN KEY (`produccion_id`) REFERENCES `produccion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_producto_tipo1_idx` FOREIGN KEY (`producto_tipo_id`) REFERENCES `producto_tipo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto_lote`
--
ALTER TABLE `producto_lote`
  ADD CONSTRAINT `fk_producto_lote_producto1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
