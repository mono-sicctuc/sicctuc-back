/*
* Configuracion DataTables - Lavinia
*/
var config_hidden = [
    {
        "targets": [ 0 ],
        "visible": false,
        "searchable": false
    },
];

var table_config = {
    dom: "Bfrtip",    
    buttons: [
    {
        extend: "copy",
        className: "btn-sm"
    },
    {
        extend: "csv",
        className: "btn-sm"
    },
    {
        extend: "excel",
        className: "btn-sm"
    },
    {
        extend: "pdfHtml5",
        className: "btn-sm"
    },
    {
        extend: "print",
        className: "btn-sm"
    },
    ],
    language: {
        url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
    },
    responsive: true,
    "order": [[ 1, "asc" ]],
    "columnDefs": config_hidden
};

var reload_table = true;

$(function(){
    PNotify.prototype.options.styling = "bootstrap3";
    PNotify.prototype.options.delay = 3000;
    PNotify.prototype.options.buttons.sticker = false;
    PNotify.prototype.options.buttons.sticker_hover = false;

    //Tooltip
    $('.right_col .navbar-right li > a').tooltip({
        //placement: 'left',
    });

    $('body').on('click', '.data-table tbody tr', function(){ 
        $parent = $(this).parents('.x_panel');
        $hidden = $parent.find('.hidden-options');
        var $parent_table = $(this).parents('.data-table');

        if ( $(this).hasClass('success') ) {
            $(this).removeClass('success');
            $hidden.hide();
            $hidden.find('a').removeAttr('data-id');

            $('.rol-permiso table > tbody').html('');
            $('.rol-permiso').slideUp('slow');

            $('a[data-action="add"], a[data-action="add-item"]').show();
        }
        else {
            $parent_table.find('tr.success').removeClass('success');
            $(this).addClass('success');
            $hidden.show();
            $('a[data-action="add"], a[data-action="add-item"]').hide();
        }
    });

    $('a[data-action="add-item"], a[data-action="edit-item"]').click(function(e){
        $this = $(this);
        var action = $this.attr('data-action');
        var update = false;

        if(action == 'edit-item')
            update = true;
        limpiar_form( $('form.send-form'), update );
    });

    //Datepicker
    var datepicker_locale = {
        "direction": "ltr",
        "format": "DD/MM/YYYY",
        "separator": " - ",
        "applyLabel": "Aplicar",
        "cancelLabel": "Cancelar",
        "fromLabel": "Del",
        "toLabel": "Al",
        "customRangeLabel": "Custom",
        "daysOfWeek": [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        "monthNames": [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ],
        "firstDay": 1
    };
    if( $('.datepicker').length ) {
        $('.datepicker').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3",
            locale: datepicker_locale,
            showDropdowns: true
        }, function(start, end, label) {
            //console.log(start.toISOString(), end.toISOString(), label);
        });
    }
    
    if( $('.range-datepicker').length ) {
        $('.range-datepicker').daterangepicker({
            locale: datepicker_locale
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
    }    
});

function limpiar_form($form, $update = false) {
    var id = $form.find('input[name="id"]').val();
    $form[0].reset();
    $('.icheckbox_flat-green').removeClass('checked');
    if($update)
        $form.find('input[name="id"]').val( id );
    else 
        $form.find('input[name="id"]').val("");
}

var sendResult = false;
function send_form($form, idLoading = '', table_obj = {}){
    url = $form.attr('action');
    data = $form.serialize();
    $.post(url, data, function(response){ 
        if(response.tipo == 1) {
            new PNotify({
                title: 'Correcto',
                text: 'Los cambios se han guardado correctamente',
                type: 'info'
            });

            $('.modal').modal('hide');

            $('a[data-action="add"], a[data-action="add-item"]').show();

            if(idLoading && table_obj) {
                reloadTable2(idLoading, table_obj);
                $form[0].reset();
            }
        }
        else {
            new PNotify({
                title: 'Error',
                text: response.texto,
                type: 'error'
            });
        } 
        //formCallback(response);
    }, 'json');
}
function formCallback(response){
    sendResult = false;
    if(response.tipo == 1)
        sendResult = true;
}

function reloadTable2(idloading, table_obj){
    //$('#procesandoPersonal').css( 'display', 'block' );
    $( idloading ).show();
    setTimeout(function(){
        //table_personal.ajax.reload(null, true);
        table_obj.ajax.reload(null, true);
    }, 1500);
    $('.navbar-right li a').show();
    $('.hidden-options').hide();
}

function formatoFecha(fecha)
{
    if( fecha === '' || fecha === undefined || fecha === null )
        return '';
    var fecha_split = fecha.split('-');
    var fecha_format = fecha_split[2] + "/" + fecha_split[1] + "/" + fecha_split[0];
    return fecha_format;
}