$(function(){
    $('body').on('click', 'a.delete-row', function(e){
        e.preventDefault();
        $this = $( this );
        $parent = $this.parent().parent()
        $parent.fadeOut(400, function(){
            $parent.remove();
        });        
    });

    var fila = '<tr>'+
        '<td><input type="number" step="0.01" name="monto_desde[]" class="form-control col-xs-12" min="0.01"></td>'+
        '<td><input type="number" step="0.01" name="monto_hasta[]" class="form-control col-xs-12" min="0"></td>'+
        '<td><input type="number" step="0.01" name="impuesto_base[]" class="form-control col-xs-12" min="0"></td>'+
        '<td><input type="number" step="0.01" name="porcentaje[]" class="form-control col-xs-12" min="0" max="100"></td>'+
        '<td><input type="number" step="0.01" name="sobre_exceso[]" class="form-control col-xs-12" min="0"></td>'+
        '<td><a href="#" class="delete-row"><i class="fa fa-trash"></i></a></td>'+
    '</tr>';

    $('a.add-row').click(function(e){
        e.preventDefault();
        $('#impuesto-table tbody').append( fila );
    });

    $('.send-form').on('submit', function(e){
        e.preventDefault();
        var $form = $(this);
        send_form($form);
        //console.log('enviar');
    });
});