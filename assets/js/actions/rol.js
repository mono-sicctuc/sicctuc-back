$(function(){
    //Tabla
    var rol_config = table_config;
    rol_columns =   [
            { "data": "id" },
            { "data": "rol_nombre" },
            { "data": "rol_descripcion" },
    ];
    rol_config.columns = rol_columns;
    rol_config.ajax = '/seguridad/roles/1';
    
    var table_rol = $('#datatable-rol')
    .on( 'processing.dt', function ( e, settings, processing ) {
        $('#procesandoRoles').css( 'display', processing ? 'block' : 'none' );        
    } )
    .DataTable(rol_config);

    $('.send-form').on('submit', function(e){
        e.preventDefault();
        var $form = $(this);
        send_form($form);
        
        reloadTable();
    });

    $('a[data-action="edit-item"]').on('click', function(){
        $this = $( this );
        var action = $this.attr('data-action');
        var $form = $('.send-form');

        limpiar_form($form);

        data = table_rol.row('.success').data();

        $form.find('input[name="id"]').val( data.id );
        $form.find('input[name="rol_nombre"]').val( data.rol_nombre );
        $form.find('input[name="rol_descripcion"]').val( data.rol_descripcion );
    });

    $('a[data-action="delete-item"]').on('click', function(e){
        e.preventDefault();
        data = table_rol.row('.success').data();
        id = data.id;

        (new PNotify({
            title: 'Confirmar',
            text: '¿Está seguro que desea eliminar el registro?',
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
        })).get().on('pnotify.confirm', function() {
            $.post('/seguridad/eliminar-rol/'+id, function(response){
                    if(response.tipo == 1) {
                        new PNotify({
                            title: 'Correcto',
                            text: 'Registro eliminado correctamente',
                            type: 'info'
                        });

                        reloadTable();
                    }
                    else {
                        new PNotify({
                            title: 'Error',
                            text: response.texto,
                            type: 'error'
                        });
                    }               
                }, 'json'
            );
        }).on('pnotify.cancel', function() {
            //alert('Oh ok. Chicken, I see.');
        });
    });

    $('a[data-action="permisos-item"]').on('click', function(e){
        e.preventDefault();

        data = table_rol.row('.success').data();
        id = data.id;
        url = '/seguridad/obtener-permisos/'+id;
        $('.rol-permiso').slideUp('slow');

        $.post( url, function(response){
            var table_body = '';
            if(response.length){                
                setTimeout(function(){
                    $('.rol-permiso table > tbody').html(response);
                    $('.rol-permiso').slideDown('slow');
                }, 1500);
            }
            else {
                $('.rol-permiso table > tbody').html('');
            }                
            
        }, 'json' );
    });

    $('body').on('click', 'input.toggle-todos', function(e){
        //e.preventDefault();
        $this = $( this );
        $td = $this.parent().parent();
        
        if ($this.is(':checked')) {
            $td.find('.check').prop('checked', true);
        }
        else {
            $td.find('.check').prop('checked', false);
        }
    });

    $('body').on('click', 'input.check', function(e){
        $this = $( this );
        $td = $this.parent().parent();
        $toggle = $td.find('input.toggle-todos');

        if ( !($td.is(':checked')) && $toggle.is(':checked') )
            $toggle.prop('checked', false);
        else if( $td.find('input.check:checked').length == 4 )
            $toggle.prop('checked', true);
    });

    //Actualizar los permisos
    $('a.actualizar-permisos').on('click', function(e){
        e.preventDefault();
    });

    function reloadTable(){
        $('#procesandoRoles').css( 'display', 'block' );
        setTimeout(function(){
            table_rol.ajax.reload(null, true);
        }, 1500);
        $('.hidden-options').hide();
    }
});