$(function(){
    //Tabla
    var variable_config = table_config;
    variable_columns =   [
            { "data": "id" },
            { "data": "constante_nombre" },
            { "data": "constante_slug" },
            { "data": "constante_valor" },
    ];
    variable_config.columns = variable_columns;
    variable_config.ajax = '/contabilidad/variables/1';
    
    var table_variable = $('#datatable-variable')
    .on( 'processing.dt', function ( e, settings, processing ) {
        $('#procesandoVariables').css( 'display', processing ? 'block' : 'none' );        
    } )
    .DataTable(variable_config);

    $('.send-form').on('submit', function(e){
        e.preventDefault();
        var $form = $(this);
        send_form($form);
        
        reloadTable();
    });

    $('a[data-action="edit-item"]').on('click', function(){
        $this = $( this );
        var $form = $('.send-form');

        limpiar_form($form);

        data = table_variable.row('.success').data();

        $form.find('input[name="id"]').val( data.id );
        $form.find('input[name="constante_nombre"]').val( data.constante_nombre );
        $form.find('input[name="constante_slug"]').val( data.constante_slug );
        $form.find('input[name="constante_valor"]').val( data.constante_valor );
    });

    function reloadTable(){
        $('#procesandoVariables').css( 'display', 'block' );
        setTimeout(function(){
            table_variable.ajax.reload(null, true);
        }, 1500);
        $('.hidden-options').hide();
    }
});