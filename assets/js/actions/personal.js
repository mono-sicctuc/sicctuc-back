$(function(){
    //Aqui van las acciones respesto a puestos
    //Tabla
    var personal_config = table_config;
    personal_columns =   [
            { "data": "id" },
            { "data": "persona_nombre" },
            { "data": "persona_apellido" },
            { "data": "persona_telefono" },
            { "data": "puesto_nombre" },
            { "data": "persona_salario" },
    ];
    personal_config.columns = personal_columns;
    personal_config.ajax = site+'personal/index/1';
    personal_config.columnDefs = [
        {
            "render": function (data, type, row) {
                if (row.persona_acceso === "1")
                    return data + ' <i class="fa fa-user"></i>';
                return data;            
            },
            "targets": 1
        },
        { "visible": false, "searchable": false, "targets": [0] }
    ];
    
    var table_personal = $('#datatable-personal')
    .on( 'processing.dt', function ( e, settings, processing ) {
        $('#procesandoPersonal').css( 'display', processing ? 'block' : 'none' );      
    } )
    .DataTable(personal_config);

    //Ocultar / Mostrar campos de acceso
    $('input[name="persona_acceso"]').on('ifChanged', function(event){ console.log(event);
        $this = $(this);        

        if( $this.is(':checked') ) {
            $('.extra-persona-fields input[name="usuario"], .extra-persona-fields select').prop('required', true);
            $('.extra-persona-fields').slideDown();
        }
        else {
            $('.extra-persona-fields input[name="usuario"], .extra-persona-fields select').prop('required', false);
            $('.extra-persona-fields').slideUp();
        }
    });

    //Rellenar Select de Puestos
    $.post(site+'personal/puestos/1', function(response){
            if(response.data.length) {
                select_options = '<option value=""></option>';
                $.each(response.data, function(index, value){
                    select_options += '<option value="'+ value.id +'">'+ value.puesto_nombre +'</option>';
                });
                $("#puesto_id, #puesto_id_editar").html( select_options );
            }
            else {
                $("#puesto_id, #puesto_id_editar").html('<option value="">No hay puestos activos</option>');
            }               
        }, 'json'
    );

    //Rellenar select del Rol
    $.post(site+'seguridad/roles/1', function(response){
        if(response.data.length) {
            select_options = '<option value=""></option>';
            $.each(response.data, function(index, value){
                select_options += '<option value="'+ value.id +'">'+ value.rol_nombre +'</option>';
            });
            $("#rol_id, #rol_id_editar").html( select_options );
        } 
        else {
            $("#rol_id, #rol_id_editar").html('<option value="">No hay roles activos</option>');
        }
    }, 'json');

    $('.send-form, .send-form-edit').on('submit', function(e){
        e.preventDefault();
        var $form = $(this);
        $('input[name="persona_acceso"]').change();
        send_form($form, '#procesandoPersonal', table_personal);
    });


    $('a[data-action="add-item"]').on('click', function(){
        $this = $( this );
        $('.extra-persona-fields').slideUp();

        $('.send-form').find('input[name="id"]').val( 0 );
        $('.send-form').find('input[name="salario_monto"]').parents('.form-group').show();
        $('.send-form').find('input[name="persona_ingreso"]').parents('.form-group').show();
    });

    $('a[data-action="edit-item"]').on('click', function(){
        $this = $( this );
        var $form = $('.send-form-edit');
        limpiar_form($form);
        data = table_personal.row('.success').data();

        $form.find('input[name="id"]').val(data.id);
        $form.find('input[name="persona_nombre"]').val( data.persona_nombre );
        $form.find('input[name="persona_apellido"]').val( data.persona_apellido );
        $form.find('input[name="persona_cedula"]').val(data.persona_cedula);
        nacimiento = formatoFecha(data.persona_nacimiento);
        $form.find('input[name="persona_nacimiento"]').val(nacimiento);
        $form.find('input[name="persona_telefono"]').val( data.persona_telefono );
        $form.find('input[name="persona_email"]').val( data.persona_email );
        $form.find('input[name="persona_direccion"]').val( data.persona_direccion );
        $form.find('select[name="puesto_id"]').val( data.puesto_id );        

        if( data.persona_acceso == 1 ) {
            $form.find('select[name="rol_id"]').val(data.rol_usuario_id);
            $form.find('input[name="usuario"]').val(data.usuario);
            $('.extra-persona-fields').slideDown();
            $('input[name="persona_acceso"]').prop('checked', true);
            $('.icheckbox_flat-green').addClass('checked');
        }
        else{
            $('.extra-persona-fields').slideUp();
            $('.icheckbox_flat-green').removeClass('checked');
        }
    });

    $('a[data-action="delete-item"]').on('click', function(e){
        e.preventDefault();
        $this = $(this);
        data = table_personal.row('.success').data();
        id = data.id;

        (new PNotify({
            title: 'Confirmar',
            text: '¿Está seguro que desea eliminar el registro?',
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
        })).get().on('pnotify.confirm', function() {
            var url = $this.data('target')+'/'+id;

            $.post(url, function(response){
                    if(response.tipo == 1) {
                        new PNotify({
                            title: 'Correcto',
                            text: 'Registro eliminado correctamente',
                            type: 'info'
                        });

                        reloadTable2("#procesandoPersonal", table_personal);
                    }
                    else {
                        new PNotify({
                            title: 'Error',
                            text: response.texto,
                            type: 'error'
                        });
                    }               
                }, 'json'
            );
        }).on('pnotify.cancel', function() {
            //alert('Oh ok. Chicken, I see.');
        });
    });

    $('a[data-action="aumentar-salario"').click(function(e){
        var $form = $('.send-form-salario');
        data = table_personal.row('.success').data();
        $form.find('input[name="persona_id"]').val( data.id );
        console.log(data);
        var salario_actual = parseFloat( data.persona_salario );
        salario_actual = ( salario_actual <= 0 || isNaN(salario_actual) ) ? '' : salario_actual;

        console.log(data.persona_salario);
        console.log(salario_actual);

        $form.find('.salario_actual').text( salario_actual );
        $form.find('input[name="persona_salario_monto"]').val( salario_actual );
        $form.find('input[name="persona_salario_monto"]').attr( 'min',  salario_actual );
    });

    $('.send-form-salario').on('submit', function(e){
        e.preventDefault();
        var $form = $(this);
        send_form($form, '#procesandoPersonal', table_personal);
        // send_form($form);
        // reloadTable2("#procesandoPersonal", table_personal);
        // $form[0].reset();
    });
});