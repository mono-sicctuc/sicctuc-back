$(function () {
    //Tabla
    var marca_config = table_config;
    marca_columns = [
        { "data": "id" },
        { "data": "bus_marca_nombre" },
    ];
    marca_config.columns = marca_columns;
    marca_config.ajax = site+'bus/marcas/1';

    var table_marca = $('#datatable-marca')
        .on('processing.dt', function (e, settings, processing) {
            $('#procesandoTabla').css('display', processing ? 'block' : 'none');
        })
        .DataTable(marca_config);

    //Enviar datos
    $('.send-form').on('submit', function (e) {
        e.preventDefault();
        var $form = $(this);
        var result = send_form($form);

        reloadTable2("#procesandoTabla", table_marca);
    });

    //Formulario Editar
    $('a[data-action="edit-item"]').on('click', function () {
        $this = $(this);
        var action = $this.attr('data-action');
        var $form = $('.send-form');

        limpiar_form($form);

        data = table_marca.row('.success').data();

        $form.find('input[name="id"]').val(data.id);
        $form.find('input[name="bus_marca_nombre"]').val(data.bus_marca_nombre);
    });

    //Eliminar
    $('a[data-action="delete-item"]').on('click', function (e) {
        e.preventDefault();
        data = table_marca.row('.success').data();
        id = data.id;

        (new PNotify({
            title: 'Confirmar',
            text: '¿Está seguro que desea eliminar el registro?',
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
        })).get().on('pnotify.confirm', function () {
            $.post(site+'bus/marcas/eliminar/' + id, function (response) {
                if (response.tipo == 1) {
                    new PNotify({
                        title: 'Correcto',
                        text: 'Registro eliminado correctamente',
                        type: 'info'
                    });

                    reloadTable2("#procesandoTabla", table_marca);
                }
                else {
                    new PNotify({
                        title: 'Error',
                        text: response.texto,
                        type: 'error'
                    });
                }
            }, 'json'
            );
        }).on('pnotify.cancel', function () {
            //alert('Oh ok. Chicken, I see.');
        });
    });

});