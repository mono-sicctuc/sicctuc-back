$(function(){
    //Tabla
    var bus_config = table_config;
    bus_columns = [
        { "data": "id" },
        { "data": "bus_placa" },
        { "data": "dueno" },        
        { "data": "marca" },
        { "data": "bus_modelo" },
        { "data": "bus_chasis" },
        { "data": "bus_motor" },
        { "data": "bus_circulacion" },
        { "data": "bus_color" },
    ];
    bus_config.columns = bus_columns;
    bus_config.ajax = site+'bus/index/1';

    var table_bus = $('#datatable-bus')
        .on('processing.dt', function (e, settings, processing) {
            $('#procesandoTabla').css('display', processing ? 'block' : 'none');
        })
        .DataTable(bus_config);

    //Autocomplete de persona
    $('#persona_nombre').autocomplete({
        serviceUrl: site+'socio/personas',
        onSelect: function (response) {
            $('input[name="persona_id"]').val(response.data);
            /*$.post('/prestamo/persona-tipo/' + response.data, function (data) {
                console.log(data);
                if (data.persona_tipo == 1 || data.persona_tipo == 2)
                    poblarFormaPago(data.persona_tipo);
            }, 'json');*/
        }
    });

    //Enviar datos
    $('.send-form').on('submit', function (e) {
        e.preventDefault();
        var $form = $(this);
        send_form($form); console.log(result);
        reloadTable2("#procesandoTabla", table_bus);
    });

    //Formulario Editar
    $('a[data-action="edit-item"]').on('click', function () {
        $this = $(this);
        var action = $this.attr('data-action');
        var $form = $('.send-form');

        limpiar_form($form);

        data = table_bus.row('.success').data();

        $form.find('input[name="id"]').val(data.id);
        $form.find('input[name="bus_placa"]').val(data.bus_placa);
        $form.find('input[name="bus_marca"]').val(data.bus_marca);
        $form.find('input[name="bus_modelo"]').val(data.bus_modelo);
        $form.find('input[name="bus_chasis"]').val(data.bus_chasis);
        $form.find('input[name="bus_motor"]').val(data.bus_motor);
        $form.find('input[name="bus_circulacion"]').val(data.bus_circulacion);
        $form.find('input[name="bus_color"]').val(data.bus_color);
        $form.find('input[name="persona_id"]').val(data.persona_id);
        $form.find('input[name="persona_nombre"]').val(data.dueno);
    });

    //Eliminar
    $('a[data-action="delete-item"]').on('click', function (e) {
        e.preventDefault();
        data = table_bus.row('.success').data();
        id = data.id;

        (new PNotify({
            title: 'Confirmar',
            text: '¿Está seguro que desea eliminar el registro?',
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
        })).get().on('pnotify.confirm', function () {
            $.post(site+'bus/eliminar/' + id, function (response) {
                if (response.tipo == 1) {
                    new PNotify({
                        title: 'Correcto',
                        text: 'Registro eliminado correctamente',
                        type: 'info'
                    });

                    reloadTable2("#procesandoTabla", table_bus);
                }
                else {
                    new PNotify({
                        title: 'Error',
                        text: response.texto,
                        type: 'error'
                    });
                }
            }, 'json'
            );
        }).on('pnotify.cancel', function () {
            //alert('Oh ok. Chicken, I see.');
        });
    });

});