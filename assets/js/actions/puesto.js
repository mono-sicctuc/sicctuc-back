$(function(){
    //Tabla
    var puesto_config = table_config;
    puesto_columns =   [
            { "data": "id" },
            { "data": "puesto_nombre" },
            { "data": "puesto_descripcion" },
    ];
    puesto_config.columns = puesto_columns;
    puesto_config.ajax = site+'personal/puestos/1';
    
    var table_puesto = $('#datatable-puesto')
    .on( 'processing.dt', function ( e, settings, processing ) {
        $('#procesandoPuestos').css( 'display', processing ? 'block' : 'none' );        
    } )
    .DataTable(puesto_config);

    $('.send-form').on('submit', function(e){
        e.preventDefault();
        var $form = $(this);
        send_form($form, "#procesandoPuestos", table_puesto);
        // send_form($form);
        // setTimeout(function(){
        //     if (reload_table)
        //         reloadTable();
        // }, 300);
    });

    $('a[data-action="edit-item"]').on('click', function(){
        $this = $( this );
        var action = $this.attr('data-action');
        var $form = $('.send-form');

        limpiar_form($form);

        data = table_puesto.row('.success').data();

        $form.find('input[name="id"]').val( data.id );
        $form.find('input[name="puesto_nombre"]').val( data.puesto_nombre );
        $form.find('input[name="puesto_descripcion"]').val( data.puesto_descripcion );
    });

    $('a[data-action="delete-item"]').on('click', function(e){
        e.preventDefault();
        data = table_puesto.row('.success').data();
        id = data.id;

        (new PNotify({
            title: 'Confirmar',
            text: '¿Está seguro que desea eliminar el registro?',
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
        })).get().on('pnotify.confirm', function() {
            $.post(site+'puesto/eliminar/'+id, function(response){
                    if(response.tipo == 1) {
                        new PNotify({
                            title: 'Correcto',
                            text: 'Registro eliminado correctamente',
                            type: 'info'
                        });

                        reloadTable();
                    }
                    else {
                        new PNotify({
                            title: 'Error',
                            text: response.texto,
                            type: 'error'
                        });
                    }               
                }, 'json'
            );
        }).on('pnotify.cancel', function() {
            //alert('Oh ok. Chicken, I see.');
        });
    });

    function reloadTable(){
        $('#procesandoPuestos').css( 'display', 'block' );
        setTimeout(function(){
            table_puesto.ajax.reload(null, true);
        }, 1500);
        $('.hidden-options').hide();
    }
});