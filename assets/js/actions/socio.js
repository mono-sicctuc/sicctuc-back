$(function(){
    //Tabla
    var socio_config = table_config;
    socio_columns =   [
            { "data": "id" },
            { "data": "persona_nombre" },
            { "data": "persona_apellido" },
            { "data": "persona_telefono" },
            { "data": "puesto_nombre" },
            { "data": "persona_salario" },
    ];
    socio_config.columns = socio_columns;
    socio_config.ajax = site+'socio/index/1';
    
    var table_socio = $('#datatable-socio')
    .on( 'processing.dt', function ( e, settings, processing ) {
        $('#procesandoSocios').css( 'display', processing ? 'block' : 'none' );      
    } )
    .DataTable(socio_config);

    //Rellenar Select de Puestos
    $.post(site+'personal/puestos/1', function(response){
        if(response.data.length) {
            //$("#puesto_id").html(response.data);
            select_options = '<option value=""></option>';
            $.each(response.data, function(index, value){
                select_options += '<option value="'+ value.id +'">'+ value.puesto_nombre +'</option>';
            });
            $("#puesto_id").html( select_options );
        }
        else {
            $("#puesto_id").html('<option value="">No hay puestos activos</option>');
        }               
    }, 'json');

    //Rellenar select del Rol
    $.post(site+'seguridad/roles/1', function(response){
        if(response.data.length) {
            select_options = '<option value=""></option>';
            $.each(response.data, function(index, value){
                select_options += '<option value="'+ value.id +'">'+ value.rol_nombre +'</option>';
            });
            $("#rol_id").html( select_options );
        } 
        else {
            $("#rol_id").html('<option value="">No hay roles activos</option>');
        }
    }, 'json');


    $('.send-form').on('submit', function(e){
        e.preventDefault();
        var $form = $(this);
        send_form($form);        
        reloadTable2('#procesandoSocios', table_socio);
    
        $form[0].reset();
    });

    $('a[data-action="add-item"]').on('click', function(){
        $('.send-form').find('input[name="id"]').val( 0 );
        $('.send-form').find('input[name="salario_monto"]').parents('.form-group').show();
        $('.send-form').find('input[name="persona_ingreso"]').parents('.form-group').show();
    });

    $('a[data-action="edit-item"]').on('click', function(){
        $this = $( this );
        var action = $this.attr('data-action');
        var $form = $('.send-form');

        limpiar_form($form);

        data = table_socio.row('.success').data();

        //console.log(data);

        $form.find('input[name="salario_monto"]').parents('.form-group').hide();
        $form.find('input[name="persona_ingreso"]').parents('.form-group').hide();

        $form.find('input[name="id"]').val( data.id );
        $form.find('input[name="persona_nombre"]').val( data.persona_nombre );
        $form.find('input[name="persona_apellido"]').val( data.persona_apellido );
        $form.find('input[name="persona_cedula"]').val(data.persona_cedula);
        nacimiento = formatoFecha(data.persona_nacimiento);
        $form.find('input[name="persona_nacimiento"]').val(nacimiento);
        $form.find('input[name="persona_telefono"]').val( data.persona_telefono );
        $form.find('input[name="persona_email"]').val( data.persona_email );
        $form.find('input[name="persona_direccion"]').val( data.persona_direccion );
        $form.find('select[name="puesto_id"]').val( data.puesto_id );
        $form.find('select[name="rol_id"]').val( data.rol_id );
        $form.find('input[name="usuario"]').val( data.usuario );
    });

    $('a[data-action="delete-item"]').on('click', function(e){
        e.preventDefault();
        $this = $(this);
        data = table_socio.row('.success').data();
        id = data.id;

        (new PNotify({
            title: 'Confirmar',
            text: '¿Está seguro que desea eliminar el registro?',
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
        })).get().on('pnotify.confirm', function() {
            var url = $this.data('target')+'/'+id;

            $.post(url, function(response){
                    if(response.tipo == 1) {
                        new PNotify({
                            title: 'Correcto',
                            text: 'Registro eliminado correctamente',
                            type: 'info'
                        });

                        reloadTable2('#procesandoSocios', table_socio);
                    }
                    else {
                        new PNotify({
                            title: 'Error',
                            text: response.texto,
                            type: 'error'
                        });
                    }               
                }, 'json'
            );
        }).on('pnotify.cancel', function() {
            //alert('Oh ok. Chicken, I see.');
        });
    });

    //Asignar Salario
    $('a[data-action="aumentar-salario"').click(function(e){
        var $form = $('.send-form-salario');
        data = table_socio.row('.success').data();
        $form.find('input[name="persona_id"]').val( data.id );
        console.log(data);
        var salario_actual = parseFloat( data.persona_salario );
        salario_actual = ( salario_actual <= 0 || isNaN(salario_actual) ) ? 1 : salario_actual;

        console.log(data.persona_salario);
        console.log(salario_actual);

        $form.find('.salario_actual').text( salario_actual );
        $form.find('input[name="persona_salario_monto"]').val( salario_actual );
        $form.find('input[name="persona_salario_monto"]').attr( 'min',  salario_actual );
    });

    $('.send-form-salario').on('submit', function(e){
        e.preventDefault();
        var $form = $(this);
        send_form($form);
        reloadTable2('#procesandoSocios', table_socio);
        $form[0].reset();
    });
});