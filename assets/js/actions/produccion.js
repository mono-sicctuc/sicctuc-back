$(function(){
    //Tabla
    var pr_config = table_config;
    pr_columns = [
        { "data": "id" },
        { "data": "bus_placa" },
        { "data": "produccion_monto_total" },
        { "data": "produccion_fecha" },
    ];
    pr_config.columns = pr_columns;
    pr_config.ajax = site+'produccion/index/1';
    pr_config.columnDefs = [
        {
            targets: 1,
            "render": function (data, type, row) {
                produccion_tipo = 'D&iacute;a';
                if (row.produccion_tipo == 2)
                    produccion_tipo = 'Ciclo';
                return produccion_tipo;
            }
        },
        {
            "targets": [0],
            "visible": false,
            "searchable": false
        },
    ];

    var table_produccion = $('#datatable-produccion')
        .on('processing.dt', function (e, settings, processing) {
            $('#procesandoTabla').css('display', processing ? 'block' : 'none');
        })
        .DataTable(pr_config);


    //Autocomplete de persona
    $('#persona_nombre').autocomplete({
        serviceUrl: site +'personal/produccion/conductores',
        dataType: 'json',
        onSelect: function (suggestion) {
            $('input[name="conductor_id"]').val(suggestion.data);
        }
    });

    // Autocompletar placa
    $('#bus_placa').autocomplete({
        lookup: placas,
        onSelect: function(suggestion) {
            $('input[name="bus_id"]').val(suggestion.data);
        }
    });

    // calcular pasajeros
    $("input[name=ciclo_monto_bruto]").on("keyup change", function(){
        var monto = $(this).val();
        var pasajeros = monto / pasaje;
        $("input[name=ciclo_pasajeros]").val(Math.floor(pasajeros));
        updateMontoNeto();
    });

    // calcular monto neto
    $("input[name=ciclo_gastos]").on("keyup change", function () {
        updateMontoNeto();
    });

    function updateMontoNeto() {
        var monto_bruto = $("input[name=ciclo_monto_bruto]").val();
        var gastos = $("input[name=ciclo_gastos]").val();
        var monto_neto = monto_bruto - gastos;
        $("input[name=ciclo_monto_neto]").val(monto_neto.toFixed(2));
    }

    //Ocultar / Mostrar campos segun tipo de ciclo
    $('input[name="produccion_tipo"]').on('ifChecked', function (event) {
        var tipo = $(this).val();
        
        $(".day-type-fields").toggleClass("hidden");
        $(".cicle-type-fields").toggleClass("hidden");


        // $this = $(this);

        // if ($this.is(':checked')) {
        //     $('.extra-persona-fields input[name="usuario"], .extra-persona-fields select').prop('required', true);
        //     $('.extra-persona-fields').slideDown();
        // }
        // else {
        //     $('.extra-persona-fields input[name="usuario"], .extra-persona-fields select').prop('required', false);
        //     $('.extra-persona-fields').slideUp();
        // }
    });

    // Save
    $('.send-form').on('submit', function (e) {
        e.preventDefault();
        var $form = $(this);
        $('input[name="produccion_ciclo"]').change();
        send_form($form, "#procesandoTabla", table_produccion);
    });

    $('.importar-produccion').on('click', function(e){
        e.preventDefault();
        $('.archivoCsv').trigger('click');
    });

    $('#archivoCsv').on('change', function(e){
        e.preventDefault();
        var $el = $(this);

        (new PNotify({
            title: 'Confirmar acción',
            text: '¿Está seguro de cargar el archivo seleccionado?',
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
            history: {
                history: false
            }
        })).get().on('pnotify.confirm', function () {
            $.blockUI();
            //Cargar archivo
            var data = new FormData();

            $.each($('#archivoCsv')[0].files, function (i, file) {
                data.append('archivoCsv', file);
            });

            //console.log(data);

            $.post({
                url: site+'produccion/carga/',
                data: data,
                processData: false, 
                contentType: false,
                dataType: 'json',
                success: function (response, status){
                    console.log(response);
                    $.unblockUI();

                    //Resetear input            
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();

                    if(response.exito !== undefined) {
                        new PNotify({
                            title: 'Correcto',
                            text: response.exito,
                            type: 'info'
                        });
                    } else {
                        new PNotify({
                            title: 'Error',
                            text: response.error,
                            type: 'error'
                        });
                    }
                }
            });
        }).on('pnotify.cancel', function () {
            //Resetear input            
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
        });
    });
});