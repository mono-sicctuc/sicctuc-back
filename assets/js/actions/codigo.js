$(function(){
    //Tabla
    var codigos_config = table_config;
    codigos_columns = [
        { "data": "id" },
        { "data": "codigo_valor" },
        { "data": "codigo_nombre" },
        { "data": "estado_resultado" }
        /*{ "data": "codigo_tipo" },
        { "data": "codigo_subtipo" },*/
    ];
    codigos_config.columns = codigos_columns;
    codigos_config.ajax = '/codigo/index/1';
    codigos_config.columnDefs = [
        {
            targets: 3,
            "render": function (data, type, row) {
                /*console.log(data);
                console.log(type);
                console.log(row);*/
                if(row.estado_resultado == 1)
                    return 'Estado de Resultado';
                else 
                    return 'Balance General';
            }
        },
        {
            "targets": [0],
            "visible": false,
            "searchable": false
        },
    ];

    var table_codigos = $('#datatable-codigo')
        .on('processing.dt', function (e, settings, processing) {
            $('#procesandoTabla').css('display', processing ? 'block' : 'none');
        })
        .DataTable(codigos_config);

    $('#codigo_tipo').on('change', function(){
        var tipo = $(this).val();
        
        if(tipo == 1) {
            $('#codigo_subtipo').html('<option value="">Seleccionar SubTipo</option>'+
            '<option value="1">Activo Circulante</option>'+
            '<option value="2">Activo Diferido</option>'+
            '<option value="3">Activo Fijo</option>');
        } else if(tipo == 2) {  
            $('#codigo_subtipo').html('<option value="">Seleccionar SubTipo</option>' +
            '<option value="4">Pasivo Corto Plazo</option>' +
            '<option value="5">Pasivo Largo Plazo</option>');
        } else if(tipo == 3) {
            $('#codigo_subtipo').html('<option value="6">Capital</option>');
        } else {
            $('#codigo_subtipo').html('<option value="">Primero Seleccionar Tipo</option>');
        }
    });

    $('input[name="tipo_cuenta"]').on('ifChanged', function(e){
        //console.log($('input[name="tipo_cuenta"]:checked').val());
        $this = $(this);
        if ($this.is(':checked')) {
            var valor = $('input[name="tipo_cuenta"]:checked').val();
            if(valor=='estado') {
                $('.balance_data').slideUp();
                $('.er_data').slideDown();
            } else {
                $('.balance_data').slideDown();
                $('.er_data').slideUp();
            }
        }
    });

    // edit item
    $('a[data-action="edit-item"]').on('click', function () {
        $this = $(this);
        var action = $this.attr('data-action');
        var $form = $('.send-form');

        limpiar_form($form);

        data = table_codigos.row('.success').data();

        $form.find('input[name="id"]').val(data.id);
        $form.find('input[name="codigo_valor"]').val(data.codigo_valor);
        $form.find('select[name="codigo_tipo"]').val(data.codigo_tipo);
        $form.find('select[name="codigo_tipo"]').trigger('change');
        $form.find('select[name="codigo_subtipo"]').val(data.codigo_subtipo);        
        $form.find('input[name="codigo_nombre"]').val(data.codigo_nombre);
        $form.find('input[name="codigo_descripcion"]').val(data.codigo_descripcion);
        $form.find('input[name="estado_resultado"]').val(data.estado_resultado);
        $form.find('select[name="estado_resultado_tipo"]').val(data.estado_resultado_tipo);

        $('.iradio_flat-green').removeClass('checked');
        if (data.estado_resultado == 1) {
            $('input[name="tipo_cuenta"]').val('estado');
            $('input[name="tipo_cuenta"]').change();
            
            $('label[data-id="estado"] .iradio_flat-green').addClass('checked');
            $('.balance_data').slideUp();
            $('.er_data').slideDown();
        }
        else {
            $('input[name="tipo_cuenta"]').val('balance');
            $('input[name="tipo_cuenta"]').change();

            $('label[data-id="balance"] .iradio_flat-green').addClass('checked');
            $('.balance_data').slideDown();
            $('.er_data').slideUp();
        }
    });

    $('.send-form').on('submit', function (e) {
        e.preventDefault();
        var $form = $(this);
        send_form($form);

        setTimeout(function () {
            if (reload_table){
                reloadTable2('#procesandoTabla', table_codigos);
                $form[0].reset();
                $('input[name="estado_resultado"]').change();
            }
        }, 300);        
    }); 
});