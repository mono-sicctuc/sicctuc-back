$(function(){
    var table_prestamo_detalle = {};
    

    //Tabla
    var prestamo_config = table_config;
    prestamo_columns =   [
            { "data": "id" },
            { "data": "persona_nombre_apellido" },
            { "data": "fecha_solicitud" },
            { "data": "monto" },
            { "data": "saldo" },
            { "data": "monto_abono" },
            { "data": "forma_pago" },
            { "data": "estado" },
    ];
    prestamo_config.columns = prestamo_columns;
    prestamo_config.ajax = '/prestamo/index/0/1';
    prestamo_config.columnDefs = [
        {
            targets: 6,
            "render": function (data, type, row) {
                /*console.log(data);
                console.log(type);
                console.log(row);*/
                forma_pago = 'Deducir de Salario';
                if (row.forma_pago ==  2) 
                    forma_pago = 'Deducir de Subsidio';
                else if(row.forma_pago == 3)
                    forma_pago = 'Pago Directo';
                return forma_pago;
            }            
        },
        {
            targets: 7,
            "render": function (data, type, row) {
                pago_estado = 'Activo';
                if (row.estado == 2)
                    pago_estado = 'Atrasado';
                else if (row.estado == 3)
                    pago_estado = 'Cancelado';
                return pago_estado;
            }
        },
        {
            "targets": [0],
            "visible": false,
            "searchable": false
        },
    ];
    
    var table_prestamo = $('#datatable-prestamo')
    .on( 'processing.dt', function ( e, settings, processing ) {
        $('#procesandoTabla').css( 'display', processing ? 'block' : 'none' );      
    } )
    .DataTable(prestamo_config);

    //Autocomplete de persona
    $('#persona_nombre').autocomplete({
        serviceUrl: '/prestamo/personas',
        onSelect: function(response) {
            //console.log(response);
            $('input[name="persona_id"]').val(response.data);
            $.post('/prestamo/persona-tipo/'+response.data, function(data){
                console.log(data);
                if(data.persona_tipo == 1 || data.persona_tipo == 2)
                    poblarFormaPago( data.persona_tipo );
            }, 'json');
        }
    });

    var forma_pago_socio = '<option value= "2" > Deducir de Subsidio</option>'+
        '<option value="3">Pago Directo</option>';

    var forma_pago_persona = '<option value="1">Deducir de Salario</option>' +
        '<option value="3">Pago Directo</option>';

    function poblarFormaPago(tipo){
        if(tipo == 1) 
            $('select[name="forma_pago"]').html(forma_pago_socio);
        else
            $('select[name="forma_pago"]').html(forma_pago_persona);
    }

    $('.send-form').on('submit', function (e) {
        e.preventDefault();
        var $form = $(this);
        var result = send_form($form);

        console.log(result);

        if (result === true){
            reloadTable2('#procesandoTabla', table_prestamo);
            $form[0].reset();
        }
    }); 

    $('a[data-action="aplicar-abono"]').on('click', function(e){
        data = table_prestamo.row('.success').data();
        if (data.estado == 3) {
            new PNotify({
                title: 'Atención',
                text: 'El préstamo ya fue cancelado',
                type: 'info'
            });
            e.stopPropagation();
        }
        else {
            $('#abono-form input[name="prestamo_id"]').val(data.id);
            $('.abono_minimo').text(data.monto_abono);
            $('#abono-form input[name="monto_abono"]').val(data.monto_abono);
        }
    });

    var tabla_detalle = $("#datatable-prestamo-detalle").DataTable({
        "ajax": "/prestamo/detalle/1",
        "columns": [
            { "data": "id" },
            { "data": "prestamo_id" },
            { "data": "fecha_abono" },
            { "data": "forma_pago" },
            { "data": "monto" },
            { "data": "saldo" },
            { "data": "created" },            
        ],
        "columnDefs": [
            {
                "targets": [0, 1, -1],
                "visible": false,
                "searchable": false
            },
            {
                "render": function(data, type, row) {
                    switch (data.forma_pago) {
                        case 1:
                            return "Deducir de Salario";                            
                        case 2:
                            return "Deducir de Subsidio";                            
                    
                        default:
                            return "Abono Personal";                            
                    }                    
                },
                "targets": 3
            }
        ]
    });   

    $('a[data-action="open-detalle"]').on('click', function(e){
        data = table_prestamo.row('.success').data();
        tabla_detalle.ajax.url('/prestamo/detalle/'+data.id).load();
        //tabla_detalle.ajax.reload();
        tabla_detalle.columns.adjust().draw();
    });
});