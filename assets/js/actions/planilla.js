$(function(){
    //Aqui van las acciones respesto a puestos
    //Tabla
    var planilla_config = table_config;
    planilla_columns =   [
            { "data": "id" },
            { "data": "planilla_desde" },
            { "data": "planilla_hasta" },
            { "data": "creado_el" },
    ];
    planilla_config.columns = planilla_columns;
    planilla_config.ajax = site+'planilla/lista';
    
    var table_planilla = $('#datatable-planillas')
    .on( 'processing.dt', function ( e, settings, processing ) {
        $('#procesandoPlanilla').css( 'display', processing ? 'block' : 'none' );      
    } )
    .DataTable(planilla_config);
});