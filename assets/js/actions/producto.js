$(function(){
    var producto_config = table_config;
    producto_columns = [
        { "data": "id" },
        { "data": "producto_nombre" },
        { "data": "producto_descripcion" },
        { "data": "producto_cantidad" },
        { "data": "producto_minimo" },
        { "data": "producto_precio" },
    ];
    producto_config.columns = producto_columns;
    producto_config.ajax = site + 'producto/index/1';

    var table_producto = $('#datatable-producto')
        .on('processing.dt', function (e, settings, processing) {
            $('#procesandoProducto').css('display', processing ? 'block' : 'none');
        })
        .DataTable(producto_config);

    $('.send-form, .send-form-edit').on('submit', function (e) {
        e.preventDefault();
        var $form = $(this);
        send_form($form, "#procesandoProducto", table_producto);
    });


    //Formulario Editar
    $('a[data-action="edit-item"]').on('click', function () {
        $this = $(this);
        var action = $this.attr('data-action');
        var $form = $('.send-form-edit');

        limpiar_form($form);

        data = table_producto.row('.success').data();

        $form.find('input[name="id"]').val(data.id);
        $form.find('select[name="producto_tipo"]').val(data.producto_tipo_id);
        $form.find('input[name="producto_nombre"]').val(data.producto_nombre);
        $form.find('input[name="producto_descripcion"]').val(data.producto_descripcion);
        $form.find('input[name="producto_minimo"]').val(data.producto_minimo);
    });
});