$(function(){
    var proveedor_config = table_config;
    proveedor_columns = [
        { "data": "id" },
        { "data": "nombre_proveedor" },
        { "data": "contacto_proveedor" },
        { "data": "detalle_proveedor" },
    ];
    proveedor_config.columns = proveedor_columns;
    proveedor_config.ajax = site + 'proveedor/index/1';

    var table_proveedor = $('#datatable-proveedor')
        .on('processing.dt', function (e, settings, processing) {
            $('#procesandoProveedor').css('display', processing ? 'block' : 'none');
        })
        .DataTable(proveedor_config);

    $('.send-form, .send-form-edit').on('submit', function (e) {
        e.preventDefault();
        var $form = $(this);
        send_form($form, "#procesandoProveedor", table_proveedor);
    });


    //Formulario Editar
    $('a[data-action="edit-item"]').on('click', function () {
        $this = $(this);
        var action = $this.attr('data-action');
        var $form = $('.send-form-edit');

        limpiar_form($form);

        data = table_proveedor.row('.success').data();

        $form.find('input[name="id"]').val(data.id);
        $form.find('input[name="nombre_proveedor"]').val(data.nombre_proveedor);
        $form.find('input[name="contacto_proveedor"]').val(data.contacto_proveedor);
        $form.find('input[name="detalle_proveedor"]').val(data.detalle_proveedor);
    });

    //Eliminar
    $('a[data-action="delete-item"]').on('click', function (e) {
        e.preventDefault();
        data = table_proveedor.row('.success').data();
        id = data.id;

        (new PNotify({
            title: 'Confirmar',
            text: '¿Está seguro que desea eliminar el registro?',
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
        })).get().on('pnotify.confirm', function () {
            $.post(site + 'proveedor/eliminar/' + id, function (response) {
                if (response.tipo == 1) {
                    new PNotify({
                        title: 'Correcto',
                        text: 'Registro eliminado correctamente',
                        type: 'info'
                    });

                    reloadTable2("#procesandoTabla", table_proveedor);
                }
                else {
                    new PNotify({
                        title: 'Error',
                        text: response.texto,
                        type: 'error'
                    });
                }
            }, 'json'
            );
        }).on('pnotify.cancel', function () {
            //alert('Oh ok. Chicken, I see.');
        });
    });
});