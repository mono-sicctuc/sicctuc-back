$(function(){
    var productotipo_config = table_config;
    productotipo_columns = [
        { "data": "id" },
        { "data": "producto_tipo_descripcion" },
    ];
    productotipo_config.columns = productotipo_columns;
    productotipo_config.ajax = site + 'productotipo/index/1';

    var table_productotipo = $('#datatable-producto')
        .on('processing.dt', function (e, settings, processing) {
            $('#procesandoTipo').css('display', processing ? 'block' : 'none');
        })
        .DataTable(productotipo_config);

    $('.send-form, .send-form-edit').on('submit', function (e) {
        e.preventDefault();
        var $form = $(this);
        send_form($form, "#procesandoTipo", table_productotipo);
    });


    //Formulario Editar
    $('a[data-action="edit-item"]').on('click', function () {
        $this = $(this);
        var action = $this.attr('data-action');
        var $form = $('.send-form-edit');

        limpiar_form($form);

        data = table_productotipo.row('.success').data();

        $form.find('input[name="id"]').val(data.id);
        $form.find('input[name="producto_tipo_descripcion"]').val(data.producto_tipo_descripcion);
    });

    //Eliminar
    $('a[data-action="delete-item"]').on('click', function (e) {
        e.preventDefault();
        data = table_productotipo.row('.success').data();
        id = data.id;

        (new PNotify({
            title: 'Confirmar',
            text: '¿Está seguro que desea eliminar el registro?',
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
        })).get().on('pnotify.confirm', function () {
            $.post(site + 'productotipo/eliminar/' + id, function (response) {
                if (response.tipo == 1) {
                    new PNotify({
                        title: 'Correcto',
                        text: 'Registro eliminado correctamente',
                        type: 'info'
                    });

                    reloadTable2("#procesandoTabla", table_productotipo);
                }
                else {
                    new PNotify({
                        title: 'Error',
                        text: response.texto,
                        type: 'error'
                    });
                }
            }, 'json'
            );
        }).on('pnotify.cancel', function () {
            //alert('Oh ok. Chicken, I see.');
        });
    });
});