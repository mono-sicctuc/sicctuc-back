<div class="modal fade formulario-registro-producto" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Registrar Producto</h4>
      </div>
      <div class="modal-body">
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="producto_nombre">Nombre<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="producto_nombre" name="producto_nombre" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="producto_descripcion">Descripción
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="producto_descripcion" name="producto_descripcion" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label for="producto_precio" class="control-label col-md-3 col-sm-3 col-xs-12">Precio <span>*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="producto_precio" class="form-control col-md-7 col-xs-12" type="number" name="producto_precio" required="required" min="1">
            </div>
          </div>
          <div class="form-group">
            <label for="producto_cantidad" class="control-label col-md-3 col-sm-3 col-xs-12">En Existencias</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="producto_cantidad" class="form-control col-md-7 col-xs-12" type="number" name="producto_cantidad" min="0">
            </div>
          </div>
          <div class="form-group">
            <label for="producto_minimo" class="control-label col-md-3 col-sm-3 col-xs-12">M&iacute;nimo <span>*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="producto_minimo" class="form-control col-md-7 col-xs-12" type="number" min="1" name="producto_minimo" required="required">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="producto_vence">Fecha de Vencimiento <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" class="form-control has-feedback-left single-date" id="single_cal2" placeholder="Seleccionar Fecha" name="producto_vence" id="producto_vence" aria-describedby="inputSuccess2Status2">
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
              <span id="inputSuccess2Status2" class="sr-only">(success)</span>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button class="btn btn-primary" type="reset">Limpiar</button>
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
          </div>
        </form>        
      </div>
    </div>
  </div>
</div>