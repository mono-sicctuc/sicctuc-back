<div class="modal fade formulario-registro-gasto" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Registrar Gasto</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre <span class="required">*</span></label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" class="form-control" placeholder="Nombre del Gasto" required="required" name="gasto_nombre">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripci&oacute;n</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <textarea class="form-control" rows="3" placeholder="Descripci&oacute;n del Gasto" name="gasto_comentario"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Monto <span class="required">*</span></label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="number" class="form-control" placeholder="Monto del Gasto" required="required" name="gasto_monto">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha <span class="required">*</span></label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" class="form-control has-feedback-left single-date" id="single_cal2" placeholder="Seleccionar Fecha" required="required" name="gasto_fecha" aria-describedby="inputSuccess2Status2">
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
              <span id="inputSuccess2Status2" class="sr-only">(success)</span>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tipo de Gasto <span class="required">*</span></label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <select class="form-control" name="gasto_tipo" required="required">
                <option>Choose option</option>
                <option>Option one</option>
                <option>Option two</option>
                <option>Option three</option>
                <option>Option four</option>
              </select>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="reset" class="btn btn-primary">Limpiar</button>
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>