<div class="modal fade formulario-editar-privilegio" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Editar Privilegio</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="privilegio_seccion">Secci&oacute;n<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="privilegio_seccion" required="required" readonly="readonly" value="Gestionar Usuarios" name="privilegio_seccion" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="privilegio_permiso">Permiso<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="checkbox">
                    <label><input name="privilegio_permiso[]" type="checkbox" value="R"> Listar</label>
                </div>
                <div class="checkbox">
                    <label><input name="privilegio_permiso[]" type="checkbox" value="C"> Registrar</label>
                </div>
                <div class="checkbox">
                    <label><input name="privilegio_permiso[]" type="checkbox" value="U"> Editar</label>
                </div>
                <div class="checkbox">
                    <label><input name="privilegio_permiso[]" type="checkbox" value="D"> Eliminar</label>
                </div> 
            </div>
          </div>

          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="reset" class="btn btn-primary">Limpiar</button>
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>