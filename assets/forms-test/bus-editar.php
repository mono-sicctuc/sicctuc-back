<div class="modal fade formulario-editar-bus" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Editar Bus</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bus_placa">Placa<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="bus_placa" required="required" name="bus_placa" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bus_estado">Estado<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p>Activo
                   <input type="radio" class="flat" name="bus_estado" id="bus_estado_activo" value="0" checked="checked" required /> 
                   Inactivo
                   <input type="radio" class="flat" name="bus_estado" id="bus_estado_inactivo" value="1" />
                </p>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="socio_id">Socio<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="socio_id" required="required" name="socio_id" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="reset" class="btn btn-primary">Limpiar</button>
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>