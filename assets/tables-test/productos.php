<table id="datatable-buttons" class="table table-striped table-bordered">
  <thead>
    <tr>
      <th>Nombre</th>
      <th>Descripci&oacute;n</th>
      <th>Precio</th>
      <th>Cantidad</th>
      <th>M&iacute;nimo</th>
    </tr>
  </thead>

  <tbody>
    <?php for($i = 1; $i <= 100; $i++):  $gasto = ($i * rand(50, 1000)); ?>
    <tr>
      <td>Producto <?php echo $i ?></td>
      <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit <?php echo $i ?></td>
      <td><?php echo number_format($gasto, 2) ?></td>
      <td><?php echo rand(0, 10) ?></td>
      <td><?php echo rand(0, 10) ?></td>
    </tr>
    <?php endfor; ?>
  </tbody>
</table>