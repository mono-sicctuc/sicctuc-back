<table id="datatable-buttons" class="table table-striped table-bordered">
  <thead>
    <tr>
      <th>Sección</th>
      <th>Permisos</th>
    </tr>
  </thead>

  <tbody>
    <tr>
      <td>Gestionar Usuarios</td>
      <td>Crear, Editar, Eliminar</td>
    </tr>

    <tr>
      <td>Gestionar Buses</td>
      <td>Crear, Editar</td>
    </tr>

    <tr>
      <td>Gestionar Gastos</td>
      <td>Crear, Editar, Eliminar</td>
    </tr>

    <tr>
      <td>Gestionar Productos</td>
      <td>Listar</td>
    </tr>
  </tbody>
</table>