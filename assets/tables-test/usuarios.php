<table id="datatable-buttons" class="table table-striped table-bordered">
  <thead>
    <tr>
      <th>Usuario</th>
      <th>Nombres</th>
      <th>Apellidos</th>
      <th>Tel&eacute;fono</th>
    </tr>
  </thead>

  <tbody>
    <?php for($i = 1; $i <= 20; $i++): ?>
    <tr>
      <td>usuario_<?php echo $i ?></td>
      <td>Lorem ipsum<?php echo $i ?></td>
      <td>Dolor sit amet</td>
      <td><?php echo rand(22222222, 22999999) ?></td>
    </tr>
    <?php endfor; ?>
  </tbody>
</table>