<table id="datatable-buttons" class="table table-striped table-bordered">
  <thead>
    <tr>
      <th>Nombre</th>
      <th>Descripci&oacute;n</th>
      <th>Monto</th>
      <th>Fecha</th>
      <th>Tipo</th>
    </tr>
  </thead>

  <tbody>
    <?php for($i = 1; $i <= 100; $i++):  $gasto = ($i * rand(1000, 5000)); ?>
    <tr>
      <td>Gasto <?php echo $i ?></td>
      <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit <?php echo $i ?></td>
      <td><?php echo number_format($gasto, 2) ?></td>
      <td><?php echo date('d-m-Y'); ?></td>
      <td>Tipo de Gasto</td>
    </tr>
    <?php endfor; ?>
  </tbody>
</table>