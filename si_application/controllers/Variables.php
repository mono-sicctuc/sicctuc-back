<?php
class Variables extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('constante_model', 'constante');
        $this->load->model('constantecambio_model', 'cambio');
    }

    /**
     * @param boolean 
     */
    public function index($ajax = false)
    {
        $constantes = $this->constante->getConstantes();

        if($ajax) 
            output_json(array('data' => $constantes));
        else 
        {
            $data['titulo'] = 'Variables Predefinidas';
			$data['constantes'] = $constantes;
			//habilitar plugins
			//$data['enable_icheck'] = true;
			$data['js_file'] = 'variables.js';

			$views = array(
				'contabilidad/lista_variables',
				'contabilidad/formulario_variables'
			);
			use_template($views, $data);
        }
    }

    public function guardar()
    {
        //TODO:Validacion
        $this->constante->constante_nombre = $_POST['constante_nombre'];
        $this->constante->constante_slug = $_POST['constante_slug'];

		if( !empty($_POST['id']) )
		{
            //Actualizar los datos generales de la constante
            $this->constante->id = $_POST['id'];
            $this->constante->actualizar();

            //Insertar un nuevo valor y poner en estado 0 todos los demas
            $this->cambio->constante_id = $_POST['id'];
            $this->cambio->inhabilitarConstantes();
            
            $this->cambio->constante_valor = $_POST['constante_valor'];
            $this->cambio->insertar();
        }		
            
        $resultado = array(
            'tipo' => 1
        );

        output_json($resultado);
    }
}