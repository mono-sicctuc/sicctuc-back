<?php
class Prestamo extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('prestamo_model', 'prestamo');
        $this->load->model('prestamoabono_model', 'abono');
        $this->load->model('persona_model', 'persona');
    }

    public function index($tipo = 0, $ajax = false)
    {
        $this->prestamo->estado = $tipo;
        $prestamos = $this->prestamo->getPrestamos();

        if( $ajax )
            output_json(array('data' => $prestamos));
        else
        {
            $data['titulo'] = 'Gestionar Préstamos';
			$data['prestamos'] = $prestamos;
			//habilitar plugins
            $data['enable_datepicker'] = true;
            $data['enable_autocomplete'] = true;
            
			$data['js_file'] = 'prestamo.js';

			$views = array(
				'contabilidad/prestamo_lista',
                'contabilidad/prestamo_form',
                'contabilidad/prestamo_abono_form'
			);
			use_template($views, $data);
        }
    }

    /**
     * Retornar lista de personas
     */
    public function personas()
    {  
        $find = trim( $_GET['query'] );
        $personas = $this->persona->getAll( $find );

        $result = array(
            'query' => 'Unit',
            'suggestions' => $personas
        );

        output_json( $result );
    }

    /**
     * Retornar el tipo de persona (socio o personal)
     */
    public function personaTipo($persona_id)
    {
        $persona_id = (int) $persona_id;
        
        $this->persona->id = $persona_id;
        $resultado = $this->persona->getPersonaTipo();

        if(!empty($resultado['persona_tipo_id']))
            output_json(array('persona_tipo' => $resultado['persona_tipo_id']));
        else
            output_json(array('pe   rsona_tipo' => 0));
    }

    public function guardar()
    {

        $config = array(
            array(
                'field' => 'persona_nombre',
                'label' => 'Nombres',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'fecha_solicitud',
                'label' => 'Fecha de Solicitud',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'monto',
                'label' => 'Monto del Préstamo',
                'rules' => 'trim|required|integer'
            ),
            array(
                'field' => 'monto_abono',
                'label' => 'Cuota',
                'rules' => 'trim|required|integer'
            ),
            array(
                'field' => 'forma_pago',
                'label' => 'Forma de Pago',
                'rules' => 'trim|required'
            )
        ); 

        $this->form_validation->set_rules($config);

        if( $this->form_validation->run() == FALSE )
        {
            output_json(array('tipo' => 2, 'texto' => $this->form_validation->error_string() ));
        }        
        else 
        {
            list($dia, $mes, $anio) = explode( '/', $_POST['fecha_solicitud'] );
            $fecha_solicitud = $anio.'-'.$mes.'-'.$dia;
    
            $this->prestamo->fecha_solicitud = $fecha_solicitud;
            $this->prestamo->monto = (float) $_POST['monto'];
            $this->prestamo->saldo = (float) $_POST['monto'];
            $this->prestamo->monto_abono = (float) $_POST['monto_abono'];
            $this->prestamo->forma_pago = (int) $_POST['forma_pago'];
            $this->prestamo->persona_id = (int) $_POST['persona_id'];
            $this->prestamo->guardar();
            output_json(array('tipo' => 1));
        }
        
    }

    /**
     * Guardar abono de la persona
     */
    public function guardarAbono()
    {
        //TODO: Validacion
        //TODO: Validar el monto del abono
        /**
         * Si el abono es mayor o menor al abono restante, etc
         */
        $this->prestamo->id = (int) $_POST['prestamo_id'];
        $monto_abono = (float) $_POST['monto_abono'];
        $saldo_anterior = $this->prestamo->obtenerSaldo();
        $nuevo_saldo = $saldo_anterior - $monto_abono;

        //Registrar pago de abono
        //$dias_retraso = $this->prestamo->calcularDias();
        $this->abono->fecha_abono = date('Y-m-d');
        $this->abono->monto = $monto_abono;
        $this->abono->saldo = $nuevo_saldo;
        $this->abono->forma_pago = 3;
        $this->abono->prestamo_id = (int) $_POST['prestamo_id'];
        $this->abono->guardar();

        //Actualizar saldo del prestamo
        $this->prestamo->saldo = $nuevo_saldo;
        $this->prestamo->actualizarSaldo();

        //Actualizar estado del prestamo
        if($nuevo_saldo <= 0)
        {
            $this->prestamo->estado = 3;
            $this->prestamo->actualizarEstado();
        }

        output_json(array('tipo' => 1));
    }

    /**
     * Obtener Detalle
     */
    public function obtenerDetalle($prestamo_id)
    {
        $this->abono->prestamo_id = $prestamo_id;
        $detalle = $this->abono->obtenerDetalle();
        //output_json($detalle);
        output_json(array('data' => $detalle));
    }
}