<?php
class Codigo extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('codigocontable_model', 'codigo');
    }

    public function index($ajax = false)
    {
        $codigos = $this->codigo->getCodigos();
        if($ajax)
            output_json(array('data' => $codigos));
        else 
        {
            $data['titulo'] = 'Gestionar Códigos Contables / Cuentas';
			$data['codigos'] = $codigos;
			//habilitar plugins
            $data['enable_icheck'] = true;
			$data['js_file'] = 'codigo.js';

			$views = array(
				'codigo/lista',
				'codigo/modals'
			);
			use_template($views, $data);
        }
    }

    public function guardar()
    {
        //Set Rule for codigo_nombre
        $this->form_validation->set_rules('codigo_nombre', 'Nombre', 'trim|required');
        
        $this->codigo->codigo_nombre = trim($_POST['codigo_nombre']);
        $this->codigo->codigo_descripcion = trim($_POST['codigo_descripcion']);
        if($_POST['tipo_cuenta'] == 'estado') 
        {
            //Set Rules for this case
            $this->form_validation->set_rules('estado_resultado_tipo', 'Tipo de Registro', 'required|integer|greater_than[0]');

            $this->codigo->estado_resultado = 1;
            $this->codigo->estado_resultado_tipo = (!empty($_POST['estado_resultado_tipo'])) ? $_POST['estado_resultado_tipo'] : 0;
        }
        else
        {
            //Set rules for this case
            $this->form_validation->set_rules('codigo_valor', 'Código de la Cuenta', 'trim|required');
            $this->form_validation->set_rules('codigo_tipo', 'Tipo de Cuenta', 'required|integer|greater_than[0]');
            $this->form_validation->set_rules('codigo_subtipo', 'Subtipo de Cuenta', 'required|integer|greater_than[0]');

            $this->codigo->codigo_valor = $_POST['codigo_valor'];
            $this->codigo->codigo_tipo = $_POST['codigo_tipo'];
            $this->codigo->codigo_subtipo = $_POST['codigo_subtipo'];
        }

        //Run Validation
        if( $this->form_validation->run() == FALSE )
        {
            output_json(['tipo' => 2, 'texto' => $this->form_validation->error_string()]);
        }
        else 
        {
            if( !empty($_POST['id']) )
            {	//Existente
                $this->codigo->id = (int) $_POST['id'];
                $this->codigo->actualizar();
            }
            else
                $this->codigo->insertar();

            output_json(array('tipo' => 1));
        }        
    }
}