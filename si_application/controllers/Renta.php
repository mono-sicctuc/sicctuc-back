<?php 
class Renta extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('impuestorenta_model', 'renta');
        $this->load->model('impuestodetalle_model', 'rentad');
    }

    public function index($ajax = false)
    {
        $rangos = $this->rentad->obtenerRangos(); 
        //var_dump($rangos); die();

        if($ajax) 
            output_json(array('data' => $rangos));
        else 
        {
            $data['titulo'] = 'Impuesto sobre la Renta';
            $data['rangos'] = $rangos;

            //habilitar plugins
            $data['js_file'] = 'ir_rangos.js';

            $views = array(
                'contabilidad/formulario_renta',
            );
            use_template($views, $data);
        }
    }

    public function guardar()
    {
        //TODO: Validacion
        //print_r($_POST); die();
        if( $_POST['monto_desde'] )
        {
            //Poner los registros anteriores a estado 0
            $this->renta->inhabilitarImpuestos();
            
            //Insertar nuevo registro
            $impuesto_renta_id = $this->renta->guardar();

            //Guardar detalle de rangos
            $this->rentad->impuesto_renta_id = $impuesto_renta_id;
            foreach( $_POST['monto_desde'] as $index => $post ) 
            {
                $this->rentad->monto_desde = !empty( $_POST['monto_desde'][ $index ] ) ? $_POST['monto_desde'][ $index ] : 0;
                $this->rentad->monto_hasta = !empty( $_POST['monto_hasta'][ $index ] ) ? $_POST['monto_hasta'][ $index ] : 0;
                $this->rentad->impuesto_base = !empty( $_POST['impuesto_base'][ $index ] ) ? $_POST['impuesto_base'][ $index ] : 0;
                $this->rentad->porcentaje = !empty( $_POST['porcentaje'][ $index ] ) ? $_POST['porcentaje'][ $index ] : 0;
                $this->rentad->sobre_exceso = !empty( $_POST['sobre_exceso'][ $index ] ) ? $_POST['sobre_exceso'][ $index ] : 0;
                $this->rentad->guardar();
            }
        }

        output_json(array('tipo' => 1));
    }
}