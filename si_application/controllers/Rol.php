<?php
class Rol extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('rol_model');
    }

    public function index($ajax = false)
    {
        if($ajax)
        {
            $roles = $this->rol_model->get_roles();
			output_json(array('data' => $roles));
        }
        else
		{
			$data['titulo'] = 'Gestionar Roles';
			$data['roles'] = $this->rol_model->get_roles();
			//habilitar plugins
			//$data['enable_icheck'] = true;
			$data['js_file'] = 'rol.js';

			$views = array(
				'rol/lista',
				'rol/modal_formulario'
			);
			use_template($views, $data);
		}
    }

    public function guardar()
	{
		//TODO:Validacion
        $this->rol_model->rol_nombre = $_POST['rol_nombre'];
        $this->rol_model->rol_descripcion = $_POST['rol_descripcion'];


		if( !empty($_POST['id']) )
		{
            $this->rol_model->id = $_POST['id'];
            $this->rol_model->actualizar();
        }
		else
			$this->rol_model->insertar();

		$resultado = array(
			'tipo' => 1
		);

		output_json($resultado);
	}

    public function eliminar($id)
    {
        $this->rol_model->id = $id;
        $rol = $this->rol_model->eliminar();
		
		$resultado = array(
			'tipo' => 1
		);

		output_json($resultado);
    }
}