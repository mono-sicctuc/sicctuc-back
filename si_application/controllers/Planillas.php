<?php
class Planillas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('planilla_model', 'planilla');
        $this->load->model('planilladetalle_model', 'planillad');
        //$this->load->model('persona_model', 'persona');
        //$this->load->model('puesto_model', 'puesto');
        //$this->load->model('personapuesto_model', 'personapuesto');
		//$this->load->model('personasalario_model', 'salario');
    }

    public function index()
    {
        $data['titulo'] = 'Gestionar Planilla';
        $data['planillas'] = $this->planilla->getPlanillas();
        //habilitar plugins
        $data['enable_icheck'] = true;
        $data['enable_datepicker'] = true;
        $data['js_file'] = 'planilla.js';

        $views = array(
            'personal/lista-planilla',
            //'personal/modal-registrar'
        );
        use_template($views, $data);
    }

    public function lista()
    {
        $planillas = $this->planilla->getPlanillas();
        output_json(array('data' => $planillas));
    }
}