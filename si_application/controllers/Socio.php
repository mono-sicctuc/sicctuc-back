<?php 
class Socio extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('persona_model', 'persona');
		$this->load->model('puesto_model', 'puesto');
		$this->load->model('personaacceso_model', 'acceso');
		$this->load->model('personapuesto_model', 'personapuesto');
		$this->load->model('personasalario_model', 'salario');
    }

    public function index($ajax = false)
    {
        $socios = $this->persona->getSocios();
        if($ajax)
            output_json(array('data' => $socios));
        else 
        {
            $data['titulo'] = 'Gestionar Socios';
			$data['personal'] = $socios;
			//habilitar plugins
			//$data['enable_icheck'] = true;
			$data['enable_datepicker'] = true;
			$data['js_file'] = 'socio.js';

			$views = array(
				'socios/lista',
				'socios/modal-registrar'
			);
			use_template($views, $data);
        }
    }


    public function guardar()
    {
        //TODO:Validacion
        $config = array(
            array(
                'field' => 'persona_nombre',
                'label' => 'Nombres',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'persona_apellido',
                'label' => 'Apellidos',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'persona_cedula',
                'label' => 'Cédula',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'persona_nacimiento',
                'label' => 'Fecha de Nacimiento',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'persona_telefono',
                'label' => 'Telefono    ',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'persona_email',
                'label' => 'Correo Electronico',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'persona_direccion',
                'label' => 'Direccion',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'puesto_id',
                'label' => 'Puesto',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'salario_monto',
                'label' => 'Salario',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'persona_ingreso',
                'label' => 'Fecha de Ingreso',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'usuario',
                'label' => 'Usuario',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'contrasena',
                'label' => 'Contraseña',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'rol_id',
                'label' => 'Rol',
                'rules' => 'trim|required'
            )
        ); 
        
        $this->form_validation->set_rules($config);
        //TODO: Si ya esta registrado no requerir contrasena
       if( empty($_POST['id']) )
        {
            list($dia, $mes, $anio) = explode( '/', $_POST['persona_ingreso'] );
            $persona_ingreso = $anio.'-'.$mes.'-'.$dia;
        }		

        $persona_nacimiento = '';
        if($_POST['persona_nacimiento'])
        {
            list($dian, $mesn, $anion) = explode('/', $_POST['persona_nacimiento']);
            $persona_nacimiento = $anion .'-'. $mesn .'-'. $dian;
        }
         
        if( $this->form_validation->run() == FALSE )
        {
            output_json(array('tipo' => 2, 'texto' => $this->form_validation->error_string() ));
        }
    else
    {

        //Guardar Persona
		$this->persona->persona_nombre = $_POST['persona_nombre'];
        $this->persona->persona_apellido = $_POST['persona_apellido'];
        $this->persona->persona_cedula = $_POST['persona_cedula'];
		$this->persona->persona_nacimiento = $persona_nacimiento;
		$this->persona->persona_telefono = $_POST['persona_telefono'];
		$this->persona->persona_email = $_POST['persona_email'];
		$this->persona->persona_direccion = $_POST['persona_direccion'];
		$this->persona->persona_acceso = 1;		
		$this->persona->persona_tipo_id = 1; //Socio
   
    
		
		if( !empty($_POST['id']) )
		{	//Existente
			$id = $_POST['id'];
			$this->persona->id = $id;
			$this->persona->actualizar();
		}
		else 
		{
			//Nuevo
			$this->persona->persona_ingreso = $persona_ingreso;
			$id = $this->persona->insertar($_POST);

            
            if( !empty($_POST['salario_monto']) )
            {
                //Insertar salario
                $this->salario->persona_id = $id;
                $this->salario->persona_salario_monto = (float) $_POST['salario_monto'];
                $this->salario->insertar();
    
                //Asignar el salario del nuevo ingreso
                $this->persona->id = $id;
                $this->persona->persona_salario = (float) $_POST['salario_monto'];
                $this->persona->asignarSalario();
            }			
		} 
		
		//Guardar Usuario
		$this->acceso->usuario = $_POST['usuario'];
		$this->acceso->contrasena = password_hash($_POST['contrasena'], PASSWORD_DEFAULT);
		$this->acceso->rol_id = $_POST['rol_id'];
		$this->acceso->persona_id = $id;
		$usuario = $this->acceso->get_acceso();
		if( !empty($usuario) ) 
		{
			$this->acceso->id = $usuario['id'];
			$this->acceso->actualizar();
        }
        else
            $this->acceso->insertar(); 

        // Asignar puesto
        if( !empty($_POST['puesto_id']) )
        {
            $this->personapuesto->persona_id = $id;
            $this->personapuesto->puesto_id = $_POST['puesto_id'];
            if( empty($_POST['id']) )
                $this->personapuesto->insertar();
            else
                $this->personapuesto->actualizar();
        }
        elseif( empty($_POST['puesto_id']) and !empty( $_POST['id'] ) )
        {
            //borrar puesto
            $this->personapuesto->persona_id = $id;
            $this->personapuesto->eliminarPuestoPersona();
        }
		output_json(array('tipo' => 1));
    }
}
    /**
     * Retornar lista de personas
     */
    public function personas()
    {  
        $find = trim( $_GET['query'] );
        $personas = $this->persona->getAll( $find, 1);

        $result = array(
            'query' => 'Unit',
            'suggestions' => $personas
        );

        output_json( $result );
    }
}