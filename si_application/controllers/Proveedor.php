<?php
class Proveedor extends CI_Controller 
{
    public function __construct() {
        parent::__construct();
        $this->load->model('proveedor_model', 'proveedor');
    }

    //Listar 
    public function index($ajax = false) {        
        if($ajax) {
            $proveedores = $this->proveedor->get();
            output_json(array('data' => $proveedores));
        } else {
            $data = [
                'titulo'    => 'Gestionar Proveedores',
                'js_file'   => 'proveedor.js',
            ];

            $views = array(
                    'proveedor/lista',
                    'proveedor/modal-registrar',
                    'proveedor/modal-editar',
                );

            use_template($views, $data);
        }
    }

    // Guardar / Actualizar
    public function guardar()
    {
        $config = array(
            array(
                'field' => 'nombre_proveedor',
                'label' => 'Nombre',
                'rules' => 'required'
            ),
        );        

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
            output_json(array('tipo' => 2, 'texto' => $this->form_validation->error_string()));
        } else {
            $this->proveedor->nombre_proveedor = $_POST['nombre_proveedor'];
            $this->proveedor->contacto_proveedor = $_POST['contacto_proveedor'];
            $this->proveedor->detalle_proveedor = $_POST['detalle_proveedor'];

            if (!empty($_POST['id'])) {    //Existente
                $this->proveedor->id = (int) $_POST['id'];
                $this->proveedor->actualizar();
            } else {
                $this->proveedor->insertar();
            }

            output_json(array('tipo' => 1));
        }
    }

    public function eliminar($id)
    {
        //Inactivar a la persona
        $this->proveedor->id = $id;
        $this->proveedor->eliminar();
        output_json(array('tipo' => 1));
    }
}