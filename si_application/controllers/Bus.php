<?php 
class Bus extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bus_model', 'bus');
        $this->load->model('marca_model', 'marca');
    }

    public function index($ajax = false)
    {
        $buses = $this->bus->getBuses();
        if($ajax)
            output_json(array('data' => $buses));
        else 
        {
            $data['titulo'] = 'Gestionar Buses';
            $data['buses'] = $buses;
            $data['marcas'] = $this->marca->getAll();
			//habilitar plugins
			$data['enable_autocomplete'] = true;
			//$data['enable_datepicker'] = true;
			$data['js_file'] = 'bus.js';

			$views = array(
				'bus/bus_lista',
				'bus/bus_modals'
			);
			use_template($views, $data);
        }
    }

    public function guardar()
    {
        //Validacion de placa
        $placa_rules = 'trim|required|is_unique[bus.bus_placa]';
        $placa_error = array('is_unique' => 'Esta %s ya ha sido asignada');

        if( !empty($_POST['id']) ) 
        {
            $placa_rules = 'trim|required';
            $placa_error = array();
        }

        $config = array(
            array(
                'field' => 'persona_id',
                'label' => 'Dueño',
                'rules' => 'trim|required|integer',
                'errors' => array(
                    'required' => 'Debe seleccionar un %s de la lista.',
                    'integer' => 'Debe seleccionar un %s de la lista.',
                ),
            ),
            array(
                'field' => 'bus_placa',
                'label' => 'Placa',
                'rules' => $placa_rules,
                'errors' => $placa_error
            ),
            array(
                'field' => 'bus_marca_id',
                'label' => 'Marca',
                'rules' => 'trim|required|integer',
                'errors' => array(
                    'required' => 'Debe seleccionar un %s de la lista.',
                    'integer' => 'Debe seleccionar un %s de la lista.',
                ),
            )
        ); 

        $this->form_validation->set_rules($config);

        if( $this->form_validation->run() == FALSE )
        {
            output_json(array('tipo' => 2, 'texto' => $this->form_validation->error_string() ));
        }
        else 
        {
            $this->bus->bus_placa = $_POST['bus_placa'];
            $this->bus->bus_marca_id = $_POST['bus_marca_id'];
            $this->bus->bus_modelo = $_POST['bus_modelo'];
            $this->bus->bus_chasis = $_POST['bus_chasis'];
            $this->bus->bus_motor = $_POST['bus_motor'];
            $this->bus->bus_circulacion = $_POST['bus_circulacion'];
            $this->bus->bus_color = $_POST['bus_color'];
            $this->bus->persona_id = $_POST['persona_id'];

            if( !empty($_POST['id']) )
            {	//Existente
                $this->bus->id = (int) $_POST['id'];
                $this->bus->actualizar();
            }
            else
                $this->bus->insertar();

            output_json(array('tipo' => 1));
        }
    }

    public function eliminarBus($id)
    {
        //Inactivar a la persona
		$this->bus->id = $id;
		$this->bus->bus_estado = 0;
		$this->bus->cambiarEstado();

		output_json(array('tipo' => 1));
    }
}