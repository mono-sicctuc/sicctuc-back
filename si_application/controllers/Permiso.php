<?php
class Permiso extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('permiso_model', 'permiso');
        $this->load->model('seccion_model', 'seccion');
    }

    public function obtener_permisos($rol_id)
    {
        $html = '';
        //Obtener listado de secciones
        $secciones = $this->seccion->obtener_secciones();
        if ( !empty($secciones) )
        {
            foreach($secciones as $seccion)
            {
                //Obtener los permisos para la seccion en el loop
                $this->permiso->rol_id = $rol_id;
                $this->permiso->seccion_id = $seccion['id'];
                $permiso = $this->permiso->get_permisos();
                $valor = $permiso['valor'];

                $todos_estado = ( ( strpos($valor, 'C') !== FALSE) and ( strpos($valor, 'R') !== FALSE) and ( strpos($valor, 'U') !== FALSE) and ( strpos($valor, 'D') !== FALSE) ) ? 'checked="checked"' : '';
                $listar_estado = ( strpos($valor, 'R') !== FALSE) ? 'checked="checked"' : '';
                $crear_estado = ( strpos($valor, 'C') !== FALSE) ? 'checked="checked"' : '';
                $editar_estado = ( strpos($valor, 'U') !== FALSE) ? 'checked="checked"' : '';
                $eliminar_estado = ( strpos($valor, 'D') !== FALSE) ? 'checked="checked"' : '';

                $html .= '<tr>';
                $html .=    '<td>'. $seccion['seccion_nombre'] .'</td>';
                $html .=    '<td>';
                $html .=        '<input type="hidden" name="seccion_id[]" value="'. $seccion['id'] .'">';
                $html .=        '<input type="hidden" name="rol_id" value="'. $rol_id .'">';
                $html .=        '<label class="checkbox-inline">';                
                $html .=            '<input '. $todos_estado .' class="toggle-todos" type="checkbox" id="todos'. $seccion['id'] .'"> Todos';
                $html .=        '</label>';
                $html .=        '<label class="checkbox-inline">';
                $html .=            '<input '. $listar_estado .' class="check" name="permiso_valor_'. $seccion['id'] .'[]" type="checkbox" id="permisoLeer'. $seccion['id'] .'" value="R"> Listar';
                $html .=        '</label>';
                $html .=        '<label class="checkbox-inline">';
                $html .=            '<input '. $crear_estado .' class="check" name="permiso_valor_'. $seccion['id'] .'[]" type="checkbox" id="permisoCrear'. $seccion['id'] .'" value="C"> Crear';
                $html .=        '</label>';
                $html .=        '<label class="checkbox-inline">';
                $html .=            '<input '. $editar_estado .' class="check" name="permiso_valor_'. $seccion['id'] .'[]" type="checkbox" id="permisoEditar'. $seccion['id'] .'" value="U"> Editar';
                $html .=        '</label>';
                $html .=        '<label class="checkbox-inline">';
                $html .=            '<input '. $eliminar_estado .' class="check" name="permiso_valor_'. $seccion['id'] .'[]" type="checkbox" id="permisoEliminar'. $seccion['id'] .'" value="D"> Eliminar';
                $html .=        '</label>';
                $html .=    '</td>';
                $html .= '</tr>';
            }
        }
        output_json( $html );
    }
}