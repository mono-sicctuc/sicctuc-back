<?php
class ProductoTipo extends CI_Controller 
{
    public function __construct() {
        parent::__construct();
        $this->load->model('productotipo_model', 'tipo');
    }

    //Listar 
    public function index($ajax = false) {        
        if($ajax) {
            $tipos = $this->tipo->get();
            output_json(array('data' => $tipos));
        } else {
            $data = [
                'titulo'            => 'Gestionar Tipo de Productos',
                'js_file'           => 'productotipo.js',
            ];

            $views = array(
                    'producto_tipo/lista',
                    'producto_tipo/modal-registrar',
                    'producto_tipo/modal-editar',
                );

            use_template($views, $data);
        }
    }

    // Guardar / Actualizar
    public function guardar()
    {
        $config = array(
            array(
                'field' => 'producto_tipo_descripcion',
                'label' => 'Nombre',
                'rules' => 'required'
            ),
        );        

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
            output_json(array('tipo' => 2, 'texto' => $this->form_validation->error_string()));
        } else {
            $this->tipo->producto_tipo_descripcion = $_POST['producto_tipo_descripcion'];

            if (!empty($_POST['id'])) {    //Existente
                $this->tipo->id = (int) $_POST['id'];
                $this->tipo->actualizar();
            } else {
                $this->tipo->insertar();
            }

            output_json(array('tipo' => 1));
        }
    }

    public function eliminar($id)
    {
        //Inactivar a la persona
        $this->tipo->id = $id;
        $this->tipo->eliminar();
        output_json(array('tipo' => 1));
    }
}