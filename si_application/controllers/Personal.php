<?php 
class Personal extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('persona_model', 'persona');
		$this->load->model('puesto_model', 'puesto');
		$this->load->model('personaacceso_model', 'acceso');
		$this->load->model('personapuesto_model', 'personapuesto');
		$this->load->model('personasalario_model', 'salario');
	}

	/**
	* Listar Personal
	*/
	public function index($ajax = false)
	{
		if($ajax)
		{
			$personas = $this->persona->get_personas();
			output_json(array('data' => $personas));
		}
		else
		{
			$data['titulo'] = 'Gestionar Personal';
			$data['personal'] = $this->persona->get_personas();
			//habilitar plugins
			$data['enable_icheck'] = true;
			$data['enable_datepicker'] = true;
			$data['js_file'] = 'personal.js';

			$views = array(
				'personal/lista',
				'personal/modal-registrar',
				'personal/modal-editar',
				'personal/modal-salario'
			);
			use_template($views, $data);
		}
	}
	/**
	* Guardar persona
	*/
	public function guardar_persona()
	{
		$persona_acceso = (!empty($_POST['persona_acceso'])) ? $_POST['persona_acceso'] : 0;
		
		$config = array(
			array(
				'field' => 'persona_nombre',
				'label' => 'Nombres',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'persona_apellido',
				'label' => 'Apellidos',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'persona_cedula',
				'label' => 'Cédula',
				'rules' => 'trim|required|regex_match[/^\d{3}-\d{6}-\d{4}[A-Z]$/]'
			),
			array(
				'field' => 'persona_telefono',
				'label' => 'Teléfono',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'puesto_id',
				'label' => 'Puesto',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'salario_monto',
				'label' => 'Salario',
				'rules' => 'trim|required|numeric'
			),
			array(
				'field' => 'persona_ingreso',
				'label' => 'Fecha de Ingreso',
				'rules' => 'trim|required'
			),
		);

		if($persona_acceso) {
			$config[] = array(
				'field' => 'usuario',
				'label' => 'Usuario',
				'rules' => 'trim|required|is_unique[persona_acceso.usuario]'
			);
			$config[] = array(
				'field' => 'contrasena',
				'label' => 'Contraseña',
				'rules' => 'required'
			);

			$config[] = array(
				'field' => 'rol_id',
				'label' => 'Rol',
				'rules' => 'required'
			);
		}

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) {
			output_json(array('tipo' => 2, 'texto' => $this->form_validation->error_string()));
		} elseif(!$this->cedula_isValid($_POST['persona_cedula'])){
			output_json(array('tipo' => 2, 'texto' => 'El campo Cédula no es válido'));
		} else {
			$persona_ingreso = "";
			if (!empty($_POST['persona_ingreso'])) {
				list($dia, $mes, $anio) = explode('/', $_POST['persona_ingreso']);
				$persona_ingreso = $anio . '-' . $mes . '-' . $dia;
			}

			$persona_nacimiento = '';
			if (!empty($_POST['persona_nacimiento'])) {
				list($dian, $mesn, $anion) = explode('/', $_POST['persona_nacimiento']);
				$persona_nacimiento = $anion . '-' . $mesn . '-' . $dian;
			}

			//Guardar Persona
			$this->persona->persona_nombre = $this->input->post('persona_nombre');
			$this->persona->persona_apellido = $this->input->post('persona_apellido');
			$this->persona->persona_cedula = strtoupper($this->input->post('persona_cedula'));
			$this->persona->persona_nacimiento = $persona_nacimiento;
			$this->persona->persona_telefono = $this->input->post('persona_telefono');
			$this->persona->persona_email = $this->input->post('persona_email');
			$this->persona->persona_direccion = $this->input->post('persona_direccion');
			$this->persona->persona_acceso = $persona_acceso;
			$this->persona->persona_tipo_id = 2; //Empleado
			$this->persona->persona_ingreso = $persona_ingreso;
			$this->persona->puesto_id = $this->input->post('puesto_id');
			$id = $this->persona->insertar();

			//Insertar salario
			$this->salario->persona_id = $id;
			$this->salario->persona_salario_monto = (float) $this->input->post('salario_monto');
			$this->salario->insertar();

			//Asignar el salario del nuevo ingreso
			$this->persona->id = $id;
			$this->persona->persona_salario = (float) $this->input->post('salario_monto');
			$this->persona->asignarSalario();

			//Guardar Usuario (Acceso) si es el caso
			if ($persona_acceso) {
				$this->acceso->persona_id = $id;
				$this->acceso->usuario = $this->input->post('usuario');
				$this->acceso->contrasena = password_hash($this->input->post('contrasena'), PASSWORD_DEFAULT);
				$this->acceso->rol_usuario_id = $this->input->post('rol_id');
				$this->acceso->insertar();
			}

			$resultado = array(
				'tipo' => 1
			);

			output_json($resultado);
		}		
	}
	
	/**
	 * Actualizar persona
	 */
	public function actualizar_persona() {
		$id = $_POST['id'];
		$persona_acceso = (!empty($_POST['persona_acceso'])) ? $_POST['persona_acceso'] : 0;
		$this->acceso->persona_id = $id;
		$acceso_data = $this->acceso->get_acceso();

		$config = array(
			array(
				'field' => 'persona_nombre',
				'label' => 'Nombres',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'persona_apellido',
				'label' => 'Apellidos',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'persona_cedula',
				'label' => 'Cédula',
				'rules' => 'trim|required|regex_match[/^\d{3}-\d{6}-\d{4}[a-zA-Z]$/]'
			),
			array(
				'field' => 'persona_telefono',
				'label' => 'Teléfono',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'puesto_id',
				'label' => 'Puesto',
				'rules' => 'trim|required'
			),
		);

		if ($persona_acceso) {
			$config[] = array(
				'field' => 'usuario',
				'label' => 'Usuario',
				'rules' => 'trim|required'
			);
			$config[] = array(
				'field' => 'rol_id',
				'label' => 'Rol',
				'rules' => 'required'
			);
		}

		if($persona_acceso && empty($acceso_data)) {
			$config[] = array(
				'field' => 'contrasena',
				'label' => 'Contraseña',
				'rules' => 'required'
			);
		}

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) {
			output_json(array('tipo' => 2, 'texto' => $this->form_validation->error_string()));
		} elseif (!$this->cedula_isValid($_POST['persona_cedula'])) {
			output_json(array('tipo' => 2, 'texto' => 'El campo Cédula no es válido.'));
		} elseif(!$this->username_isUnique($_POST['usuario'], $_POST['id'])) {
			output_json(array('tipo' => 2, 'texto' => 'El valor para el campo Usuario ya fue usado, por favor elija otro.'));
		} else {
			$persona_nacimiento = '';
			if (!empty($_POST['persona_nacimiento'])) {
				list($dian, $mesn, $anion) = explode('/', $_POST['persona_nacimiento']);
				$persona_nacimiento = $anion . '-' . $mesn . '-' . $dian;
			}

			//Guardar Persona
			$this->persona->id = $id;
			$data['persona_nombre'] = $this->input->post('persona_nombre');
			$data['persona_apellido'] = $this->input->post('persona_apellido');
			$data['persona_cedula'] = strtoupper($this->input->post('persona_cedula'));
			$data['persona_nacimiento'] = $persona_nacimiento;
			$data['persona_telefono'] = $this->input->post('persona_telefono');
			$data['persona_email'] = $this->input->post('persona_email');
			$data['persona_direccion'] = $this->input->post('persona_direccion');
			$data['persona_acceso'] = $persona_acceso;
			$data['puesto_id'] = $this->input->post('puesto_id');
			$this->persona->actualizar($data);

			// Eliminar datos de acceso si el valor fue deshabilitado
			if(!$persona_acceso && $acceso_data) {
				$this->acceso->eliminar_by_persona();
			}

			//Guardar Usuario (Acceso) si es el caso
			if ($persona_acceso) {
				$data_a['persona_id'] = $id;
				$data_a['usuario'] = $this->input->post('usuario');
				if($acceso_data && $this->input->post('contrasena'))
					$data_a['contrasena'] = password_hash($this->input->post('contrasena'), PASSWORD_DEFAULT);
				elseif(empty($acceso_data))
					$data_a['contrasena'] = password_hash($this->input->post('contrasena'), PASSWORD_DEFAULT);
				$data_a['rol_usuario_id'] = $this->input->post('rol_id');
				if($acceso_data)
					$this->acceso->actualizar($data_a);
				else
					$this->acceso->insertar($data_a);
			}

			$resultado = array(
				'tipo' => 1
			);

			output_json($resultado);
		}
	}

	// Validar cedula
	public function cedula_isValid($cedula) {
		$cedula_parts = explode("-", $cedula);
		if(count($cedula_parts) != 3)
			return false;
		$date = $cedula_parts[1];
		$dia = (int) substr($date, 0, 2);
		$mes = (int) substr($date, 2, 2);
		$anio = substr($date, 4, 2);
		$dt = DateTime::createFromFormat('y', $anio);
		$anio = $dt->format('Y');
		return checkdate($mes, $dia, $anio);
	}

	// Validar usuario
	public function username_isUnique($usuario, $persona_id) {
		$resultado = $this->acceso->obtener_registro_por_usuario_id($usuario, $persona_id);
		return empty($resultado);
	}

	/**
	 * Actualizar el salario de la persona
	 */
	public function actualizar_salario()
	{
		$config = array(
			array(
				'field' => 'persona_salario_monto',
				'label' => 'Salario',
				'rules' => 'trim|required|numeric'
			),
		);
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) {
			output_json(array('tipo' => 2, 'texto' => $this->form_validation->error_string()));
		} else {
			$persona_id = $this->input->post('persona_id');

			//Poner todos los salarios de la persona en estado 0
			$this->salario->persona_id = $persona_id;
			$this->salario->desactivarTodos();

			//Insertar salario
			$this->salario->persona_salario_monto = (float) $this->input->post('persona_salario_monto');
			$this->salario->insertar();

			//Asignar el salario del nuevo ingreso
			$this->persona->id = $persona_id;
			$this->persona->persona_salario = (float) $this->input->post('persona_salario_monto');
			$this->persona->asignarSalario();

			$resultado = array(
				'tipo' => 1
			);

			output_json($resultado);
		}
	}

	/**
	 * Eliminar registro de la persona
	 */
	public function eliminar_persona($id)
	{
		//Inactivar a la persona
		$this->persona->id = $id;
		$this->persona->persona_estado = 0;
		$this->persona->cambiarEstado();

		$resultado = array(
			'tipo' => 1
		);

		output_json($resultado);
	}

	/**
	* Listar puestos
	*/
	public function puesto($ajax = false) 
	{
		if($ajax)
		{
			$puestos = $this->puesto->get_puestos();			
			output_json(array('data' => $puestos));
		}
		else
		{
			$data['titulo'] = 'Gestionar Puestos';
			$data['puestos'] = $this->puesto->get_puestos();
			//habilitar plugins
			$data['js_file'] = 'puesto.js';

			$views = array(
				'puesto/lista',
				'puesto/modal-formulario'
			);

			use_template($views, $data);
		}		
	}

	/**
	* Guardar / Actualizar puesto
	*/
	public function guardar_puesto()/**prueba */
	{
		$this->form_validation->set_rules('puesto_nombre', 'Nombre del Puesto', 'trim|required');


		if( !empty($_POST['id']) )
			$this->form_validation->set_rules('id', 'Puesto', 'trim|required|integer');
		
		if($this->form_validation->run() == FALSE)
		{
			$texto = $this->form_validation->error_string();
			output_json(['tipo' => 2, 'texto' => $texto]);
		}
		else {
			if( !empty($_POST['id']) )
				$this->puesto->actualizar($_POST);
			else
				$this->puesto->insertar($_POST);	
			output_json(['tipo' => 1]);
		}	
	}

	/**
	* Obtener un puesto
	* @param int $id
	* @return json
	*/
	public function obtener_puesto($id)
	{
		$puesto = $this->puesto->get_puesto($id);
		output_json($puesto);
	}

	/**
	* Eliminar un puesto
	* @param int $id
	* @return json
	*/
	public function eliminar_puesto($id)
	{
		$puesto = $this->puesto->eliminar_puesto($id);
		
		$resultado = array(
			'tipo' => 1
		);

		output_json($resultado);
	}
}
?>