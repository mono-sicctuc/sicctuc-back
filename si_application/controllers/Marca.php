<?php
class Marca extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('marca_model', 'marca');
    }

    public function index($ajax = false)
    {
        $marcas = $this->marca->getAll();
        if($ajax)
            output_json(array('data' => $marcas));
        else 
        {
            $data['titulo'] = 'Gestionar Marcas';
			$data['marcas'] = $marcas;
			$data['js_file'] = 'marca.js';

			$views = array(
				'bus/marcas_lista',
				'bus/marcas_modals'
			);
			use_template($views, $data);
        }
    }

    public function guardar()
    {
        $this->form_validation->set_rules('bus_marca_nombre', 'Marca', 'trim|required');
        if($this->form_validation->run() == FALSE)
        {
            output_json([
                'tipo' => 2,
                'texto' => $this->form_validation->error_string()
            ]);
        }
        else 
        {
            $this->marca->bus_marca_nombre = $_POST['bus_marca_nombre'];
            if( !empty($_POST['id']) )
            {	//Existente
                $this->marca->id = (int) $_POST['id'];
                $this->marca->actualizar();
            }
            else
                $this->marca->insertar();

            output_json(array('tipo' => 1));
        }
    }

    public function eliminarMarca($id)
    {
        //Inactivar a la persona
		$this->marca->id = $id;
		$this->marca->bus_marca_estado = 0;
		$this->marca->cambiarEstado();

		output_json(array('tipo' => 1));
    }
}