<?php
class Sesion extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('persona_model', 'p');
        $this->load->model('personaacceso_model', 'pa');
    }

    public function index()
    {
        $data['titulo'] = 'Iniciar Sesi&oacute;n';
        $data['js_file'] = 'sesion.js';

        //Validacion
        $config = array(
            array(
                'field' => 'usuario',
                'label' => 'Usuario',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'contrasena',
                'label' => 'Contrase&ntilde;a',
                'rules' => 'required'
            )
        );

        $this->form_validation->set_rules($config);

        if( $this->form_validation->run() == FALSE )
        {
            $this->load->view('login', $data);
        }
        else 
        {
            $this->pa->usuario = $this->input->post('usuario');
            $persona = $this->pa->obtener_acceso_activo();
            if(!empty($persona) && password_verify($this->input->post('contrasena'), $persona['contrasena']) )
            {
                //Iniciar sesion
                $this->session->set_userdata('usuario', $persona['usuario']);
                redirect('/'); exit;
            }
            else 
            {
                //Enviar mensaje    
                //$this->form_validation->error('usuario', )
                $data['custom_error'] = 'Usuario o contrase&ntilde;a inv&aacute;lidos';
                $this->load->view('login', $data);
            }
        }
    }

    public function borrar()
    {
        $this->session->unset_userdata('usuario');
        redirect(site_url('iniciar-sesion')); exit;
    }
}