<?php
class Produccion extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('produccion_model', 'pr');
        $this->load->model('ciclo_model', 'cl');
        $this->load->model('persona_model', 'pe');
        $this->load->model('puesto_model', 'pu');
        $this->load->model('bus_model', 'bus');
        $this->load->model('constante_model', 'const');
        $this->load->model('ruta_model', 'ruta');
    }

    public function index($ajax = false)
    {
        $producciones = $this->pr->getAll();
        if($ajax)
        {
            output_json(array('data' => $producciones));
        }
        else 
        {
            $data['titulo'] = 'Reportar Producci&oacute;n';
            //$data['prestamos'] = $producciones;
            //habilitar plugins
            $data['enable_datepicker'] = true;
            $data['enable_icheck'] = true;
            $data['enable_autocomplete'] = true;
            
			$data['js_file'] = 'produccion.js';
            $data['placas'] = $this->bus->getPlacas();
            $data['pasaje'] = $this->const->getPasaje();
            $data['rutas'] = $this->ruta->getRutas();

			$views = array(
				'personal/produccion_lista',
                'personal/produccion_form',
			);
			use_template($views, $data);
        }
    }

    /**
     * Guardar entrada de produccion manual
     */
    public function guardar() {
        $tipo_ingreso = $this->input->post('produccion_tipo');
        $config = array(
            array(
                'field' => 'produccion_fecha',
                'label' => 'Fecha',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'conductor_id',
                'label' => 'Conductor',
                'rules' => 'required'
            ),
            array(
                'field' => 'bus_id',
                'label' => 'Unidad de Transporte',
                'rules' => 'required'
            ),
            array(
                'field' => 'produccion_tipo',
                'label' => 'Tipo de Ingreso',
                'rules' => 'required'
            ),
            array(
                'field' => 'produccion_monto',
                'label' => 'Monto',
                'rules' => 'trim|required|numeric'
            ),
            array(
                'field' => 'ciclo_pasajeros',
                'label' => 'Pasajeros',
                'rules' => 'trim|required|numeric'
            ),
            array(
                'field' => 'ciclo_gastos',
                'label' => 'Gastos',
                'rules' => 'trim|numeric'
            ),
            array(
                'field' => 'ciclo_km',
                'label' => 'Kilómetros',
                'rules' => 'trim|numeric'
            ),
            array(
                'field' => 'ruta_id',
                'label' => 'Ruta',
                'rules' => 'required'
            ),
        );

        if($tipo_ingreso == 1) {
            // tipo = ciclo
            $config[] = array(
                'field' => 'sentido',
                'label' => 'Sentido',
                'rules' => 'required'
            );
        } else {
            // tipo = dia
            $config[] = array(
                'field' => 'cantidad',
                'label' => 'Cantidad de Ciclos',
                'rules' => 'trim|required|numeric'
            );
        }

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            output_json(array('tipo' => 2, 'texto' => $this->form_validation->error_string()));
        } else {




            $resultado = array(
                'tipo' => 1
            );

            output_json($resultado);
        }
        // $this->input->post()
    }

    public function carga()
    {
        $config['upload_path']          = FCPATH.'uploads'.DIRECTORY_SEPARATOR;
        $config['allowed_types']        = 'csv';
        $config['max_size']             = (1024 * 5); //5Mb
        $config['file_ext_tolower']     = TRUE;
        $config['file_name']            = 'importe-produccion-'.time();
        $this->load->library('upload', $config);

        //var_dump($_FILES);
        
        if( ! $this->upload->do_upload('archivoCsv') )
            $data['error'] = $this->upload->display_errors();
        else 
        {
            $fileData = $this->upload->data();
            
            //Procesar el archivo
            $csvfile  = $config['upload_path'].$config['file_name'].'.csv';
            $lines = array();

            $file_handle = fopen($csvfile, 'r');
            if($file_handle !== FALSE)
            {
                while (!feof($file_handle) ) {
                    $lines[] = fgetcsv($file_handle, 1024);
                }

                fclose($file_handle);
            }                    
            
            if(!empty($lines) && count($lines) > 1)
            {
                unset($lines[0]);
                //Guardar los valores en la base de datos
                var_dump($lines);
            }

            $data['exito'] = 'El archivo '.$fileData['client_name'].' se ha cargado exitosamente!';
        }
        output_json($data);
    }

    public function conductores() {
        $puesto = $this->pu->getConductorPuesto();
        $conductores = array();
        if($puesto)
            $conductores = $this->pr->getConductores($this->input->get('query'), $puesto['id']);
        $result['suggestions'] = $conductores;
        output_json($result);
    }
}