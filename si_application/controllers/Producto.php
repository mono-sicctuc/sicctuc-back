<?php 
class Producto extends CI_Controller 
{    
    public function __construct() {
        parent::__construct();
        $this->load->model('producto_model', 'producto');
        $this->load->model('productolote_model', 'lote');
        $this->load->model('productotipo_model', 'tipo');
    }

    // Listar productos
    public function index($ajax = false) {
        $productos_db = $this->producto->get();
        $productos = array();
        $tipos = $this->tipo->get();
        
        if(!empty($productos_db)) {
            foreach($productos_db as $producto) {
                $this->lote->producto_id = $producto['id'];
                $lotes = $this->lote->getLoteByProducto();
                $total = count($lotes);
                $cantidad = 0;
                $precio = 0;
                $suma = 0;

                if($total > 0) {                    
                    foreach($lotes as $lote) {
                        $suma += $lote['lote_precio_venta'];
                        $cantidad += $lote['lote_cantidad_actual'];
                    }                        

                    $precio = $suma / $total;
                }

                $producto['producto_precio'] = $precio;
                $producto['producto_cantidad'] = $cantidad;
                $productos[] = $producto;
            }
        }

        if($ajax) {
            output_json(array('data' => $productos));
        } else {
            $data = [
                'titulo'            => 'Gestionar Productos', 
                'productos'         => $productos,
                'tipos'             => $tipos,
                // 'enable_icheck'     => true,
                // 'enable_datepicker' => true,
                'js_file'           => 'producto.js',
            ];

			$views = array(
				'producto/lista',
				'producto/modal-registrar',
				'producto/modal-editar',
			);

			use_template($views, $data);
        }
    }

    public function guardar() {
        $config = array(
            array(
                'field' => 'producto_tipo',
                'label' => 'Tipo de Producto',
                'rules' => 'required'
            ),
            array(
                'field' => 'producto_nombre',
                'label' => 'Nombre',
                'rules' => 'trim|required'
            ),            
            array(
                'field' => 'producto_minimo',
                'label' => 'Mínimo',
                'rules' => 'integer'
            ),
        );

        if( empty($_POST['id']) ) {
            $config[] = array(
                'field' => 'producto_cantidad',
                'label' => 'Cantidad Actual',
                'rules' => 'required|integer'
            );

            $config[] = array(
                'field' => 'producto_precio',
                'label' => 'Precio',
                'rules' => 'required|numeric'
            );

            $config[] = array(
                'field' => 'producto_precio_venta',
                'label' => 'Precio de Venta',
                'rules' => 'required|numeric'
            );
        }

        $this->form_validation->set_rules($config);

        if( $this->form_validation->run() == FALSE )
        {
            output_json(array('tipo' => 2, 'texto' => $this->form_validation->error_string() ));
        }
        else 
        {
            $this->producto->producto_tipo_id = $_POST['producto_tipo'];
            $this->producto->producto_nombre = $_POST['producto_nombre'];            
            $this->producto->producto_descripcion = $_POST['producto_descripcion'];
            $this->producto->producto_minimo = $_POST['producto_minimo'];

            if( !empty($_POST['id']) )
            {	//Existente
                $this->producto->id = (int) $_POST['id'];
                $this->producto->actualizar();
            }
            else {
                $this->producto->insertar();

                // insertar primer dato del lote
                $this->lote->lote_fecha = date('Y-m-d H:i:s');
                $this->lote->lote_precio = $_POST['producto_precio'];
                $this->lote->lote_precio_venta = $_POST['producto_precio_venta'];
                $this->lote->lote_cantidad = $_POST['producto_cantidad'];
                $this->lote->lote_cantidad_actual = $_POST['producto_cantidad'];
                $this->lote->producto_id = $this->db->insert_id();
                $this->lote->insertar();
            }

            output_json(array('tipo' => 1));
        }
    }
}