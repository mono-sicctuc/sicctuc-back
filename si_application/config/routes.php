<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
//Iniciar sesion
$route['iniciar-sesion'] = 'sesion/index';
$route['acceder'] = 'sesion/registrar';
$route['cerrar-sesion'] = 'sesion/borrar';
//Personal
$route['personal/index/(:num)'] = 'personal/index/$1';
$route['personal/guardar'] = 'personal/guardar_persona';
$route['personal/actualizar'] = 'personal/actualizar_persona';
$route['personal/salario'] = 'personal/actualizar_salario';
$route['personal/eliminar/(:num)'] = 'personal/eliminar_persona/$1';
$route['personal/produccion'] = 'produccion';
$route['personal/produccion/(:num)'] = 'produccion/index/$1';
$route['personal/produccion/guardar'] = 'produccion/guardar';
$route['personal/produccion/conductores'] = 'produccion/conductores';
//Planilla
$route['personal/planilla'] = 'planillas/index';
$route['planilla/lista'] = 'planillas/lista';
//Puesto
$route['personal/puestos'] = 'personal/puesto';
$route['personal/puestos/(:num)'] = 'personal/puesto/$1';
$route['personal/tipo'] = 'personal/puesto';
$route['puesto/guardar'] = 'personal/guardar_puesto';
$route['puesto/obtener/(:num)'] = 'personal/obtener_puesto/$1';
$route['puesto/eliminar/(:num)'] = 'personal/eliminar_puesto/$1';
//Roles
$route['seguridad/roles'] = 'rol';
$route['seguridad/roles/(:num)'] = 'rol/index/$1';
$route['seguridad/guardar-rol'] = 'rol/guardar';
$route['seguridad/eliminar-rol/(:num)'] = 'rol/eliminar/$1';
//Permisos del Rol
$route['seguridad/obtener-permisos/(:num)'] = 'permiso/obtener_permisos/$1';
//Contabilidad
$route['contabilidad/variables'] = 'variables/index';
$route['contabilidad/variables/(:num)'] = 'variables/index/$1';
$route['contabilidad/guardar-variable'] = 'variables/guardar';
$route['contabilidad/impuesto-renta'] = 'renta/index';
$route['contabilidad/guardar-impuesto'] = 'renta/guardar';
$route['contabilidad/prestamos'] = 'prestamo';
$route['contabilidad/codigos'] = 'codigo';
$route['prestamo/index/(:num)/(:num)'] = 'prestamo/index/$1/$2';
$route['prestamo/persona-tipo/(:num)'] = 'prestamo/personaTipo/$1';
$route['prestamo/abonar'] = 'prestamo/guardarAbono';
$route['prestamo/detalle/(:num)'] = 'prestamo/obtenerDetalle/$1';
//$route['contabilidad/r']
// Productos
$route['productos'] = 'producto';
$route['productos/tipos'] = 'productotipo';
$route['productos/proveedores'] = 'proveedor';
//$route['productos/compras'] = 'compra';
//$route['personal/index/(:num)'] = 'personal/index/$1';
//$route['producto/guardar'] = 'producto/guardar';
//Socios
$route['socios'] = 'socio';
$route['socio/eliminar/(:num)'] = 'personal/eliminar_persona/$1';
$route['socio/salario'] = 'personal/actualizar_salario';
$route['socios/buses'] = 'bus';
$route['bus/eliminar/(:num)'] = 'bus/eliminarBus/$1';
$route['bus/marcas'] = 'marca';
$route['bus/marcas/(:num)'] = 'marca/index/$1';
$route['bus/marcas/eliminar/(:num)'] = 'marca/eliminarMarca/$1';