<?php
class PersonaAcceso_model extends CI_Model 
{
    public $id;
    public $usuario;
    public $contrasena;
    public $rol_usuario_id;
    public $persona_id;

    public function __construct() 
    {
        $this->load->database();
    }

    public function insertar($data = null)
    {
        if($data)
            $this->db->insert('persona_acceso', $data);
        else
            $this->db->insert('persona_acceso', $this);        
    }

    public function actualizar($data)
    {
        $this->db->update( 'persona_acceso', $data, array('id' => $this->id) );
    }

    public function eliminar()
    {
        $this->db->where('id', $this->id);
        $this->db->delete('persona_acceso');
    }

    public function get_acceso()
    {
        $query = $this->db->get_where('persona_acceso', array('persona_id' => $this->persona_id), 1 );
        return $query->row_array();
    }

    public function obtener_acceso_activo()
    {
        $this->db->select('*');
        //$this->db->from('persona_acceso as pa');
        $this->db->join('persona as p', 'p.id = pa.persona_id');
        $query = $this->db->get_where('persona_acceso as pa', array(
            'pa.usuario' => $this->usuario,
            'p.persona_estado' => 1
        ), 1);
        return $query->row_array();
    }

    public function obtener_nombre_by_usuario($usuario)
    {
        $this->db->select('*');
        //$this->db->from('persona_acceso as pa');
        $this->db->join('persona as p', 'p.id = pa.persona_id');
        $query = $this->db->get_where('persona_acceso as pa', array(
            'usuario' => $usuario
        ), 1);
        $resultado = $query->row_array();
        return $resultado['persona_nombre'].' '.$resultado['persona_apellido'];
    }

    public function obtener_registro_por_usuario_id($usuario, $persona_id)
    {
        $this->db->select('usuario');
        $query = $this->db->get_where('persona_acceso', ['persona_id !=' => $persona_id, 'usuario' => $usuario], 1);
        return $query->row_array(); 
    }

    public function eliminar_by_persona()
    {
        $this->db->where('persona_id', $this->persona_id);
        $this->db->delete('persona_acceso');
    }
}