<?php
class Puesto_model extends CI_Model {
    public $puesto_nombre;
    public $puesto_descripcion;

    public function __construct() 
    {
        $this->load->database();
    }

    public function insertar($data)
    {
        $this->puesto_nombre = $data['puesto_nombre'];
        $this->puesto_descripcion = $data['puesto_descripcion'];
        $this->db->insert('puesto', $this);
    }

    public function actualizar($data) 
    {
        $this->puesto_nombre = $data['puesto_nombre'];
        $this->puesto_descripcion = $data['puesto_descripcion'];

        $this->db->where('id', $data['id']);
        $this->db->update('puesto', $this);
    }

    public function get_puestos()
    {
        $query = $this->db->get_where('puesto', array('puesto_estado' => 1));
        return $query->result_array();
    }

    public function get_puesto($id) 
    {
        $query = $this->db->get_where('puesto', array('id' => $id, 'puesto_estado' => 1)); 
        return $query->row_array();
    }

    public function eliminar_puesto($id)
    {
        $this->db->set('puesto_estado', 0);
        $this->db->where('id', $id);
        $this->db->update('puesto');
    }

    public function getConductorPuesto() 
    {
        $query = $this->db->query("SELECT id FROM puesto WHERE LOWER(puesto_nombre) like '%conductor%' AND puesto_estado = 1 limit 0, 1");
        return $query->row_array();        
    }
}