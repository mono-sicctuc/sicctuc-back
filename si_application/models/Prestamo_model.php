<?php 
class Prestamo_model extends CI_Model 
{
    public $id; 
    public $fecha_solicitud;
    public $monto;
    public $saldo;
    public $monto_abono;
    public $forma_pago = 1;
    public $persona_id;
    public $estado = 1;

    public function __construct()
    {
        $this->load->database();
    }

    public function guardar()
    {
        $this->db->insert('prestamo', $this);
    }

    public function getPrestamos() 
    {
        $this->db->select("CONCAT_WS(' ', persona_nombre, persona_apellido) as persona_nombre_apellido, p.*");
        $this->db->join('persona as pe', 'pe.id = p.persona_id', 'left');

        if($this->estado)
            $query = $this->db->get_where('prestamo as p', array('estado', $this->estado));
        else 
            $query = $this->db->get('prestamo as p');

        return $query->result_array();
    }

    /**
     * Retorna el saldo del prestamo
     * @return float
     */
    public function obtenerSaldo()
    {
        $query = $this->db->get_where('prestamo', 'id = '.$this->id, '1', 0);
        $resultado = $query->row_array();
        return (!empty($resultado['saldo'])) ? $resultado['saldo'] : 0;
    }

    /**
     * Actualizar el saldo del prestamo
     */
    public function actualizarSaldo()
    {
        $this->db->set('saldo', $this->saldo);
        $this->db->where('id', $this->id);
        $this->db->update('prestamo');
    }

    /**
     * Actualizar el estado del prestamo
     */
    public function actualizarEstado()
    {
        $this->db->set('estado', $this->estado);
        $this->db->where('id', $this->id);
        $this->db->update('prestamo');
    }

    /**
     * Calcular dias de retraso de pago
     */
    /*public function calcularDias()
    {
        $this->db->get_where('prestamo', 'id = '.$this->id, '1', 0);
        $resultado = $query->row_array();
        //return (!empty($resultado['saldo'])) ? $resultado['saldo'] : 0;
    }*/
}