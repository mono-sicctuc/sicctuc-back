<?php
class Bus_model extends CI_Model 
{
    public $id;
    public $bus_placa;    
    public $bus_modelo;
    public $bus_chasis;
    public $bus_motor;
    public $bus_circulacion;
    public $bus_color;
    public $bus_estado = 1;
    public $bus_marca_id;
    public $persona_id;

    public function __construct()
    {
        $this->load->database();
    }

    public function getBuses()
    {
        $this->db->select('CONCAT_WS(" ", persona_nombre, persona_apellido) as dueno, b.*, m.bus_marca_nombre as marca');
        $this->db->from('bus as b');
        $this->db->join('persona as p', 'p.id = b.persona_id', 'inner');
        $this->db->join('bus_marca as m', 'm.id = b.bus_marca_id', 'inner');
        $this->db->where('bus_estado', 1);
        $this->db->where('persona_estado', 1);

        $query = $this->db->get(); //var_dump($this->db->last_query());
        return $query->result_array();
    }

    public function insertar()
    {
        $this->db->insert('bus', $this);
    }

    public function actualizar()
    {
        $this->db->update( 'bus', $this, array('id' => $this->id) );
    }

    public function cambiarEstado()
    {
        $this->db->set('bus_estado', $this->bus_estado);
        $this->db->where('id', $this->id);
        $this->db->update('bus');
    }

    public function getPlacas() {
        $query = $this->db->query("SELECT id AS data, bus_placa AS value FROM bus WHERE bus_estado = 1");
        return $query->result_array();
    }
}