<?php
class CodigoContable_model extends CI_Model 
{
    public $id;
    public $codigo_valor;
    public $codigo_tipo = 0;
    public $codigo_subtipo = 0;
    public $codigo_nombre;
    public $codigo_descripcion;
    public $estado_resultado = 0;
    public $estado_resultado_tipo = 0;

    public function __construct() 
    {
        $this->load->database();
    }

    public function insertar()
    {
        $this->db->insert('codigo_contable', $this);
    }

    public function actualizar()
    {
        $this->db->update( 'codigo_contable', $this, array('id' => $this->id) );
    }

    public function getCodigos()
    {
        $query = $this->db->get('codigo_contable');
        return $query->result_array();
    }
}