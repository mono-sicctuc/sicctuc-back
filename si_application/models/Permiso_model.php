<?php
class Permiso_model extends CI_Model 
{
    public $id;
    public $valor;
    public $seccion_id;
    public $rol_id;

    public function __construct() 
    {
        $this->load->database();
    }

    public function get_permisos()
    {
        /*$this->db->select('r.rol_nombre, s.seccion_nombre, p.valor');
        $this->db->from('permiso as p');
        $this->db->join('seccion as s', 's.id = p.seccion_id', 'left');
        $this->db->join('rol as r', 'r.id = p.rol_id', 'left');
        $this->db->where('r.id', $this->rol_id);
        $query = $this->db->get();

        return $query->result_array();*/
        $this->db->where('rol_id', $this->rol_id);
        $this->db->where('seccion_id', $this->seccion_id);
        $query = $this->db->get('permiso', 1, 0);

        return $query->row_array();
    }
}