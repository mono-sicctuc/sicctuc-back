<?php
class Seccion_model extends CI_Model
{
    public $id;
    public $seccion_nombre;

    public function __construct()
    {
        $this->load->database();
    }

    public function obtener_secciones()
    {
        $query = $this->db->get('seccion');
        return $query->result_array();
    }
}