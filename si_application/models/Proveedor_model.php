<?php
class Proveedor_model extends CI_Model 
{
    public $id;
    public $nombre_proveedor;
    public $contacto_proveedor;
    public $detalle_proveedor;

    public function __construct()
    {
        $this->load->database();
    }

    public function get() {
        $query = $this->db->get('proveedor');
        return $query->result_array();
    }

    public function insertar()
    {
        $this->db->insert('proveedor', $this);
    }

    public function actualizar()
    {
        $this->db->update('proveedor', $this, array('id' => $this->id));
    }

    public function eliminar()
    {
        $this->db->delete('proveedor', array('id' => $this->id));
    }
}