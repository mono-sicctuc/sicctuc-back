<?php
class Rol_model extends CI_Model 
{
    public $id;
    public $rol_nombre;
    public $rol_descripcion;
    public $rol_estado = 1;

    public function __construct() 
    {
        $this->load->database();
    }

    public function get_roles()
    {
        $query = $this->db->get_where('rol_usuario', array('rol_estado' => 1));
        return $query->result_array();
    }

    public function insertar()
    {
        $this->db->insert('rol_usuario', $this);
    }

    public function actualizar()
    {
        $this->db->update( 'rol_usuario', $this, array('id' => $this->id) );
    }

    public function eliminar()
    {
        $this->db->set('rol_estado', 0);
        $this->db->where('id', $this->id);
        $this->db->update('rol_usuario');
    }
}