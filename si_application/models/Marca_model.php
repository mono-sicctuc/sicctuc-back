<?php
class Marca_model extends CI_Model 
{
    public $id;
    public $bus_marca_nombre;
    public $bus_marca_estado = 1;

    public function __construct()
    {
        $this->load->database();
    }

    public function getAll()
    {
        $query = $this->db->get_where('bus_marca', array('bus_marca_estado' => 1));
        return $query->result_array();
    }

    public function insertar()
    {
        $this->db->insert('bus_marca', $this);
    }

    public function actualizar()
    {
        $this->db->update( 'bus_marca', $this, array('id' => $this->id) );
    }

    public function cambiarEstado()
    {
        $this->db->set('bus_marca_estado', $this->bus_marca_estado);
        $this->db->where('id', $this->id);
        $this->db->update('bus_marca');
    }
}