<?php
class Persona_model extends CI_Model {
    public $id;
    public $persona_nombre;
    public $persona_apellido;
    public $persona_cedula;
    public $persona_nacimiento;
    public $persona_telefono;
    public $persona_email;
    public $persona_direccion;
    public $persona_salario;
    public $persona_acceso = 0;
    public $persona_estado = 1;
    public $persona_tipo_id;
    public $puesto_id;

    public function __construct() 
    {
        $this->load->database();
    }

    //Obtener personas de tipo empleado
    public function get_personas()
    {
        $this->db->select('pe.*, pu.puesto_nombre, pu.id as puesto_id, pa.usuario, pa.rol_usuario_id');
        $this->db->from('persona as pe');
        $this->db->join('puesto as pu', 'pu.id = pe.puesto_id', 'left');
        $this->db->join('persona_acceso as pa', 'pa.persona_id = pe.id', 'left');
        $this->db->where('pe.persona_estado', 1);
        $this->db->where('persona_tipo_id', 2);

        $query = $this->db->get(); //var_dump($this->db->last_query());
        return $query->result_array();
    }

    public function insertar()
    {
        $this->db->insert('persona', $this);
        return $this->db->insert_id();
    }

    public function actualizar($data)
    {
        $this->db->update( 'persona', $data, array('id' => $this->id) );
    }

    /**
	 * Actualizar el salario segun el ultimo asignado
	 */
	public function asignarSalario()
    {
		$this->db->set('persona_salario', $this->persona_salario);
		$this->db->where('id', $this->id);
		$this->db->update('persona');
    }

    /**
     * Cambiar estado
     */
    public function cambiarEstado()
    {
        $this->db->set('persona_estado', $this->persona_estado);
        $this->db->where('id', $this->id);
        $this->db->update('persona');
    }

    /**
     * Obtener todos los socios
     */
    public function getSocios()
    {
        $this->db->select('pe.*, pu.puesto_nombre, pu.id as puesto_id, pa.usuario, pa.rol_usuario_id');
        $this->db->from('persona as pe');
        $this->db->join('persona_puesto as pp', 'pe.id = pp.persona_id', 'left');
        $this->db->join('puesto as pu', 'pp.puesto_id = pu.id', 'left');
        $this->db->join('persona_acceso as pa', 'pa.persona_id = pe.id', 'left');
        $this->db->where('pe.persona_estado', 1);
        $this->db->where('persona_tipo_id', 1);

        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Obtener todas las personas
     */
    public function getAll($parametro = '', $tipo = false)
    {
        $this->db->select("CONCAT_WS(' ', persona_nombre, persona_apellido) as value, id as data");
        $this->db->from('persona');        

        if( !empty($parametro) )
        {
            $this->db->group_start();
                $this->db->like('persona_nombre', $parametro);
                $this->db->or_like('persona_apellido', $parametro);
            $this->db->group_end();
        }

        if( $tipo !== false )
        {
            $this->db->group_start();
                $this->db->where('persona_tipo_id', $tipo);
                $this->db->where('persona_estado = 1');
            $this->db->group_end();
        }
        else 
            $this->db->where('persona_estado = 1');
        
        $query = $this->db->get(); //var_dump($this->db->last_query());


        return $query->result_array();
    }

    public function getPersonaTipo()
    {
        $this->db->select('persona_tipo_id');
        $query = $this->db->get_where('persona', 'id = '.$this->id, 1, 0);
        return $query->row_array();
    }
}