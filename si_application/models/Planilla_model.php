<?php
class Planilla_model extends CI_Model 
{
    public $id;
    public $planilla_descripcion;
    public $planilla_desde;
    public $planilla_hasta;
    public $impuesto_renta_id;
    public $impuesto_inss_laboral;
    public $impuesto_inss_patronal;
    public $impuesto_inatec;

    public function __construct() 
    {
        $this->load->database();
    }

    public function getPlanillas()
    {
        //$query = $this->db->get_where('planilla', array('id' => $id), $limit, $offset);
        $this->db->select('id, planilla_descripcion, planilla_desde, planilla_hasta');
        $this->db->where('planilla_estado', 1);
        $query = $this->db->get('planilla');
        return $query->result_array();
    }
}