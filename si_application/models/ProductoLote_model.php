<?php
class ProductoLote_model extends CI_Model 
{
    public $id;
    public $lote_fecha;
    public $lote_precio;
    public $lote_precio_venta;
    public $lote_cantidad;
    public $lote_cantidad_actual;
    public $producto_id;

    public function __construct()
    {
        $this->load->database();
    }

    public function get() {
        $query = $this->db->get('producto_lote');
        return $query->result_array();
    }

    public function getLoteByProducto() {
        $sql = "SELECT * FROM producto_lote WHERE producto_id = ? and lote_cantidad_actual > 0";
        $query = $this->db->query($sql, array($this->producto_id));
        return $query->result_array();
    }

    public function insertar()
    {
        $this->db->insert('producto_lote', $this);
    }

    public function actualizar()
    {
        $this->db->update( 'producto_lote', $this, array('id' => $this->id) );
    }
}