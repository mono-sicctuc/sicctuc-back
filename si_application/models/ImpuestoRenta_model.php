<?php
class ImpuestoRenta_model extends CI_model 
{  
    public $id;
    public $estado = 1;

    public function __construct() 
    {
        $this->load->database();
    }

    public function getImpuestos()
    {
        $query = $this->db->get_where('impuesto_renta', array('estado' => 1));
        return $query->result_array();
    }

    public function guardar()
    {
        $this->db->insert('impuesto_renta', $this);
        return $this->db->insert_id();
    }

    public function inhabilitarImpuestos()
    {
        $this->db->set('estado', 0);
        $this->db->update('impuesto_renta');
    }
}