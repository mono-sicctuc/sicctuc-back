<?php
class ConstanteCambio_model extends CI_Model 
{
    public $id;
    public $constante_id;
    public $constante_valor;
    public $constante_estado = 1;

    public function __construct() 
    {
        $this->load->database();
    }

    public function inhabilitarConstantes()
    {
        $this->db->set('constante_estado', 0);
        $this->db->where('constante_id', $this->constante_id);
        $this->db->update('constante_cambio');
    }

    public function insertar()
    {
        $this->db->insert('constante_cambio', $this);
    }
}