<?php
class Asiento_model extends CI_Model 
{
    public $id;
    public $asiento_monto;
    public $asiento_descripcion;
    public $asiento_tipo;
    public $asiento_estado = 1;
    public $persona_id;
    public $codigo_contable_id;

    public function __construct() 
    {
        $this->load->database();
    }

    public function insertar()
    {
        $this->db->insert('asiento', $this);
    }

    /*public function actualizar()
    {
        $this->db->update( 'asiento', $this, array('id' => $this->id) );
    }*/
}