<?php
class PersonaSalario_model extends CI_Model 
{
    public $id;
    public $persona_salario_monto;
    public $persona_salario_estado = 1;
    public $persona_id;

    public function __construct() 
    {
        $this->load->database();
    }

    public function insertar()
    {
        $this->db->insert('persona_salario', $this);
    }

    /*public function actualizar()
    {
        $this->db->update( 'persona_puesto', $this, array('persona_id' => $this->persona_id) );
    }*/    

    public function desactivarTodos()
    {
        $this->db->update('persona_salario', array('persona_salario_estado' => 0), 'persona_id = '.$this->persona_id );
    }
}