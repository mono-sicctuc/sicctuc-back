<?php
class PrestamoAbono_model extends CI_Model 
{
    public $id;
    public $fecha_abono;
    public $monto;
    public $saldo;
    //public $dias_retraso;
    public $prestamo_id;
    public $forma_pago;

    public function __construct ()
    {
        $this->load->database();
    }

    public function guardar()
    {
        $this->db->insert('prestamo_abono', $this);
    }

    public function obtenerDetalle()
    {
        $query = $this->db->get_where('prestamo_abono', 'prestamo_id = '.$this->prestamo_id);
        return $query->result_array();
    }
}