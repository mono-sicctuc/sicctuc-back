<?php
class AsientoAnulacion_model extends CI_Model 
{
    public $id;
    public $anulacion_fecha;
    public $asiento_id;
    public $persona_id;

    public function __construct() 
    {
        $this->load->database();
    }

    public function insertar()
    {
        $this->db->insert('asiento', $this);
    }
}