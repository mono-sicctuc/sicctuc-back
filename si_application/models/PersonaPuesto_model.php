<?php
class PersonaPuesto_model extends CI_Model
{
    public $id;
    public $persona_id;
    public $puesto_id;

    public function __construct() 
    {
        $this->load->database();
    }

    public function insertar()
    {
        $this->db->insert('persona_puesto', $this);
    }

    public function actualizar()
    {
        $update['persona_id'] =  $this->persona_id;
        $update['puesto_id'] = $this->puesto_id;
        $this->db->update( 'persona_puesto', $update, array('persona_id' => $this->persona_id) );
    }

    public function eliminar()
    {
        $this->db->where('id', $this->id);
        $this->db->delete('persona_puesto');
    }

    public function eliminarPuestoPersona()
    {
        $this->db->where('persona_id', $this->persona_id);
        $this->db->delete('persona_puesto');
    }
}