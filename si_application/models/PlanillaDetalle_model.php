<?php
class PlanillaDetalle_model extends CI_Model 
{
    public $id;
    public $persona_id;
    public $salario_id;
    public $vacaciones_acumuladas;
    public $planilla_id;

    public function __construct() 
    {
        $this->load->database();
    }
}