<?php
class Producto_model extends CI_Model 
{
    public $id;
    public $producto_nombre;
    public $producto_descripcion;
    public $producto_minimo;
    public $producto_tipo_id;
    //public $producto_precio; // no es necesario
    //public $producto_cantidad; // cantidad actual // no es necesario    
    //public $producto_entrada; // cantidad al iniciar o registrar producto - creo que esto no deberia ir es ambiguo
    // agregar un precio de venta que almacene el precio promedio segun el precio de compra que podria variar entre pedidos

    public function __construct()
    {
        $this->load->database();
    }

    public function get() {
        $query = $this->db->get('producto');
        return $query->result_array();
    }

    public function insertar()
    {
        $this->db->insert('producto', $this);
    }

    public function actualizar()
    {
        $this->db->update( 'producto', $this, array('id' => $this->id) );
    }
}