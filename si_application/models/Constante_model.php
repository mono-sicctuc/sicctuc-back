<?php
class Constante_model extends CI_Model 
{
    public $id;
    public $constante_nombre;
    public $constante_slug;

    public function __construct() 
    {
        $this->load->database();
    }

    public function getConstantes() 
    {
        $this->db->select('c.id, constante_nombre, constante_slug, constante_valor');
        $this->db->from('constante as c');
        $this->db->join('constante_cambio as cc', 'c.id = cc.constante_id');
        $this->db->where('constante_estado', 1);

        $query = $this->db->get(); //var_dump($this->db->last_query());
        return $query->result_array();
    }

    public function actualizar()
    {
        $this->db->where('id', $this->id);
        $this->db->update('constante', $this);
    }

    public function getPasaje() 
    {
        $pasaje = 0;
        $query = $this->db->query("SELECT constante_id, constante_valor FROM constante AS c INNER JOIN constante_cambio cc ON c.id = cc.constante_id WHERE LOWER(constante_nombre) = 'pasaje' LIMIT 0, 1");
        $row = $query->row_array();
        if($row)
            $pasaje = $row['constante_valor'];
        return $pasaje;
    }
}