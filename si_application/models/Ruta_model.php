<?php
class Ruta_model extends CI_Model 
{
    public $id;
    public $ruta_nombre;
    public $ruta_descripcion;

    public function __construct() 
    {
        $this->load->database();
    }

    public function insertar()
    {
        $this->db->insert('ruta', $this);
    }

    public function actualizar()
    {
        $this->db->update( 'ruta', $this, array('id' => $this->id) );
    }

    // public function eliminar()
    // {
    //     $this->db->set('rol_estado', 0);
    //     $this->db->where('id', $this->id);
    //     $this->db->update('rol_usuario');
    // }

    public function getRutas() 
    {
        $query = $this->db->querY("SELECT * FROM ruta");
        return $query->result_array();
    }
}