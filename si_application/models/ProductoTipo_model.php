<?php
class ProductoTipo_model extends CI_Model 
{
    public $id;
    public $producto_tipo_descripcion;

    public function __construct()
    {
        $this->load->database();
    }

    public function get() {
        $query = $this->db->get('producto_tipo');
        return $query->result_array();
    }

    public function insertar()
    {
        $this->db->insert('producto_tipo', $this);
    }

    public function actualizar()
    {
        $this->db->update('producto_tipo', $this, array('id' => $this->id));
    }

    public function eliminar()
    {
        $this->db->delete('producto_tipo', array('id' => $this->id));
    }
}