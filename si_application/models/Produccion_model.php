<?php
class Produccion_model extends CI_Model
{
    public $produccion_tipo;
    public $produccion_monto;
    public $conductor_id;
    public $ciclo_id = null;

    public function __construct()
    {
        $this->load->database();
    }

    public function guardar()
    {
        $this->db->insert('produccion', $this);
    }

    public function getAll() 
    {
        /*$query = $this->db->get('produccion');
        return $query->result_array();*/
        $this->db->select("p.id, bus_placa, produccion_monto_total, produccion_fecha");
        $this->db->join('bus as b', 'p.bus_id = b.id');
        //$this->db->select("CONCAT_WS(' ', persona_nombre, persona_apellido) as conductor_nombre, p.*");
        //$this->db->join('persona as pe', 'pe.id = p.conductor_id', 'left'); 
        $query = $this->db->get('produccion as p');

        return $query->result_array();
    }

    public function getConductores($term, $puesto_id) 
    {
        $term = $this->db->escape_like_str(strtolower($term));
        $query = $this->db->query("SELECT pe.id as data, CONCAT_WS(' ', persona_nombre, persona_apellido) as value FROM persona as pe inner join puesto as pu on pe.puesto_id = pu.id WHERE puesto_id = {$puesto_id} AND (LOWER(persona_nombre) LIKE '%" . $term . "%' OR LOWER(persona_apellido) LIKE '%" . $term . "%')");
        $conductores = $query->result_array();
        return $conductores;
    }
}