<?php
class ImpuestoDetalle_model extends CI_Model 
{
    public $id;
    public $monto_desde;
    public $monto_hasta;
    public $impuesto_base;
    public $porcentaje;
    public $sobre_exceso;
    public $impuesto_renta_id;

    public function __construct() 
    {
        $this->load->database();
    }

    public function obtenerRangos()
    {
        $this->db->select('d.*');
        $this->db->from('impuesto_renta as i');
        $this->db->where('i.estado = 1');
        $this->db->join('impuesto_detalle as d', 'i.id = d.impuesto_renta_id');
        $this->db->order_by('d.monto_desde');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function guardar()
    {
        $this->db->insert('impuesto_detalle', $this);
    }
}