    <?php $vendor_url = base_url().'assets/vendors/'; ?>    
    <script>
      var site = '<?php echo site_url(); ?>';
    </script>
    <!-- jQuery -->
    <script src="<?php echo $vendor_url; ?>jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo $vendor_url; ?>bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- NProgress -->
    <script src="<?php echo $vendor_url; ?>nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script src="<?php echo base_url(); ?>assets/datatables/datatables.min.js"></script>
    <!-- <script src="<?php echo $vendor_url; ?>datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo $vendor_url; ?>datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo $vendor_url; ?>datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo $vendor_url; ?>datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo $vendor_url; ?>datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo $vendor_url; ?>datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo $vendor_url; ?>datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo $vendor_url; ?>datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo $vendor_url; ?>datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo $vendor_url; ?>datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo $vendor_url; ?>datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo $vendor_url; ?>datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo $vendor_url; ?>jszip/dist/jszip.min.js"></script>
    <script src="<?php echo $vendor_url; ?>pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo $vendor_url; ?>pdfmake/build/vfs_fonts.js"></script> -->

    <!-- Notify -->
    <script src="<?php echo $vendor_url; ?>pnotify/dist/pnotify.js"></script>
    <script src="<?php echo $vendor_url; ?>pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo $vendor_url; ?>pnotify/dist/pnotify.nonblock.js"></script>
    <script src="<?php echo $vendor_url; ?>pnotify/dist/pnotify.confirm.js"></script>
    <!-- BlockUI -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.min.js"></script>
    <?php 
    //iCheck
    if(!empty($enable_icheck)):
    ?>
    <script src="<?php echo $vendor_url; ?>iCheck/icheck.min.js"></script>
    <?php endif; ?>

    <?php
    //DatePicker
    if(!empty($enable_datepicker)): ?>
    <script src="<?php echo $vendor_url; ?>moment/min/moment.min.js"></script>
    <script src="<?php echo $vendor_url; ?>bootstrap-daterangepicker/daterangepicker.js"></script>
    <?php endif; 
    
    if( !empty($enable_autocomplete) ):?>
    <!-- AutoComplete -->
    <script type="text/javascript" src="<?php echo $vendor_url; ?>autocomplete-jquery/scripts/jquery.mockjax.js"></script>
    <script type="text/javascript" src="<?php echo $vendor_url; ?>autocomplete-jquery/src/jquery.autocomplete.js"></script>
    <?php endif; 

    //ChartJs
    if(!empty($enable_chartjs)): ?>
    <script src="<?php echo $vendor_url; ?>Chart.js/dist/Chart.min.js" ></script>
    <?php endif;
    
    //Enable Flot
    if(!empty($enable_flot)): ?>
    <!-- Flot -->
    <script src="<?php echo $vendor_url; ?>Flot/jquery.flot.js"></script>
    <script src="<?php echo $vendor_url; ?>Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo $vendor_url; ?>Flot/jquery.flot.time.js"></script>
    <script src="<?php echo $vendor_url; ?>Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo $vendor_url; ?>Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo $vendor_url; ?>flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo $vendor_url; ?>flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo $vendor_url; ?>flot.curvedlines/curvedLines.js"></script>
    <?php endif;

    //DateJs
    if(!empty($enable_datejs)): ?>
    <script src="<?php echo $vendor_url; ?>DateJS/build/date.js" ></script>
    <?php endif; ?>
    
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url() ?>assets/build/js/custom.js"></script>

    <!-- My custom actions -->
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <?php if(!empty($js_file)): ?>
    <script src="<?php echo base_url() ?>assets/js/actions/<?php echo $js_file; ?>"></script>
    <?php endif; ?>
  </body>
</html>