<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php 
    $vendor_url = base_url().'assets/vendors/';
    $titulo = (!empty($titulo)) ? $titulo : 'Inicio';
    ?>

    <title><?php echo $titulo; ?> | Sicctuc</title>

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url() ?>assets/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url() ?>assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#ffffff">

    <!-- Bootstrap -->
    <link href="<?php echo $vendor_url; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <!-- <script src="https://use.fontawesome.com/91dd5b0e0b.js"></script>-->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
    <!-- NProgress -->
    <link href="<?php echo $vendor_url; ?>nprogress/nprogress.css" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/datatables/datatables.min.css">
    <!-- <link href="<?php echo $vendor_url; ?>datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $vendor_url; ?>datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $vendor_url; ?>datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $vendor_url; ?>datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $vendor_url; ?>datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet"> -->
    <!-- PNotify -->
    <link href="<?php echo $vendor_url; ?>pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="<?php echo $vendor_url; ?>pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="<?php echo $vendor_url; ?>pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <?php
    //iCheck
    if(!empty($enable_icheck)):
    ?>
    <!-- iCheck -->
    <link href="<?php echo $vendor_url; ?>iCheck/skins/flat/green.css" rel="stylesheet">
    <?php endif; 
    //DatePicker
    if(!empty($enable_datepicker)):
    ?>
    <!-- Datepicker -->
    <link href="<?php echo $vendor_url; ?>bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <?php endif; ?>


    <!-- Custom Theme Style -->
    <link href="<?php echo base_url() ?>assets/build/css/custom.min.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/css/main.css" rel="stylesheet">
  </head>