  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url(); ?>" class="site_title"><i class="fa fa-bus"></i> <span>Sicctuc</span></a>
              <a href="<?php echo base_url(); ?>" style="padding-left: 10px; color: #fff;"><small>Sistema de control de caja &uacute;nica</small></a>
            </div>
            <br>
            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url() ?>/assets/images/img.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bienvenido(a),</span>
                <h2><?php echo obtener_nombre() ?></h2>
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <!-- <h3>General</h3> -->
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-user"></i> Personal <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo site_url() ?>personal">Gestionar Personal</a></li>
                            <li><a href="<?php echo site_url() ?>personal/puestos">Gestionar Puestos</a></li>
                            <li><a href="<?php echo site_url() ?>personal/planilla">Generar Planilla</a></li>
                            <li><a href="<?php echo site_url() ?>personal/produccion">Reportar Producción</a></li><!-- La idea es mostrar en algun lado
                            la informacion que reporte el conductor -->
                        </ul>
                    </li>

                    <li><a><i class="fa fa-handshake-o"></i> Socios <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo site_url() ?>socios">Gestionar Socios</a></li>
                            <li><a href="<?php echo site_url() ?>socios/cuentas">Gestionar Cuentas</a></li>
                            <li><a href="<?php echo site_url() ?>socios/buses">Gestionar Unidades de Transporte</a></li>
                            <li><a href="<?php echo site_url() ?>bus/marcas">Gestionar Marcas</a></li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-usd"></i> Contabilidad <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo site_url() ?>contabilidad/registros">Gestionar Registros Contables</a></li>
                            <li><a href="<?php echo site_url() ?>contabilidad/estadisticas">Generar Estadísticas</a></li>                            
                            <li><a href="<?php echo site_url() ?>contabilidad/prestamos">Gestionar Préstamos</a></li>
                            <li><a href="<?php echo site_url() ?>contabilidad/informes">Generar Informes</a></li>
                            <li><a href="<?php echo site_url() ?>contabilidad/variables">Variables Predefinidas</a></li>
                            <li><a href="<?php echo site_url() ?>contabilidad/impuesto-renta">Actualizar I.R.</a></li>
                            <li><a href="<?php echo site_url() ?>contabilidad/codigos">Gestionar C&oacute;digos</a></li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-cubes"></i> Productos <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo site_url() ?>productos">Gestionar Productos</a></li>
                            <li><a href="<?php echo site_url() ?>productos/tipos">Gestionar Tipos de Producto</a></li>
                            <li><a href="<?php echo site_url() ?>productos/proveedores">Gestionar Proveedores</a></li>
                            <li><a href="<?php echo site_url() ?>productos/compras">Gestionar Compras</a></li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-calculator"></i> Facturaci&oacute;n <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo site_url() ?>facturacion">Facturar</a></li>
                            <li><a href="<?php echo site_url() ?>facturacion/informe">Informe Diario</a></li>
                            <!-- <li><a href="<?php echo site_url() ?>facturacion/ciclos">Gestionar Ciclos</a></li> -->
                            <!--<li><a href="<?php echo site_url() ?>facturacion/ruta">Asignar Ruta</a></li>--><!-- no estoy segura de esta opcion -->
                            <li><a href="<?php echo site_url() ?>facturacion/sibe">Gestionar Reporte SIBE</a></li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-lock"></i> Seguridad <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo site_url() ?>seguridad/roles">Gestionar Roles</a></li>
                            <!--<li><a href="<?php echo site_url() ?>seguridad/permisos">Gestionar Permisos</a></li>-->
                            <li><a href="<?php echo site_url() ?>seguridad/accesos">Gestionar Accesos</a></li><!-- Mostrar personas a quienes se ha generado usuario -->
                        </ul>
                    </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <?php
              $usuario = $this->session->userdata('usuario');
              if(!empty($usuario)):
              ?>
              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url() ?>/assets/images/img.png" alt=""><?php echo obtener_nombre(); ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo base_url() ?>perfil/<?php echo $usuario; ?>"><i class="fa fa-user-circle pull-right"></i> Perfil</a></li>
                    <li><a href="<?php echo base_url() ?>configuracion"><i class="fa fa-cog pull-right"></i> Ajustes</a></li>
                    <li><a href="javascript:;"><i class="fa fa-question-circle pull-right"></i> Ayuda</a></li>
                    <li><a href="<?php echo base_url() ?>cerrar-sesion"><i class="fa fa-sign-out pull-right"></i> Cerrar Sesi&oacute;n</a></li>
                  </ul>
                </li>
              </ul>
              <?php endif; ?>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->