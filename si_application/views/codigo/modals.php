<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="codigo-form">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Formulario - Códigos Contables</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-label-left send-form" action="/codigo/guardar" novalidate>
            <input type="hidden" name="id">
            <div class="form-group">
                <label for="tipo_cuenta" class="control-label col-md-3 col-sm-3 col-xs-12">Tipo de Cuenta</label>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <div class="radio">
                        <label data-id="balance">
                            <input type="radio" class="flat" checked name="tipo_cuenta" id="balance" value="balance"> Balance General
                        </label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <div class="radio">
                        <label data-id="estado">
                            <input type="radio" class="flat" name="tipo_cuenta" id="estado" value="estado"> Estado de Resultado
                        </label>
                    </div>
                </div>                
            </div>

            <div class="form-group">
                <label for="codigo_nombre" class="control-label col-md-3 col-sm-3 col-xs-12">Nombre</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="codigo_nombre" id="codigo_nombre" required class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="form-group">
                <label for="codigo_descripcion" class="control-label col-md-3 col-sm-3 col-xs-12">Descripción</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="codigo_descripcion" id="codigo_descripcion" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <fieldset class="balance_data">
                <div class="form-group">
                    <label for="codigo_valor" class="control-label col-md-3 col-sm-3 col-xs-12">Código</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="codigo_valor" id="codigo_valor" required class="form-control col-md-7 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label for="codigo_tipo" class="control-label col-md-3 col-sm-3 col-xs-12">Tipo</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select name="codigo_tipo" id="codigo_tipo" class="form-control col-md-7 col-xs-12" required>
                            <option value="">Seleccionar Tipo</option>
                            <option value="1">Activo</option>
                            <option value="2">Pasivo</option>
                            <option value="3">Capital</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="codigo_subtipo" class="control-label col-md-3 col-sm-3 col-xs-12">SubTipo</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select name="codigo_subtipo" id="codigo_subtipo" class="form-control col-md-7 col-xs-12" required>
                            <option value="">Primero Seleccionar Tipo</option>
                        </select>
                    </div>
                </div>
            </fieldset>

            <fieldset class="er_data" style="display:none;">
                <div class="form-group">
                    <label for="estado_resultado_tipo" class="control-label col-md-3 col-sm-3 col-xs-12">Tipo de Registro (ER)</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select name="estado_resultado_tipo" id="estado_resultado_tipo" class="form-control col-md-7 col-xs-12">
                            <option value="">Seleccionar Tipo</option>
                            <option value="1">Ingreso</option>
                            <option value="2">Costo</option>
                            <option value="3">Gasto de Operación</option>
                        </select>
                    </div>
                </div> 
            </fieldset>                       
          
            <div class="ln_solid"></div>

            <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>