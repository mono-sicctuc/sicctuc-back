<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Gestionar Tipo de Producto</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a data-target="#productotipo-form-nuevo" data-toggle="modal" data-action="add-item" title="Agregar"><i class="fa fa-plus"></i></a></li>
                            <li class="hidden-options"><a title="Editar" data-target="#productotipo-form-editar" data-toggle="modal" data-action="edit-item"><i class="fa fa-pencil"></i></a></li>
                            <li class="hidden-options"><a title="Eliminar" data-action="delete-item" data-target="productotipo/eliminar"><i class="fa fa-trash"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable-producto" class="table table-striped table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                </tr>
                            </thead>
                        </table>
                        <div id="procesandoTipo" class="procesando-tabla">Cargando</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->