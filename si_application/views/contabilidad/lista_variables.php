<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $titulo ?></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li class="hidden-options"><a data-target="#variables-form" data-toggle="modal" data-action="edit-item" title="Editar" ><i class="fa fa-pencil"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">            
            <table id="datatable-variable" class="table table-striped table-bordered data-table">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Nombre</th>
                  <th>Abreviatura</th>
                  <th>Valor (%)</th>
                </tr>
              </thead>              
            </table>
            <div id="procesandoVariables" class="procesando-tabla">Cargando</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->