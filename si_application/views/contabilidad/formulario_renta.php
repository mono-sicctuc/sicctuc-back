<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $titulo ?></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">            
            <form class="form-horizontal form-label-left send-form" action="/contabilidad/guardar-impuesto">
                <table id="impuesto-table" class="table table-striped">
                    <thead>
                        <tr>
                            <th>De C$</th>
                            <th>Hasta C$</th>
                            <th>Impuesto Base</th>
                            <th>Porcentaje Aplicable</th>
                            <th>Sobre exceso de C$</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($rangos)) 
                            foreach($rangos as $rg): ?>
                        <tr>
                            <td><input type="number" step="0.01" name="monto_desde[]" class="form-control col-xs-12" min="0.01" value="<?php echo $rg['monto_desde'] ?>"></td>
                            <td><input type="number" step="0.01" name="monto_hasta[]" class="form-control col-xs-12" min="0" value="<?php echo $rg['monto_hasta'] ?>"></td>
                            <td><input type="number" step="0.01" name="impuesto_base[]" class="form-control col-xs-12" min="0" value="<?php echo $rg['impuesto_base'] ?>"></td>
                            <td><input type="number" step="0.01" name="porcentaje[]" class="form-control col-xs-12" min="0" max="100" value="<?php echo $rg['porcentaje'] ?>"></td>
                            <td><input type="number" step="0.01" name="sobre_exceso[]" class="form-control col-xs-12" min="0" value="<?php echo $rg['sobre_exceso'] ?>"></td>
                            <td><a href="#" class="delete-row"><i class="fa fa-trash"></i></a></td>
                        </tr>
                        <?php endforeach; ?>                                              
                    </tbody>
                </table>
                  
                <div class="form-group">
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <a href="#" class="btn btn-primary add-row">Agregar Fila</a>
                        <button type="submit" class="btn btn-success">Guardar</button>
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->