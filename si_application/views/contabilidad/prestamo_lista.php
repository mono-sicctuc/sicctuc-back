<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $titulo ?></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a data-target="#prestamo-form" data-toggle="modal" data-action="add-item"><i class="fa fa-plus"></i></a></li>
              <li class="hidden-options"><a title="Registrar Pago" data-target="#abono-form" data-toggle="modal" data-action="aplicar-abono"><i class="fa fa-usd"></i></a></li>
              <li class="hidden-options"><a title="Detalle de Pagos" data-target="#prestamo-detalle" data-toggle="modal" data-action="open-detalle"><i class="fa fa-eye"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
              <table id="datatable-prestamo" class="table table-striped table-bordered data-table">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Fecha</th>
                    <th>Monto</th>
                    <th>Saldo</th>
                    <th>Cuota</th>
                    <th>Forma de Pago</th>
                    <th>Estado</th>
                  </tr>
                </thead>                 
              </table>
              <div id="procesandoTabla" class="procesando-tabla">Cargando</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->