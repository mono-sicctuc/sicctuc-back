<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="prestamo-form">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Formulario - Prestamo</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-label-left send-form" action="/prestamo/guardar" novalidate>
            <input type="hidden" name="persona_id">
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_nombre">Nombres<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="persona_nombre" required="required" name="persona_nombre" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="form-group">
                <label for="fecha_solicitud" class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de Solicitud <span class="required">*</span></label>
                <div class="col-md-6 col-xs-12">
                    <input type="text" class="form-control has-feedback-left datepicker" id="fecha_solicitud" name="fecha_solicitud" placeholder="Fecha de Solicitud" aria-describedby="inputSuccess2Status" readonly required>
                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                    <span id="inputSuccess2Status" class="sr-only">(success)</span>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="monto">Monto del Préstamo<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="number" id="monto" required="required" name="monto" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="monto_abono">Cuota<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="number" id="monto_abono" required="required" name="monto_abono" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="forma_pago">Forma de Pago<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="forma_pago" id="forma_pago" required class="form-control col-md-7 col-xs-12">
                      <option value="">Debe ingresar los datos de la persona</option>                      
                    </select>                
                </div>
            </div>
          
            <div class="ln_solid"></div>

            <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="abono-form">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel2">Aplicar Abono</h4>
      </div>
      <div class="modal-body">
        <form action="/prestamo/abonar" class="form-horizontal form-label-left send-form">
          <input type="hidden" name="prestamo_id">
          <div class="form-group">
            <label for="monto" class="control-label col-md-6 col-sm-12">Monto <span class="required">*</span></label>
            <div class="col-md-6 col-xs-12">
              <input type="number" class="form-control col-md-6 col-sm-12" min="1" name="monto_abono" class="required" value="1">
            </div>
            <div class="clearfix"></div>
            <p class="help-block text-center">Abono mínimo: <span class="abono_minimo"></span> </p>
          </div>

          <div class="ln_solid"></div>

          <div class="form-group">
            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="prestamo-detalle">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel2">Detalle de Préstamo</h4>
      </div>
      <div class="modal-body">
        <table id="datatable-prestamo-detalle" class="table table-striped table-bordered data-table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Id</th>
              <th>prestamo_id</th>
              <th>Fecha de Pago</th>
              <th>Forma de Pago</th>
              <th>Cuota</th>
              <th>Saldo</th>
              <th>created</th>              
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>