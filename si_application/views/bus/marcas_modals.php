<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="marca-form">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Formulario - Marca</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-label-left send-form" action="<?php echo site_url('marca/guardar') ?>" novalidate>
            <input type="hidden" name="id">
            <div class="form-group">
                <label for="bus_marca_nombre" class="control-label col-md-3 col-sm-3 col-xs-12">Marca</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="bus_marca_nombre" id="bus_marca_nombre" required class="form-control col-md-7 col-xs-12" max-length="4">
                </div>
            </div>            
          
            <div class="ln_solid"></div>

            <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>