<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $titulo ?></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a title="Agregar Marca" data-target="#marca-form" data-toggle="modal" data-action="add-item"><i class="fa fa-plus"></i></a></li>
              <li class="hidden-options"><a title="Editar Marca" data-target="#marca-form" data-toggle="modal" data-action="edit-item"><i class="fa fa-pencil"></i></a></li>
              <li class="hidden-options"><a data-action="delete-item" title="Eliminar Marca"><i class="fa fa-trash"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
              <table id="datatable-marca" class="table table-striped table-bordered data-table">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Marca</th>
                  </tr>
                </thead>                 
              </table>
              <div id="procesandoTabla" class="procesando-tabla">Cargando</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->