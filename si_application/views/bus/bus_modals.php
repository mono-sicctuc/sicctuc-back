<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="bus-form">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Formulario - Bus</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-label-left send-form" action="<?php echo site_url('bus/guardar') ?>" novalidate>
            <input type="hidden" name="id">
            <input type="hidden" name="persona_id">
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_nombre">Dueño<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="persona_nombre" required="required" name="persona_nombre" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="form-group">
                <label for="bus_placa" class="control-label col-md-3 col-sm-3 col-xs-12">Placa<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="bus_placa" id="bus_placa" required class="form-control col-md-7 col-xs-12" max-length="4">
                </div>
            </div>

            <div class="form-group">
                <label for="bus_marca_id" class="control-label col-md-3 col-sm-3 col-xs-12">Marca<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control col-md-7 col-xs-12" name="bus_marca_id" id="bus_marca_id">
                        <option value="">Seleccionar Marca</option>
                        <?php if(!empty($marcas)): foreach($marcas as $marca): ?>
                        <option value="<?php echo $marca['id'] ?>"><?php echo $marca['bus_marca_nombre'] ?></option>
                        <?php endforeach; endif; ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="bus_modelo" class="control-label col-md-3 col-sm-3 col-xs-12">Modelo</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="bus_modelo" id="bus_modelo" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="form-group">
                <label for="bus_chasis" class="control-label col-md-3 col-sm-3 col-xs-12">Chasis</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="bus_chasis" id="bus_chasis" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="form-group">
                <label for="bus_motor" class="control-label col-md-3 col-sm-3 col-xs-12">Motor</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="bus_motor" id="bus_motor" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="form-group">
                <label for="bus_circulacion" class="control-label col-md-3 col-sm-3 col-xs-12">Circulación</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="bus_circulacion" id="bus_circulacion" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="form-group">
                <label for="bus_color" class="control-label col-md-3 col-sm-3 col-xs-12">Color</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="bus_color" id="bus_color" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
          
            <div class="ln_solid"></div>

            <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>