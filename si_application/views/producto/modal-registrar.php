<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="producto-form-nuevo">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Nuevo Producto</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-label-left send-form" action="<?php echo site_url('/producto/guardar') ?>">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="producto_tipo">Tipo de Producto<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="producto_tipo" id="producto_tipo" class="form-control col-md-7 col-xs-12">
                    <option value="">Seleccionar</option>
                    <?php if(!empty($tipos)): 
                        foreach($tipos as $tipo): ?>
                        <option value="<?php echo $tipo['id'] ?>"><?php echo $tipo['producto_tipo_descripcion'] ?></option>
                    <?php endforeach;
                    endif; ?>
                </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="producto_nombre">Nombre<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="producto_nombre" required="required" name="producto_nombre" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="producto_descripcion">Descripción</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="producto_descripcion" name="producto_descripcion" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="producto_precio">Precio<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="producto_precio" required="required" name="producto_precio" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="producto_precio_venta">Precio de Venta<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="producto_precio_venta" required="required" name="producto_precio_venta" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="producto_cantidad">Cantidad Actual<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="producto_cantidad" required="required" name="producto_cantidad" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="producto_minimo">Mínimo<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="producto_minimo" required="required" name="producto_minimo" class="form-control col-md-7 col-xs-12">
            </div>
          </div>          

          <div class="ln_solid"></div>

          <div class="form-group">
            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>