<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Gestionar Productos</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a data-target="#producto-form-nuevo" data-toggle="modal" data-action="add-item" title="Agregar Producto"><i class="fa fa-plus"></i></a></li>
              <li class="hidden-options"><a title="Editar Producto" data-target="#producto-form-editar" data-toggle="modal" data-action="edit-item"><i class="fa fa-pencil"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
              <table id="datatable-producto" class="table table-striped table-bordered data-table">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Cantidad</th>
                    <th>Cantidad Mínima</th>
                    <th>Precio de Venta</th>
                  </tr>
                </thead>                 
              </table>
              <div id="procesandoProducto" class="procesando-tabla">Cargando</div>
          </div>          
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->