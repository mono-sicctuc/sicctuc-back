<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="lote-form">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Agregar Lote</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal form-label-left send-form" action="<?php echo site_url('/producto/guardar-lote') ?>">
					<input type="hidden" name="id">

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="producto_precio-lote">Precio<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="producto_precio-lote" required="required" name="producto_precio" class="form-control col-md-7 col-xs-12">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="producto_precio_venta-lote">Precio de Venta<span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="producto_precio_venta-lote" required="required" name="producto_precio_venta" class="form-control col-md-7 col-xs-12">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="producto_cantidad-lote">Cantidad <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="producto_cantidad-lote" required="required" name="producto_cantidad" class="form-control col-md-7 col-xs-12">
						</div>
					</div>

					<div class="ln_solid"></div>

					<div class="form-group">
						<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							<button type="submit" class="btn btn-success">Guardar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>