<!-- page content -->
<?php //var_dump($persona); 
?>
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Gestionar Personal</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a data-target="#personal-form" data-toggle="modal" data-action="add-item" title="Agregar Persona"><i class="fa fa-plus"></i></a></li>
              <li class="hidden-options"><a title="Asignar Salario" data-target="#salario-form" data-toggle="modal" data-action="aumentar-salario"><i class="fa fa-usd"></i></a></li>
              <li class="hidden-options"><a title="Editar" data-target="#personal-form-editar" data-toggle="modal" data-action="edit-item"><i class="fa fa-pencil"></i></a></li>
              <li class="hidden-options"><a title="Eliminar" data-action="delete-item" data-target="personal/eliminar"><i class="fa fa-trash"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table id="datatable-personal" class="table table-striped table-bordered data-table">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Tel&eacute;fono</th>
                  <th>Puesto</th>
                  <th>Salario</th>
                </tr>
              </thead>
            </table>
            <div id="procesandoPersonal" class="procesando-tabla">Cargando</div>

            <div class="clearfix"></div>
            <p><strong><small><i class="fa fa-user"></i> Usuarios con acceso activo</small></strong></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->