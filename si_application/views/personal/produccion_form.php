<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="produccion-form">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Formulario - Producción</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form-label-left send-form" action="<?php echo site_url('personal/produccion/guardar') ?>" novalidate>
                    <input type="hidden" name="id">

                    <div class="form-group">
                        <label for="produccion_fecha" class="control-label col-md-3 col-sm-3 col-xs-12">Fecha <span class="required">*</span></label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control has-feedback-left datepicker" id="produccion_fecha" name="produccion_fecha" placeholder="Clic para seleccionar fecha" value="<?php echo date("d/m/Y"); ?>" readonly required>
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                            <span class="sr-only">(success)</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_nombre">Conductor <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="persona_nombre" id="persona_nombre" class="form-control col-md7 col-xs-12" required>
                            <input type="hidden" name="conductor_id">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bus_placa">Unidad de Transporte <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="bus_placa" id="bus_placa" class="form-control col-md7 col-xs-12" required>
                            <input type="hidden" name="bus_id">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="produccion_tipo" class="control-label col-md-3 col-sm-3 col-xs-12">Tipo de Ingresos <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="radio">
                                <label for="produccion_dia">
                                    <input type="radio" class="flat" value="1" name="produccion_tipo" id="produccion_ciclo" checked> Ingreso de un ciclo
                                </label>
                                <label for="produccion_dia">
                                    <input type="radio" class="flat" value="2" name="produccion_tipo" id="produccion_dia"> Ingreso del día
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ciclo_monto_bruto" class="control-label col-md-3 col-sm-3 col-xs-12">Monto Bruto <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" step="0.01" name="ciclo_monto_bruto" id="ciclo_monto_bruto" class="form-control col-md-7 col-xs-12" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ciclo_pasajeros" class="control-label col-md-3 col-sm-3 col-xs-12">Pasajeros <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" name="ciclo_pasajeros" id="ciclo_pasajeros" class="form-control col-md-7 col-xs-12" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ciclo_gastos" class="control-label col-md-3 col-sm-3 col-xs-12">Gastos </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" step="0.01" name="ciclo_gastos" id="ciclo_gastos" class="form-control col-md-7 col-xs-12" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ciclo_monto_neto" class="control-label col-md-3 col-sm-3 col-xs-12">Monto Neto <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="ciclo_monto_neto" id="ciclo_monto_neto" class="form-control col-md-7 col-xs-12" required readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ciclo_km" class="control-label col-md-3 col-sm-3 col-xs-12">Km. recorridos</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" step="0.01" name="ciclo_km" id="ciclo_km" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="day-type-fields hidden">
                        <div class="form-group">
                            <label for="cantidad" class="control-label col-md-3 col-sm-3 col-xs-12">Cantidad de Ciclos <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" step="0.5" name="cantidad" id="cantidad" class="form-control col-md-7 col-xs-12" required>
                            </div>
                        </div>
                    </div>

                    <div class="cicle-type-fields">
                        <div class="form-group">
                            <label for="sentido" class="control-label col-md-3 col-sm-3 col-xs-12">Sentido <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="sentido" id="sentido" class="form-control col-md-7 col-xs-12" required>
                                    <option value="">Seleccionar Sentido</option>
                                    <option value="1">Abajo - Arriba</option>
                                    <option value="2">Arriba - Abajo</option>
                                    <option value="3">Completo</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ruta_id" class="control-label col-md-3 col-sm-3 col-xs-12">Ruta <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="ruta_id" id="ruta_id" class="form-control col-md-7 col-xs-12" required>
                                <option value="">Seleccionar Ruta</option>
                                <?php if ($rutas) :
                                    foreach ($rutas as $ruta) : ?>
                                        <option value="<?php echo $ruta['id'] ?>"><?php echo $ruta['ruta_nombre'] ?></option>
                                <?php endforeach;
                                endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ciclo_notas" class="control-label col-md-3 col-sm-3 col-xs-12">Notas</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="ciclo_notas" id="ciclo_notas" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-success">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>