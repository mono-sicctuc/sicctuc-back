<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="personal-form-editar">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Formulario - Persona</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form-label-left send-form-edit" action="<?php echo site_url('/personal/actualizar') ?>" autocomplete="off">
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_nombre_edit">Nombres<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="persona_nombre_edit" required="required" name="persona_nombre" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_apellido_edit">Apellidos<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="persona_apellido_edit" required="required" name="persona_apellido" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_cedula_edit">Cédula<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="persona_cedula_edit" required="required" name="persona_cedula" class="form-control col-md-7 col-xs-12 text-uppercase">
                            <p class="help-block">Formato de cédula: ###-######-####L <span class="required">*</span></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="persona_nacimiento_edit" class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de Nacimiento </label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control has-feedback-left datepicker" id="persona_nacimiento_edit" name="persona_nacimiento" placeholder="Clic para seleccionar fecha" aria-described readonly>
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                            <span class="sr-only">(success)</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_telefono_edit">Tel&eacute;fono<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="tel" id="persona_telefono_edit" required="required" name="persona_telefono" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_email_edit">Correo Electr&oacute;nico</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" id="persona_email_edit" name="persona_email" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_direccion_edit">Direcci&oacute;n</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="persona_direccion_edit" name="persona_direccion" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="puesto_id_editar">Puesto<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="puesto_id" id="puesto_id_editar" class="form-control" required>
                                <option value=""></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_acceso_edit">Acceso</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="checkbox">
                                <label style="padding-left:0px;">
                                    <input type="checkbox" class="flat" value="1" name="persona_acceso" id="persona_acceso_edit"> Permitido
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="extra-persona-fields">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usuario_edit">Usuario<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="usuario_edit" name="usuario" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contrasena_edit">Contrase&ntilde;a</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="contrasena_edit" name="contrasena" class="form-control col-md-7 col-xs-12">
                                <p class="help-block">Si este es un nuevo usuario la contraseña es requerida. <span class="required">*</span></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rol_id_editar">Rol<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="rol_id" id="rol_id_editar" class="form-control">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-success">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>