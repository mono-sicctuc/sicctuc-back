<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="salario-form">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel2">Asignar Aumento de Salario</h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo site_url('/personal/salario') ?>" class="form-horizontal form-label-left send-form-salario">
                    <input type="hidden" name="persona_id">
                    <div class="form-group">
                        <label for="salario" class="control-label col-md-6 col-sm-12">Salario <span class="required">*</span></label>
                        <div class="col-md-6 col-xs-12">
                            <input type="number" class="form-control col-md-6 col-sm-12" min="1" name="persona_salario_monto" class="required" value="1" step="0.01">
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block text-center">Salario Actual: <span class="salario_actual"></span> </p>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-success">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>