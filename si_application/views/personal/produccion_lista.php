<!-- page content -->
<script>
  var placas = <?php echo json_encode($placas); ?>;
  var pasaje = <?php echo $pasaje; ?>;
</script>
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $titulo ?></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a data-target="#produccion-form" data-toggle="modal" data-action="add-item" title="Agregar entrada manualmente"><i class="fa fa-plus"></i></a></li>
              <li class="hidden-options"><a title="Agregar Ciclo" data-target="#produccion-form" data-toggle="modal" data-action="add-ciclo"><i class="fa fa-usd"></i></a></li>
              <li><a class="importar-produccion" title="Importar Reporte"><i class="fa fa-upload"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table id="datatable-produccion" class="table table-striped table-bordered data-table">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Bus</th>
                  <th>Monto</th>
                  <th>Fecha</th>
                </tr>
              </thead>
            </table>
            <div id="procesandoTabla" class="procesando-tabla">Cargando</div>

            <input type="file" name="archivoCsv" class="archivoCsv" id="archivoCsv" accept=".csv, text/comma-separated-values">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->