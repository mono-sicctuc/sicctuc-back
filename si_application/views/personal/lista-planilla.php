<!-- page content -->
<?php var_dump($planillas); ?>
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Generar Planilla</h2>
            <ul class="nav navbar-right panel_toolbox">
              <!-- <li><a data-target="#personal-form" data-toggle="modal" data-action="add-item"><i class="fa fa-plus"></i></a></li>
              <li class="hidden-options"><a title="Asignar Salario" data-target="#salario-form" data-toggle="modal" data-action="aumentar-salario"><i class="fa fa-usd"></i></a></li>-->
              <li class="hidden-options"><a title="Ver" data-target="#planilla-preview" data-toggle="modal" data-action="view-item"><i class="fa fa-eye"></i></a></li>
              <li class="hidden-options"><a title="Imprimir" data-action="print-item"><i class="fa fa-print"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <fieldset>
                <div class="form-group">
                    <label for="salario" class="control-label col-md-3 col-sm-3 col-xs-12">Seleccionar rango de fechas <span class="required">*</span></label>
                    <div class="col-md-4 col-xs-12">
                        <div class="input-prepend input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" name="rango_planilla"  class="range-datepicker form-control" value="<?php echo date('d/m/Y') .' - '. date('d/m/Y') ?>" readonly required />
                            <?php //echo date('d/m/Y') .' - '. date('d/m/Y') ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-primary">Generar nueva planilla</button>
                    </div>
                </div>

                
            </fieldset>
            <hr>
            <h5>Resultado</h5>
            <table id="datatable-planillas" class="table table-stiped table-bordered data-table">
              <th>id</th>
              <th>Del</th>
              <th>Al</th>
              <th>Generado el</th>
            </table>
            <div id="procesandoPlanilla" class="procesando-tabla">Cargando</div>
            <?php /*
              <table id="datatable-personal" class="table table-striped table-bordered data-table">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Tel&eacute;fono</th>
                    <th>Puesto</th>
                    <th>Salario</th>
                  </tr>
                </thead>                 
              </table>
              <div id="procesandoPersonal" class="procesando-tabla">Cargando</div>
              */ ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->