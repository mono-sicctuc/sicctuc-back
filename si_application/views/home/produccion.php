<div class="row">
    <div class="col-md-9 col-sm-12 col-xs-12">
        <div>
            <div class="x_title">
                <h2>Resumen <small>Progreso Semanal</small></h2>
                <div class="clearfix"></div>
            </div>

            <div class="demo-container" style="height:280px">
                <div id="chart_plot_02" class="demo-placeholder"></div>
            </div>
        </div>        
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div>
            <div class="x_title">
                <h2>Mejores Ingresos del d&iacute;a</h2>
                <div class="clearfix"></div>
            </div>
            <ul class="list-unstyled top_profiles scroll-view">
                <li class="media event">
                    <a class="pull-left border-blue profile_thumb">
                        <i class="fa fa-bus blue"></i>
                    </a>
                    <div class="media-body">
                        <a class="title" href="#">Unidad M-0643</a>
                        <p><strong>C$2300.55 </strong></p>
                        <p> <small>Jorge Manzanarez</small></p>
                    </div>
                </li>

                <li class="media event">
                    <a class="pull-left border-blue profile_thumb">
                        <i class="fa fa-bus blue"></i>
                    </a>
                    <div class="media-body">
                        <a class="title" href="#">Unidad M-0677</a>
                        <p><strong>$2225.45 </strong> </p>
                        <p> <small>Ren&eacute; Miranda</small></p>
                    </div>
                </li>

                <li class="media event">
                    <a class="pull-left border-blue profile_thumb">
                        <i class="fa fa-bus blue"></i>
                    </a>
                    <div class="media-body">
                        <a class="title" href="#">Unidad M-0644</a>
                        <p><strong>$2225.45 </strong> </p>
                        <p> <small>Ren&eacute; Miranda</small></p>
                    </div>
                </li>

                <li class="media event">
                    <a class="pull-left border-blue profile_thumb">
                        <i class="fa fa-bus blue"></i>
                    </a>
                    <div class="media-body">
                        <a class="title" href="#">Unidad M-0643</a>
                        <p><strong>C$2300.55 </strong></p>
                        <p> <small>Jorge Manzanarez</small></p>
                    </div>
                </li>

                <li class="media event">
                    <a class="pull-left border-blue profile_thumb">
                        <i class="fa fa-bus blue"></i>
                    </a>
                    <div class="media-body">
                        <a class="title" href="#">Unidad M-0677</a>
                        <p><strong>$2225.45 </strong> </p>
                        <p> <small>Ren&eacute; Miranda</small></p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>