<!DOCTYPE html>
<html lang="es">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php
  $vendor_url = base_url() . 'assets/vendors/';
  $titulo = (!empty($titulo)) ? $titulo : 'Inicio';
  ?>

  <title><?php echo $titulo; ?> | Sicctuc</title>

  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>assets/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>assets/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?php echo base_url() ?>assets/favicon/site.webmanifest">
  <link rel="mask-icon" href="<?php echo base_url() ?>assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#2d89ef">
  <meta name="theme-color" content="#ffffff">

  <!-- Bootstrap -->
  <link href="<?php echo $vendor_url; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <!-- <script src="https://use.fontawesome.com/91dd5b0e0b.js"></script>-->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
  <!-- NProgress -->
  <link href="<?php echo $vendor_url; ?>nprogress/nprogress.css" rel="stylesheet">
  <!-- PNotify -->
  <link href="<?php echo $vendor_url; ?>pnotify/dist/pnotify.css" rel="stylesheet">
  <link href="<?php echo $vendor_url; ?>pnotify/dist/pnotify.buttons.css" rel="stylesheet">
  <link href="<?php echo $vendor_url; ?>pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  <!-- Custom Theme Style -->
  <link href="<?php echo base_url() ?>assets/build/css/custom.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets/css/main.css" rel="stylesheet">
</head>

<body class="login">
  <div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
      <div class="animate form login_form">
        <section class="login_content">
          <?php echo form_open('iniciar-sesion', array('novalidate' => true)); ?>

          <!--form method="POST" action="acceder"-->
          <h1>Iniciar Sesi&oacute;n</h1>
          <?php if (validation_errors() || isset($custom_error)) : ?>
            <div class="alert alert-warning">
              <?php
              echo validation_errors();
              if (isset($custom_error)) echo '<p>' . $custom_error . '</p>';
              ?>
            </div>
          <?php endif; ?>
          <div>
            <input type="text" class="form-control" placeholder="Usuario" required name="usuario" />
          </div>
          <div>
            <input type="password" class="form-control" placeholder="Contrase&ntilde;a" required name="contrasena" />
          </div>
          <div>
            <button type="submit" class="btn btn-default submit">Iniciar Sesi&oacute;n</button>
            <a class="reset_pass" href="olvide-contrasena">Olvid&oacute; su contrase&ntilde;a?</a>
          </div>

          <div class="clearfix"></div>

          <div class="separator">
            <div class="clearfix"></div>
            <div>
              <h1><i class="fa fa-bus"></i> Sicctuc</h1>
              <p>&copy;<?php echo date('Y'); ?> - Bootstrap Admin Template by <a href="https://adminlte.io/themes/AdminLTE" target="_blank">AdminLTE</a></p>
            </div>
          </div>
          </form>
        </section>
      </div>
    </div>
  </div>
</body>

</html>