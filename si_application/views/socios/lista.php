<!-- page content -->
<?php //var_dump($persona); ?>
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $titulo ?></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a data-target="#socio-form" data-toggle="modal" data-action="add-item"><i class="fa fa-plus"></i></a></li>
              <li class="hidden-options"><a title="Asignar Salario" data-target="#salario-form" data-toggle="modal" data-action="aumentar-salario"><i class="fa fa-usd"></i></a></li>
              <li class="hidden-options"><a title="Editar" data-target="#socio-form" data-toggle="modal" data-action="edit-item"><i class="fa fa-pencil"></i></a></li>
              <li class="hidden-options"><a title="Eliminar" data-action="delete-item" data-target="socio/eliminar"><i class="fa fa-trash"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
              <table id="datatable-socio" class="table table-striped table-bordered data-table">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Tel&eacute;fono</th>
                    <th>Puesto</th>
                    <th>Salario</th>
                  </tr>
                </thead>                 
              </table>
              <div id="procesandoSocios" class="procesando-tabla">Cargando</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->