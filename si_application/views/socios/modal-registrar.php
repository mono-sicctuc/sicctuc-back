<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="socio-form">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Formulario - Socio</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-label-left send-form" action="<?php echo site_url('socio/guardar'); ?>" novalidate >
            <input type="hidden" name="id">
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_nombre">Nombres<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="persona_nombre" required="required" name="persona_nombre" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_apellido">Apellidos<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="persona_apellido" required="required" name="persona_apellido" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_cedula">Cédula<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="persona_cedula" required="required" name="persona_cedula" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="form-group">
                <label for="persona_nacimiento" class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de Nacimiento </label>
                <div class="col-md-6 col-xs-12">
                <input type="text" class="form-control has-feedback-left datepicker" id="persona_nacimiento" name="persona_nacimiento" placeholder="Clic para seleccionar fecha" aria-describedby="inputSuccess2Status" readonly>
                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                <span id="inputSuccess2Status" class="sr-only">(success)</span>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_telefono">Tel&eacute;fono<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="persona_telefono" required="required" name="persona_telefono" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_email">Correo Electr&oacute;nico</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="email" id="persona_email" name="persona_email" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="persona_direccion">Direcci&oacute;n</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="persona_direccion" name="persona_direccion" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="puesto_id">Puesto</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="puesto_id" id="puesto_id" class="form-control">
                    <option value=""></option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="salario" class="control-label col-md-3 col-sm-3 col-xs-12">Salario</label>
                <div class="col-md-6 col-xs-12">
                <input type="number" class="form-control col-md-7 col-xs-12" min="0" name="salario_monto" id="salario" value="0">
                </div>
            </div>

            <div class="form-group">
                <label for="persona_ingreso" class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de Ingreso <span class="required">*</span></label>
                <div class="col-md-6 col-xs-12">
                <input type="text" class="form-control has-feedback-left datepicker" id="persona_ingreso" name="persona_ingreso" placeholder="Fecha de Ingreso" aria-describedby="inputSuccess2Status" readonly required>
                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                <span id="inputSuccess2Status" class="sr-only">(success)</span>
                </div>
            </div>
          
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usuario">Usuario<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="usuario" name="usuario" class="form-control col-md-7 col-xs-12" required>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contrasena">Contrase&ntilde;a<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="password" id="contrasena" name="contrasena" class="form-control col-md-7 col-xs-12" required>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rol_id">Rol<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="rol_id" id="rol_id" class="form-control" required>
                    <option value=""></option>
                    </select>
                </div>
            </div>
          
            <div class="ln_solid"></div>

            <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="salario-form">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myModalLabel2">Asignar Aumento de Salario</h4>
      </div>
      <div class="modal-body">
        <form action="/socio/salario" class="form-horizontal form-label-left send-form-salario">
          <input type="hidden" name="persona_id">
          <div class="form-group">
            <label for="salario" class="control-label col-md-6 col-sm-12">Salario <span class="required">*</span></label>
            <div class="col-md-6 col-xs-12">
              <input type="number" class="form-control col-md-6 col-sm-12" min="1" name="persona_salario_monto" class="required" value="1">
            </div>
            <div class="clearfix"></div>
            <p class="help-block text-center">Salario Actual: <span class="salario_actual"></span> </p>
          </div>

          <div class="ln_solid"></div>

          <div class="form-group">
            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>