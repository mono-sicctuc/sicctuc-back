<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="proveedor-form-nuevo">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Nuevo Proveedor</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form-label-left send-form" action="<?php echo site_url('/proveedor/guardar') ?>">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre_proveedor">Nombre<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="nombre_proveedor" required="required" name="nombre_proveedor" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contacto_proveedor">Contacto</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="tel" id="contacto_proveedor" name="contacto_proveedor" class="form-control col-md-7 col-xs-12" maxlength="30">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="detalle_proveedor">Detalle</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="detalle_proveedor" name="detalle_proveedor" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-success">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>