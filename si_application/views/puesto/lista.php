<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Gestionar Puesto</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a data-target="#puesto-form" data-toggle="modal" data-action="add-item" title="Agregar Puesto"><i class="fa fa-plus"></i></a></li>
              <li class="hidden-options"><a data-target="#puesto-form" data-toggle="modal" data-action="edit-item" title="Editar" ><i class="fa fa-pencil"></i></a></li>
              <li class="hidden-options"><a data-action="delete-item" title="Eliminar"><i class="fa fa-trash"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">            
            <table id="datatable-puesto" class="table table-striped table-bordered data-table">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Puesto</th>
                  <th>Descripci&oacute;n</th>
                </tr>
              </thead>              
            </table>
            <div id="procesandoPuestos" class="procesando-tabla">Cargando</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->