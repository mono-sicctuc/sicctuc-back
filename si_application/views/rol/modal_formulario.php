<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="rol-form">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title modal-label">Formulario - Rol</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-label-left send-form" action="/seguridad/guardar-rol">
          <input type="hidden" name="id">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rol_nombre">Nombre del Rol<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="rol_nombre" required="required" name="rol_nombre" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rol_descripcion">Breve Descripci&oacute;n</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="rol_descripcion" name="rol_descripcion" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="reset" class="btn btn-primary">Limpiar</button>
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>