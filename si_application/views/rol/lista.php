<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Gestionar Rol</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a data-target="#rol-form" data-toggle="modal" data-action="add-item" title="Agregar Rol"><i class="fa fa-plus"></i></a></li>
              <li class="hidden-options"><a data-action="permisos-item" title="Permisos" ><i class="fa fa-key"></i></a></li>
              <li class="hidden-options"><a data-target="#rol-form" data-toggle="modal" data-action="edit-item" title="Editar" ><i class="fa fa-pencil"></i></a></li>
              <li class="hidden-options"><a data-action="delete-item" title="Eliminar" ><i class="fa fa-trash"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">            
            <table id="datatable-rol" class="table table-striped table-bordered data-table">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Rol</th>
                  <th>Descripci&oacute;n</th>
                </tr>
              </thead>              
            </table>
            <div id="procesandoRoles" class="procesando-tabla">Cargando</div>

            <div class="clearfix"></div>            

            <div class="rol-permiso">
              <div class="panel panel-default">
                <div class="panel-body">
                  <div class="text-right">
                    <a href="#" class="actualizar-permisos btn btn-primary">Actualizar</a>
                  </div>
                  <table class="table">
                    <thead>
                    <tr>
                      <th>Secci&oacute;n</th>
                      <th>Permisos</th>
                    </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Gestionar Personal</td>
                        <td>
                          <label class="checkbox-inline">
                            <input type="checkbox" id="permisoLeer1" value="R"> Listar
                          </label>
                          <label class="checkbox-inline">
                            <input type="checkbox" id="permisoCrear1" value="C"> Crear
                          </label>
                          <label class="checkbox-inline">
                            <input type="checkbox" id="permisoEditar1" value="U"> Editar
                          </label>
                          <label class="checkbox-inline">
                            <input type="checkbox" id="permisoEliminar1" value="D"> Eliminar
                          </label>
                        <td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="text-right">
                    <a href="#" class="actualizar-permisos btn btn-primary">Actualizar</a>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->