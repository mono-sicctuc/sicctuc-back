<?php
class ValidarPermisos
{
    /*public function __construct()
    {
        $this->load->model('codigocontable_model', 'codigo');
    }*/

    public function validar()
    {
        $ci=& get_instance();
        $ci->load->model('personaacceso_model', 'pa');
        //$this->CI->load->model('personaacceso_model', 'pa');
        $current_uri = $ci->uri->segment(1);
        //Verificar si ha iniciado sesion
        if( empty( $ci->session->userdata('usuario')) && $current_uri != 'iniciar-sesion')
        {
            redirect(site_url('iniciar-sesion')); exit;
        }
        elseif( !empty( $ci->session->userdata('usuario')) && $current_uri == 'iniciar-sesion' )
        {
            redirect(site_url()); exit;
        }
    }
}