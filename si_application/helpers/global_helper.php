<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!function_exists('use_template'))
{
    /**
    * Genera las partes repetitivas del template
    * @param array $views_names
    * @param array $data
    */
    function use_template($views_names = array(), $data = array())
    {
        $ci=& get_instance();
        $ci->load->view('components/header', $data);
		$ci->load->view('components/top');
        if(!empty($views_names))		
        {
            foreach($views_names as $view) 
            {
                $ci->load->view($view, $data);
            }
        }
        else 
            $ci->load->view('components/plain', $data);
		$ci->load->view('components/bottom');
		$ci->load->view('components/footer', $data);
    }
}


if(!function_exists('output_json'))
{
    /**
    * Genera la salida del josn
    * @param array $resultado
    */
    function output_json($resultado)
    {
        $ci=& get_instance();
        $ci->output
        ->set_content_type('application/json')
        ->set_output(json_encode( $resultado ));
        //die();
    }
}


if(!function_exists('obtener_nombre'))
{
    /**
     * Obtiene nombre completo del usuario en la sesion
     */
    function obtener_nombre()
    {
        $ci=& get_instance();
        $ci->load->model('personaacceso_model', 'pa');
        $usuario = $ci->session->userdata('usuario');;
        $nombre = $ci->pa->obtener_nombre_by_usuario($usuario);
        return $nombre;
    }
}